#!/bin/sh

rm SwissArmyHammer.dmg

mkdir mac_build
cp ./swiss_army_hammer mac_build
cp ./README mac_build
cp ./CHANGELOG mac_build
cp ./data/cessna.obj mac_build
cp ./data/vp.mtl mac_build

echo "Creating SwissArmyHammer.dmg..."
hdiutil create -srcfolder mac_build -volname "Swiss Army Hammer" -format UDBZ SwissArmyHammer.dmg

rm -rf mac_build
