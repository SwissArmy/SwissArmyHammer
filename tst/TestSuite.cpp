#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE SwissArmyHammerTestSuite

#include <cstdlib>

#include "TestConfig.h"

void compare_cameras(Camera* c1, Camera* c2) {
    BOOST_TEST_CHECKPOINT("Comparing orientation");
    compare_vectors(c1->getOrientation(), c2->getOrientation());
    BOOST_TEST_CHECKPOINT("Comparing position");
    compare_vectors(c1->getPosition(), c2->getPosition());
    BOOST_TEST_CHECKPOINT("Comparing up vector");
    compare_vectors(c1->getUpVector(), c2->getUpVector());
}

void compare_vectors(Vector v1, Vector v2) {
    BOOST_CHECK_CLOSE(v1.x, v2.x, PRECISION);
    BOOST_CHECK_CLOSE(v1.y, v2.y, PRECISION);
    BOOST_CHECK_CLOSE(v1.z, v2.z, PRECISION);
}