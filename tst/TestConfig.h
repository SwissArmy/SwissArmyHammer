#ifndef TEST_CONFIG_H
#define TEST_CONFIG_H

#include <boost/test/unit_test.hpp>

#include "../src/Heisenberg/Camera.h"
#include "../src/types.h"

const double PRECISION = 0.0001f;
const int TEST_ITERATIONS = 10000;

void compare_cameras(Camera* c1, Camera* c2);
void compare_vectors(Vector v1, Vector v2);

#endif
