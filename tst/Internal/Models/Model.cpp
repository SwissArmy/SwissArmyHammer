#include "TestConfig.h"

#include "../src/Internal/Models/Model.h"
#include "../src/types.h"


BOOST_AUTO_TEST_CASE(Model_FaceNormalTest) {
    BOOST_CHECK_MESSAGE(false, "Test not implemented!");
    
    /*
    Model m;
    const Vertex v0(3,7,9), v1(5,6,2), v2(12,-44,13);
    m.vertices.push_back(v0);
    m.vertices.push_back(v1);
    m.vertices.push_back(v2);
    const Triangle t = {{0,1,2}};
    
    const Normal n = m.faceNormal(t);
    
    BOOST_CHECK_EQUAL(atof("-11.003"), -11.003);
    
    // make sure nothing in the mesh was changed
    BOOST_CHECK_EQUAL(m.vertices[0].x, 3);
    BOOST_CHECK_EQUAL(m.vertices[0].y, 7);
    BOOST_CHECK_EQUAL(m.vertices[0].z, 9);
    BOOST_CHECK_EQUAL(m.vertices[1].x, 5);
    BOOST_CHECK_EQUAL(m.vertices[1].y, 6);
    BOOST_CHECK_EQUAL(m.vertices[1].z, 2);
    BOOST_CHECK_EQUAL(m.vertices[2].x, 12);
    BOOST_CHECK_EQUAL(m.vertices[2].y, -44);
    BOOST_CHECK_EQUAL(m.vertices[2].z, 13);
    BOOST_CHECK_EQUAL(m.vertices.size(), 3);
    BOOST_CHECK_EQUAL(t.vertices[0], 0);
    BOOST_CHECK_EQUAL(t.vertices[1], 1);
    BOOST_CHECK_EQUAL(t.vertices[2], 2);
    
    // make sure the normal was calculated correctly
    BOOST_CHECK_CLOSE(n.x, -0.951282, PRECISION);
    BOOST_CHECK_CLOSE(n.y, -0.18709428, PRECISION);
    BOOST_CHECK_CLOSE(n.z, -0.245067, PRECISION);
    */
}

BOOST_AUTO_TEST_CASE(Model_NormalCalculationTest) {
    BOOST_CHECK_MESSAGE(false, "Test not implemented!");
    /*
    Model m;
    const Vertex v0(3,7,9), v1(5,6,2), v2(12,-44,13), v3(4,2,4), v4(9,-10,33);
    m.vertices.push_back(v0);
    m.vertices.push_back(v1);
    m.vertices.push_back(v2);
    m.vertices.push_back(v3);
    m.vertices.push_back(v4);
    const Triangle t0 = {{0,1,2}};
    const Triangle t1 = {{0,1,3}};
    const Triangle t2 = {{1,2,4}};
    m.triangles.push_back(t0);
    m.triangles.push_back(t1);
    m.triangles.push_back(t2);
    
    m.calculateNormals();
    
    // make sure nothing in the mesh was changed
    BOOST_CHECK_EQUAL(m.vertices[0].x, 3);
    BOOST_CHECK_EQUAL(m.vertices[0].y, 7);
    BOOST_CHECK_EQUAL(m.vertices[0].z, 9);
    BOOST_CHECK_EQUAL(m.vertices[1].x, 5);
    BOOST_CHECK_EQUAL(m.vertices[1].y, 6);
    BOOST_CHECK_EQUAL(m.vertices[1].z, 2);
    BOOST_CHECK_EQUAL(m.vertices[2].x, 12);
    BOOST_CHECK_EQUAL(m.vertices[2].y, -44);
    BOOST_CHECK_EQUAL(m.vertices[2].z, 13);
    BOOST_CHECK_EQUAL(m.vertices.size(), 5);
    BOOST_CHECK_EQUAL(m.triangles[0].vertices[0], 0);
    BOOST_CHECK_EQUAL(m.triangles[0].vertices[1], 1);
    BOOST_CHECK_EQUAL(m.triangles[0].vertices[2], 2);
    
    BOOST_CHECK_EQUAL(m.face_normals.size(), m.triangles.size());
    BOOST_CHECK_EQUAL(m.vert_normals.size(), m.vertices.size());
    
    // Ensure the proper values were calculated.
    BOOST_CHECK_CLOSE(m.face_normals[0].x, -0.951282, PRECISION);
    BOOST_CHECK_CLOSE(m.face_normals[0].y, -0.18709428, PRECISION);
    BOOST_CHECK_CLOSE(m.face_normals[0].z, -0.245067, PRECISION);
    BOOST_CHECK_CLOSE(m.face_normals[1].x, -0.951282, PRECISION);
    BOOST_CHECK_CLOSE(m.face_normals[1].y, -0.18709428, PRECISION);
    BOOST_CHECK_CLOSE(m.face_normals[1].z, -0.245067, PRECISION);
    BOOST_CHECK_CLOSE(m.face_normals[2].x, -0.951282, PRECISION);
    BOOST_CHECK_CLOSE(m.face_normals[2].y, -0.18709428, PRECISION);
    BOOST_CHECK_CLOSE(m.face_normals[2].z, -0.245067, PRECISION);
    
    const Normal expected_v0 = glm::normalize((m.face_normals[0] + m.face_normals[1]) / 2.0);
    const Normal expected_v1 = glm::normalize((m.face_normals[0] + m.face_normals[1] + m.face_normals[2]) / 3.0);
    const Normal expected_v2 = glm::normalize((m.face_normals[0] + m.face_normals[2]) / 2.0);
    
    BOOST_CHECK_EQUAL(m.vert_normals[0].x, expected_v0.x);
    BOOST_CHECK_EQUAL(m.vert_normals[0].y, expected_v0.y);
    BOOST_CHECK_EQUAL(m.vert_normals[0].z, expected_v0.z);
    BOOST_CHECK_EQUAL(m.vert_normals[1].x, expected_v1.x);
    BOOST_CHECK_EQUAL(m.vert_normals[1].y, expected_v1.y);
    BOOST_CHECK_EQUAL(m.vert_normals[1].z, expected_v1.z);
    BOOST_CHECK_EQUAL(m.vert_normals[2].x, expected_v2.x);
    BOOST_CHECK_EQUAL(m.vert_normals[2].y, expected_v2.y);
    BOOST_CHECK_EQUAL(m.vert_normals[2].z, expected_v2.z);
    BOOST_CHECK_EQUAL(m.vert_normals[3].x, m.face_normals[1].x);
    BOOST_CHECK_EQUAL(m.vert_normals[3].y, m.face_normals[1].y);
    BOOST_CHECK_EQUAL(m.vert_normals[3].z, m.face_normals[1].z);
    BOOST_CHECK_EQUAL(m.vert_normals[4].x, m.face_normals[2].x);
    BOOST_CHECK_EQUAL(m.vert_normals[4].y, m.face_normals[2].y);
    BOOST_CHECK_EQUAL(m.vert_normals[4].z, m.face_normals[2].z);
    */
}

BOOST_AUTO_TEST_CASE(Model_FaceCenterTest) {
    BOOST_CHECK_MESSAGE(false, "Test not implemented!");
    
    /*
    Model m;
    const Vertex v0(3,7,9), v1(5,6,2), v2(12,-44,13);
    m.vertices.push_back(v0);
    m.vertices.push_back(v1);
    m.vertices.push_back(v2);
    const Triangle t = {{0,1,2}};
    
    const Vertex c = m.faceCenter(t);

    // make sure nothing in the mesh was changed
    BOOST_CHECK_EQUAL(m.vertices[0].x, 3);
    BOOST_CHECK_EQUAL(m.vertices[0].y, 7);
    BOOST_CHECK_EQUAL(m.vertices[0].z, 9);
    BOOST_CHECK_EQUAL(m.vertices[1].x, 5);
    BOOST_CHECK_EQUAL(m.vertices[1].y, 6);
    BOOST_CHECK_EQUAL(m.vertices[1].z, 2);
    BOOST_CHECK_EQUAL(m.vertices[2].x, 12);
    BOOST_CHECK_EQUAL(m.vertices[2].y, -44);
    BOOST_CHECK_EQUAL(m.vertices[2].z, 13);
    BOOST_CHECK_EQUAL(m.vertices.size(), 3);
    BOOST_CHECK_EQUAL(t.vertices[0], 0);
    BOOST_CHECK_EQUAL(t.vertices[1], 1);
    BOOST_CHECK_EQUAL(t.vertices[2], 2);
    
    // make sure the center was calculated correctly
    BOOST_CHECK_CLOSE(c.x, 6.66666667, PRECISION);
    BOOST_CHECK_CLOSE(c.y, -10.3333333, PRECISION);
    BOOST_CHECK_CLOSE(c.z, 8, PRECISION);*/
}
