#include "TestConfig.h"

#include "../src/Heisenberg/SimpleCamera.h"

BOOST_AUTO_TEST_CASE(Camera_getOrientationTest) {
    SimpleCamera simple_camera;
    simple_camera.setPosition(Vector(0,0,1));
    simple_camera.setTarget(Vector(0,0,0));
    const Vector o1 = simple_camera.getOrientation();
    
    compare_vectors(o1, Vector(0, 0, -1));
    
    simple_camera.setPosition(Vector(0.00100316, 11.7157, -78.7349));
    simple_camera.setTarget(Vector(0.00100316, 11.7157, 0.00117416));
    const Vector o2 = simple_camera.getOrientation();
    
    compare_vectors(o2, Vector(0, 0, 1));
}

BOOST_AUTO_TEST_CASE(Camera_OrbitTest) {
    BOOST_CHECK_MESSAGE(false, "Test not implemented!");
}

BOOST_AUTO_TEST_CASE(Camera_PanTest) {
    BOOST_CHECK_MESSAGE(false, "Test not implemented!");
}

BOOST_AUTO_TEST_CASE(Camera_LookAtTest) {
    BOOST_CHECK_MESSAGE(false, "Test not implemented!");
}
