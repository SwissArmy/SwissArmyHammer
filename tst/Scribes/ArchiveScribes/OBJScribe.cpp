#include "TestConfig.h"

#include "../src/Scribes/ModelScribes/OBJScribe.h"
#include "../src/types.h"

// Tests reading a simple model file
BOOST_AUTO_TEST_CASE(OBJFile_ReadModelTest1) {
    BOOST_CHECK_MESSAGE(false, "Test not implemented!");
    /*OBJScribe file;
    Model* m = file.readModel("tst/data/obj_test1.obj");
    
    BOOST_CHECK_EQUAL(m->vertices.size(), 3);
    BOOST_CHECK_EQUAL(m->triangles.size(), 1);
    
    BOOST_CHECK_CLOSE(m->vertices[0].x, -11.291303, PRECISION);
    BOOST_CHECK_CLOSE(m->vertices[0].y, -3.380100, PRECISION);
    BOOST_CHECK_CLOSE(m->vertices[0].z, -7.662957, PRECISION);
    BOOST_CHECK_CLOSE(m->vertices[1].x, -11.233731, PRECISION);
    BOOST_CHECK_CLOSE(m->vertices[1].y, -4.422688, PRECISION);
    BOOST_CHECK_CLOSE(m->vertices[1].z, -6.142551, PRECISION);
    BOOST_CHECK_CLOSE(m->vertices[2].x, -11.235865, PRECISION);
    BOOST_CHECK_CLOSE(m->vertices[2].y, -4.604924, PRECISION);
    BOOST_CHECK_CLOSE(m->vertices[2].z, -5.866001, PRECISION);
    
    BOOST_CHECK_EQUAL(m->triangles[0].vertices[0], 0);
    BOOST_CHECK_EQUAL(m->triangles[0].vertices[1], 1);
    BOOST_CHECK_EQUAL(m->triangles[0].vertices[2], 2);*/
}

// Tests reading a model file containing
// non-triangular faces.
BOOST_AUTO_TEST_CASE(OBJFile_ReadModelTest2) {
    BOOST_CHECK_MESSAGE(false, "Test not implemented!");
    /*OBJFile file;
    Model* m = file.readModel("tst/data/obj_test2.obj");
    
    BOOST_CHECK_EQUAL(m->vertices.size(), 16);
    BOOST_CHECK_EQUAL(m->triangles.size(), 14);*/
}