#!/bin/sh

apt -yy install \
  cmake \
  libsoil-dev \
  libsfml-dev \
  libboost-system-dev \
  libboost-iostreams-dev \
  clang \
  libc++-dev \
  libc++abi-dev \
  libxrandr-dev \
  libudev-dev \
  libglew-dev \
  libjpeg-dev \
  libfreetype-dev \
  libopenal-dev \
  libsndfile1-dev \
  libusb-1.0-0-dev \
  libglut-dev \
  zlib1g-dev
brew install cmake sfml boost
cmake -Bbuild -H.
cd build && make && cd ..
