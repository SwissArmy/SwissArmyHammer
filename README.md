[![Build Status](https://travis-ci.org/extropic-engine/Swiss-Army-Hammer.png?branch=master)](https://travis-ci.org/extropic-engine/Swiss-Army-Hammer)

Build Instructions
==================

Clone with `git clone --recursive` to get needed library files.

Or after cloning normally:

`git submodule update --init --recursive`

### On OS X 10.9:

`brew install cmake sfml boost`
`cd soil && ./configure && make && make install && cd ..`

### On Ubuntu 23.10

`apt install cmake libsoil-dev libsfml-dev libboost-system-dev libboost-iostreams-dev clang libc++-dev libc++abi-dev libxrandr-dev libudev-dev libglew-dev libjpeg-dev libfreetype-dev libopenal-dev libsndfile1-dev libusb-1.0-0-dev libglut-dev`

### After setup

To build the project:

`cmake -Bbuild -H. && make`

Unit tests are currently not runnable, also a work in progress.

`doxygen` - builds the documentation

Library Documentation
=====================
- GLM: [Manual](http://glm.g-truc.net/glm-0.9.4.pdf) - [API](http://glm.g-truc.net/api-0.9.4/index.html)
- SFML: [Tutorials](http://www.sfml-dev.org/tutorials/2.1/) - [API](http://www.sfml-dev.org/documentation/2.1/)

Usage
=====

The application must be run from the command line.

`./swiss_army_hammer load <model file>`

cessna.obj is an included sample model file.

Supported file formats
======================
.obj, .md2

Mouse controls
==============

- Left mouse drag: rotate the model
- Middle mouse drag: move the model
- Right mouse drag: zoom

Keyboard controls
=================

- r to cycle between rendering modes.
- f to toggle face normals.
- v to toggle vertex normals.
- ESC or q to quit.
