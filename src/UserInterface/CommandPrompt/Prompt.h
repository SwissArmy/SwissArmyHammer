#ifndef PROMPT_H
#define PROMPT_H

#include <string>
#include <vector>

class Prompt {
public:
    int run();

    int execute(const std::string &verb, const std::vector<std::string> &arguments, const std::vector<std::string> &flags);

    int convert(const std::vector<std::string> &arguments, const std::vector<std::string> &flags);
    int connect(const std::vector<std::string> &arguments, const std::vector<std::string> &flags);
    int extract(const std::vector<std::string> &arguments, const std::vector<std::string> &flags);
    int generate(const std::vector<std::string> &arguments, const std::vector<std::string> &flags);
    int load(const std::vector<std::string> &arguments, const std::vector<std::string> &flags);

private:
    void printCommands();
};

#endif