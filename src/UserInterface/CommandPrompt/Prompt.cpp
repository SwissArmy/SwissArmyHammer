// C includes
#include <cstring>

// C++ includes
#include <iostream>
#include <vector>

#define GLM_FORCE_RADIANS

// GLM includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Boost includes
#include <boost/algorithm/string.hpp>

#include "Scribes/Abstract/Archive.h"
#include "Scribes/Abstract/Model.h"
#include "Scribes/Registrar.h"

#include "Mars/Engine.h"

#include "Deimos/TreeGenerator.h"
#include "Deimos/TreeGrammar.h"

#include "Networking/Test.h"

#include "Utility/console.h"
#include "Utility/log.h"
#include "System/File.h"

#include "Prompt.h"

using namespace std;
using namespace Mars;
using namespace Phobos;
using namespace Deimos;

int Prompt::run() {

    cout << "Type 'h' for help." << endl;

    while (true) {
        string command;
        cout << ">";
        getline(cin, command);

        // split command into verb, arguments, flags
        vector<string> substrs;
        boost::split(substrs, command, boost::is_any_of(" "));

        assert(!substrs.empty());

        string verb = substrs[0];

        vector<string> arguments;
        vector<string> flags;
        for (u32 i = 1; i < substrs.size(); i++) {
            if (substrs[i][0] == '-') {
                flags.push_back(substrs[i]);
            } else {
                arguments.push_back(substrs[i]);
            }
        }

        // decision tree based on verb type
        const int result = execute(verb, arguments, flags);
        if (result != 0) {
            return result;
        }
    }

    return 0;
}

int Prompt::execute(const string &verb, const vector<string> &arguments, const vector<string> &flags) {
    if (verb == "convert") {
        return convert(arguments, flags);
    } else if (verb == "connect") {
        return connect(arguments, flags);
    } else if (verb == "extract") {
        return extract(arguments, flags);
    } else if (verb == "generate") {
        return generate(arguments, flags);
    } else if (verb == "load") {
        return load(arguments, flags);
    } else if (verb == "quit") {
        return 1;
    } else if (verb == "q") {
        return 1;
    } else {
        cout << "Unrecognized verb '" << verb << "'" << endl;
        printCommands();
    }

    return 0;
}

// ###########
// # Convert #
// ###########

//! Convert a file from one format to another.
int Prompt::convert(const vector<string> &arguments, const vector<string> &flags) {
    if (arguments.size() < 2) {
        cout << "Must provide input and output destinations." << endl;
        printCommands();
        return -1;
    }

    const shared_ptr<string> inFilename(new string(arguments[0]));
    const shared_ptr<string> outFilename(new string(arguments[1]));

    // TODO: let the user choose which scribe to use
    const shared_ptr<Scribe::Abstract::Base> inScribe = Scribe::Registrar::determineScribes(inFilename).front();
    // TODO: let the user choose which scribe to use
    const shared_ptr<Scribe::Abstract::Base> outScribe = Scribe::Registrar::determineScribes(outFilename).front();

    const shared_ptr<File> inFile(new File);
    inFile->openDiskSourceBinary(inFilename);
    const shared_ptr<File> outFile(new File);
    outFile->openDiskSourceBinary(outFilename);

    const shared_ptr<Scribe::Abstract::Archive> inArchive = dynamic_pointer_cast<Scribe::Abstract::Archive>(inScribe);
    const shared_ptr<Scribe::Abstract::Archive> outArchive = dynamic_pointer_cast<Scribe::Abstract::Archive>(outScribe);
    const shared_ptr<Scribe::Abstract::Model> inModel = dynamic_pointer_cast<Scribe::Abstract::Model>(inScribe);
    const shared_ptr<Scribe::Abstract::Model> outModel = dynamic_pointer_cast<Scribe::Abstract::Model>(outScribe);

    bool converted = false;

    if (inArchive && outArchive) {
        converted = true;
        // TODO: implement this
        Log().warning() << "Converting between archive formats not yet supported.";
        return 0;
    }

    if (inModel && outModel) {
        converted = true;
        auto models = inModel->readModels(inFile);

        vector<shared_ptr<Model>> convert_model;

        int x = 0;
        if (models.size() > 1) {
            cout << "Got " << models.size() << " models:" << endl;
            for (u32 i = 0; i < models.size(); i++) {
                cout << "[" << i << "]" << " - " << models[i]->name << endl;
            }
            cin >> x;
        }

        convert_model.push_back(models[x]);

        outModel->writeModels(outFile, convert_model);
    }

    if (!converted) {
        Log().error() << "Cannot convert between these two file formats.";
    }

    return 0;
}

int Prompt::connect(const vector<string> &arguments, const vector<string> &flags) {
    Networking::Test::runtest();
    return 0;
}

// ###########
// # Extract #
// ###########

//! Extract files from a compressed archive.
int Prompt::extract(const vector<string> &arguments, const vector<string> &flags) {
    if (arguments.size() < 2) {
        cout << "Must provide input and output destinations." << endl;
        printCommands();
        return -1;
    }

    shared_ptr<string> filename(new string(arguments[0]));

    // TODO: let the user choose which scribe to use
    shared_ptr<Scribe::Abstract::Base> scribe = Scribe::Registrar::determineScribes(filename).front();

    if (!scribe) {
        cout << "Don't know how to handle this type of file." << endl;
        return -1;
    }

    shared_ptr<Scribe::Abstract::Archive> arcScribe = dynamic_pointer_cast<Scribe::Abstract::Archive>(scribe);

    if (!arcScribe) {
        cout << arguments[0] << " is not an archive file!" << endl;
        return -1;
    }

    // TODO: determine if destination is a folder
    // TODO: create destination directory if it doesn't exist

    shared_ptr<File> file(new File());

    // TODO: check whether we should open input as binary or plaintext
    file->openDiskSourceBinary(filename);

    vector<shared_ptr<File> > files = arcScribe->readFiles(file);

    for (shared_ptr<File> file : files) {
        assert(file);

        string destination(arguments[1].c_str());
        destination.append("/");
        destination.append(*(file->filename));
        // TODO: determine whether to open output as binary
        ofstream fout(destination.c_str(), ios::out | ios::binary);
        if (fout) {
            Log().trace() << "Extracting file " << file->filename << "...";
            shared_ptr<istream> fileStream = file->readSource();
            assert(fileStream->tellg() == (streampos) 0);
            fout << fileStream->rdbuf();
            fout.close();
        } else {
            Log().error() << "Could not create file " << destination;
        }
    }

    return 0;
}

// ############
// # Generate #
// ############

//! Procedurally generate and render or export a 3D model.
int Prompt::generate(const vector<string> &arguments, const vector<string> &flags) {

    u32 iterations = 5;
    u32 seed = 20; // TODO: make this use a random seed

    // TODO: set sane defaults for these values
    if (arguments.size() > 0) {
        // TODO: use the C++ equivalent of atoi
        seed = atoi(arguments[0].c_str());
    }
    if (arguments.size() > 1) {
        // TOD: use the C++ equivalent of atoi
        iterations = atoi(arguments[1].c_str());
    }

    TreeGenerator tree_generator;
    tree_generator.setGrammar(new TreeGrammar());
    shared_ptr<Model> leaves(new Model());
    shared_ptr<Model> model(new Model());
    leaves->frames.resize(1);

    cout << "Generating tree with seed " << seed << endl;
    vector<shared_ptr<Model>> v = tree_generator.generateModel(seed, iterations);
    model = v[0];
    leaves = v[1];
    model->clean();
    leaves->clean();
    Matrix m = glm::rotate(Matrix(1.0), 0.0, Vector(0,1,0)); // yaw
    m = glm::rotate(m, -90.0, Vector(1,0,0)); // pitch
    m = glm::rotate(m, 0.0, Vector(0,0,1)); // roll
    model->transform(m);
    leaves->transform(m);

    // TODO: now render the model or save it to a file or something
    // set up the engine and scene with the model
    Log().trace() << "Creating engine.";
    shared_ptr<Engine> engine(new Engine());
    Log().trace() << "Creating scene graph.";
    engine->scene.root = make_shared<SceneNode>();
    engine->scene.root->transform = glm::mat4();
    Log().trace() << "Adding scene node.";
    engine->scene.root->leaves.push_back(make_shared<SceneLeaf>());
    Log().trace() << "Loading model.";
    engine->scene.root->leaves.front()->model = model;
    engine->scene.root->leaves.front()->frame = 0;
    Log().trace() << "Running engine.";
    engine->run();

    cout << "Generate verb is currently non-functional." << endl;
    return -1;
}

// ########
// # Load #
// ########

//! Load a 3D model and render it.
int Prompt::load(const vector<string> &arguments, const vector<string> &flags) {
    LOG_ENTER();

    if (arguments.size() < 1) {
        Log().error() << "Must provide a filename.";
        printCommands();
        LOG_EXIT();
        return -1;

    }
    shared_ptr<string> filename(new string(arguments[0]));

    // TODO: let the user choose which scribe to use
    vector<shared_ptr<Scribe::Abstract::Base>> scribes = Scribe::Registrar::determineScribes(filename);

    if (scribes.size() < 1) {
        cout << "Could not open file " << arguments[0] << ". (Unsupported file type.)" << endl;
        LOG_EXIT();
        return -1;
    }

    //trace("Choosing from " + string(scribes.size()) + " possible scribes");

    shared_ptr<Scribe::Abstract::Base> scribe = scribes.front();

    if (!scribe) {
        cout << "Could not open file " << arguments[0] << ". (Unsupported file type?)" << endl;
        LOG_EXIT();
        return -1;
    }

    Log().trace() << "Selected scribe " << classname(typeid(*scribe).name());

    const shared_ptr<File> file(new File);
    file->openDiskSourceBinary(filename);

    shared_ptr<Scribe::Abstract::Archive> arcScribe = dynamic_pointer_cast<Scribe::Abstract::Archive>(scribe);
    if (arcScribe) {
        Log().trace() << "Reading compressed data.";
        vector<shared_ptr<File>> compressed_files = arcScribe->readFiles(file);

        //u32 choice = choiceMenu(compressed_files);
    }

    shared_ptr<Scribe::Abstract::Model> modScribe = dynamic_pointer_cast<Scribe::Abstract::Model>(scribe);
    if (modScribe) {
        Log().trace() << "Loading model.";
        const vector<shared_ptr<Model>> models = modScribe->readModels(file);
        if (models.empty()) {
            Log().error() << "Didn't get any models back from the scribe.";
            exit(0);
        }

        int x = 0;
        if (models.size() > 1) {
            cout << "Got " << models.size() << " models:" << endl;
            for (u32 i = 0; i < models.size(); i++) {
                cout << "[" << i << "]" << " - " << models[i]->name << endl;
            }
            cin >> x;
        }

        // set up the engine and scene with the model
        Log().trace() << "Creating engine.";
        Engine engine;
        Log().trace() << "Creating scene graph.";
        engine.scene.root = make_shared<SceneNode>();
        engine.scene.root->transform = glm::mat4();
        Log().trace() << "Adding scene node.";
        engine.scene.root->leaves.push_back(make_shared<SceneLeaf>());
        engine.scene.root->leaves.front()->model = models[x];
        engine.scene.root->leaves.front()->frame = 0;
        Log().trace() << "Running engine.";
        engine.run();
    }

    if (!arcScribe && !modScribe) {
        Log().warning() << "Don't know what to do with this type of file.";
        LOG_EXIT();
        return -1;
    }

    LOG_EXIT();
    return 0;
}

void Prompt::printCommands() {
    string my_name("swiss_army_hammer");
    cout << "Usage:" << endl;
    cout << normal    << "    " << my_name << " load <file> : Load a model file and display it in the viewer." << endl;
}