//
// Created by nova on 10/20/24.
//

#include "TermConsole.h"
#include "luaaa.hpp"

using namespace std;
using namespace Society;

TermConsole::TermConsole() {
    boundMilieux = set<shared_ptr<Milieu>>();
    lua_State* state = nullptr;
}
TermConsole::~TermConsole() = default;

bool TermConsole::bindMilieu(const shared_ptr<Milieu> milieu) override {
    return boundMilieux.insert(milieu).second;
}
