//
// Created by nova on 10/20/24.
//

#ifndef TERMCONSOLE_H
#define TERMCONSOLE_H

#include <set>

#include "Society/Milieu.h"

class TermConsole final : Society::Bourgeoisie {
public:
    TermConsole();
    ~TermConsole() override;

    /**
    * Returns false if milieu was already bound.
    */
    bool bindMilieu(std::shared_ptr<Society::Milieu> milieu) override;
private:
    std::set<std::shared_ptr<Society::Milieu>> boundMilieux;
};



#endif //TERMCONSOLE_H
