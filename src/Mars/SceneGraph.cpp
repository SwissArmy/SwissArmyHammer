#include <SFML/OpenGL.hpp>

#define GLM_FORCE_RADIANS
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "Utility/log.h"

#include "SceneGraph.h"

using namespace std;
using namespace Phobos;

namespace Mars {

void SceneGraph::render() const {
    glm::mat4 matrix = glm::mat4(1.0);

    render(root, matrix);
}

void SceneGraph::render(shared_ptr<SceneNode> node, const glm::mat4 &lhs) const {
    glm::mat4 transform = lhs * node->transform;
    for (shared_ptr<SceneNode> child : node->children) {
        render(child, transform);
    }
    for (shared_ptr<SceneLeaf> leaf : node->leaves) {
        render(leaf, transform);
    }
}

void SceneGraph::render(shared_ptr<SceneLeaf> leaf, const glm::mat4 &transform) const {
    assert(leaf != nullptr);
    assert(leaf->model != nullptr);
    shared_ptr<Model> model = leaf->model;
    int frame = leaf->frame;
    //TODO: do the transform

    // render the model
    if (mode == SHADED || mode == FLAT) {
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_COLOR_MATERIAL);
    }

    // iterate through triangle groups
    for (u32 g = 0; g < model->triGroups.size(); g++) {
        const auto triGroup = &model->triGroups[g];
        const auto faceGroup = &model->frames[frame].face_normal_groups[g];
        const auto groupProperties = &model->groupProperties[g];
        const auto texture = model->textures[groupProperties->texture];

        // iterate through triangles within the group
        assert(faceGroup->size() == triGroup->size());
        auto fn = faceGroup->begin();
        for (const Triangle &t : *triGroup) {
            const Vertex v0 = model->frames[frame].vertices[t.vertices[0]];
            const Vertex v1 = model->frames[frame].vertices[t.vertices[1]];
            const Vertex v2 = model->frames[frame].vertices[t.vertices[2]];
            const Normal vn0 = model->frames[frame].vertex_normals[t.vertices[0]];
            const Normal vn1 = model->frames[frame].vertex_normals[t.vertices[1]];
            const Normal vn2 = model->frames[frame].vertex_normals[t.vertices[2]];
            //trace("vert ert " + to_string(v0.x) + ", " + to_string(v0.y) + ", " + to_string(v0.z));
            if (mode == POINT) {
                glBegin(GL_POINTS);
            } else if (mode == WIREFRAME) {
                glBegin(GL_LINE_LOOP);
            } else {
                glBegin(GL_TRIANGLES);
            }
            // use glNormal3dv() to submit a vertex normal with each vertex
            glColor3d(texture->ambient.r, texture->ambient.g, texture->ambient.b);
            if (mode == FLAT) { glNormal3dv(glm::value_ptr(*fn)); }
            if (mode == SHADED) { glNormal3dv(glm::value_ptr(vn0)); }
            glVertex3dv(glm::value_ptr(v0));
            if (mode == SHADED) { glNormal3dv(glm::value_ptr(vn1)); }
            glVertex3dv(glm::value_ptr(v1));
            if (mode == SHADED) { glNormal3dv(glm::value_ptr(vn2)); }
            glVertex3dv(glm::value_ptr(v2));

            glEnd();

            const double axis_length = 1.0;

            if (showFaceNormals) {
                glBegin(GL_LINES);
                // Start drawing from the center of the face
                Vertex face_center = model->faceCenter(t, model->frames[0]);
                glColor3d(0,1,0);
                glVertex3dv((GLdouble*) &face_center);
                face_center += ((*fn) * axis_length);
                glVertex3dv((GLdouble*) &face_center);
                glEnd();
            }

            if (showVertNormals) {
                glBegin(GL_LINES);
                Vertex line = v0;
                glColor3d(1,0,0);
                glVertex3dv((GLdouble*) &line);
                line += (vn0 * axis_length);
                glVertex3dv((GLdouble*) &line);
                glEnd();

                glBegin(GL_LINES);
                line = v1;
                glColor3d(1,0,0);
                glVertex3dv((GLdouble*) &line);
                line += (vn1 * axis_length);
                glVertex3dv((GLdouble*) &line);
                glEnd();

                glBegin(GL_LINES);
                line = v2;
                glColor3d(1,0,0);
                glVertex3dv((GLdouble*) &line);
                line += (vn2 * axis_length);
                glVertex3dv((GLdouble*) &line);
                glEnd();
            }

            fn++;
        }
    }

    if (mode == SHADED || mode == FLAT) {
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        glDisable(GL_COLOR_MATERIAL);
    }
}

}