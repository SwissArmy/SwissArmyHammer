#include <SFML/OpenGL.hpp>

#define GLM_FORCE_RADIANS

#include <glm/gtx/rotate_vector.hpp>

#include "Camera.h"

namespace Mars {

void Camera::pan(Scalar w, Scalar h) {
    w *= panSpeed;
    h *= panSpeed;
    position += glm::normalize(glm::cross(orientation, up)) * w;
    position += up * h;
}

void Camera::move(Scalar w, Scalar h) {
    w *= panSpeed;
    h *= panSpeed;
    position += glm::normalize(glm::cross(orientation, up)) * w;
    position += orientation * h;
}

void Camera::zoom(Scalar z) {
    z *= zoomSpeed;
    orbitDistance += z;
    if (orbitDistance <= 0) {
        orbitDistance -= z;
    } else {
        position += orientation * z;
    }
}

void Camera::orbit(Scalar yaw, Scalar pitch, Scalar roll) {
    yaw *= orbitSpeed;
    pitch *= orbitSpeed;
    roll *= orbitSpeed;
    // move the position vector along the orientation vector by the orbitDistance amount
    position += orientation * orbitDistance;
    // reverse-rotate the orientation vector
    const Vector normal = glm::normalize(glm::cross(orientation, up));
    orientation = glm::rotate(orientation, -yaw, up);
    orientation = glm::rotate(orientation, -pitch, normal);
    up = glm::rotate(up, -pitch, normal);
    up = glm::rotate(up, -roll, orientation);
    // move the position vector back along the new orientation vector by the negative orbitDistance amount
    position -= orientation * orbitDistance;
}

void Camera::rotate(Scalar yaw, Scalar pitch, Scalar roll) {
    yaw *= rotateSpeed;
    pitch *= rotateSpeed;
    roll *= rotateSpeed;
    const Vector normal = glm::normalize(glm::cross(orientation, up));
    orientation = glm::rotate(orientation, yaw, up);
    orientation = glm::rotate(orientation, pitch, normal);
    up = glm::rotate(up, pitch, normal);
    up = glm::rotate(up, roll, orientation);
}

void Camera::render() const {
    const Vector target = position + (orientation * orbitDistance);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fieldOfView, aspect, zNear, zFar);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(position.x, position.y, position.z, target.x, target.y, target.z, up.x, up.y, up.z);
}

}