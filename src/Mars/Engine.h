#ifndef MARS_ENGINE_H
#define MARS_ENGINE_H

// Mars
#include "Mars/UI/Manager.h"

#include "Camera.h"
#include "SceneGraph.h"

#include "types.h"

namespace Mars {

/**
 * Mars::Engine is the main rendering engine for SwissArmyHammer.
 * Also the gameloop manager.
 * Also the input manager.
 * It should probably be split apart at some point.
 */
class Engine {
public:
    Engine();

    // public data
    SceneGraph scene;

    // public methods
    void run();

    void mouseMoved(int x, int y);

    void printMatrices();

    void loadShader(std::string file);
    void activateShader();
    void deactivateShader();

private:

    void readShaderFile(std::string file);

    UI::Manager ui;

    // private data
    Camera camera;
    f64 diameter;
    f64 axis_line;

    // window parameters
    int window_width, window_height;
    float window_aspect;

    // mouse button stuff
    bool leftDown;
    bool middleDown;
    bool rightDown;
    glm::vec2 left_drag_position;
    glm::vec2 middle_drag_position;
    glm::vec2 right_drag_position;
};

}

#endif
