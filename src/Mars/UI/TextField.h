#ifndef MARS_UI_TEXTFIELD
#define MARS_UI_TEXTFIELD

#include "Element.h"
#include "types.h"

namespace Mars {
namespace UI {

class TextField : public Element {
public:
    void initialize();
    void render(sf::RenderWindow &window);
    bool handleEvent(const sf::Event &event);

    // Set handler functions
    void setSubmitHandler(const std::function<void (std::string)>& handler) { _submitHandler = handler; }

    // Getters and setters for properties
    void setHidden(bool shouldHide);
    void setText(std::string &newText) { _text.setString(newText); }
    const std::string text() { return _text.getString(); }

    void setDimensions(f32 x, f32 y);
    void setPosition(f32 x, f32 y);
    void setPositionCenter(f32 x, f32 y);

    void setFontSize(u32 fontSize);
    void setFontFace(std::string &fontFace);
    void setMaxLines(int lines);

    // Properties
    bool active;
private:
    u32 _maxLines;
    sf::Text _text;
    sf::RectangleShape _chrome;
    std::function<void (std::string)> _submitHandler;
};

}
}

#endif