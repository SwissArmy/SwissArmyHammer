#ifndef MARS_UI_MANAGER_H
#define MARS_UI_MANAGER_H

#include <sstream>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Element.h"

namespace Mars {
namespace UI {

class Manager {
public:
    void initialize();
    void render(sf::RenderWindow &window);
    bool handleEvent(const sf::Event &event);
    void addElement(std::shared_ptr<Element> element);
private:
    std::vector<std::shared_ptr<Element>> elements;
};

}
}

#endif