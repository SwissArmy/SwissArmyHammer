#include <map>

#include "Utility/log.h"

#include "FontManager.h"

using namespace std;

namespace Mars {
namespace UI {

static map<string, sf::Font> fonts;
static string systemFont;

const sf::Font& FontManager::getFont(string filename) {
    if (fonts.count(filename) == 0) {
        if (!fonts[filename].loadFromFile(filename)) {
            Log().error() << "Could not load font " << filename;
        }
    }

    return fonts[filename];
}

void FontManager::setSystemFont(string filename) {
    systemFont = filename;
}

const sf::Font& FontManager::getSystemFont() {
    return FontManager::getFont(systemFont);
}

}
}