#ifndef MARS_UI_ELEMENT
#define MARS_UI_ELEMENT

#include <SFML/Graphics.hpp>

namespace Mars {
namespace UI {

class Element {
public:
    Element() { isHidden = false; }
    virtual void initialize() {};
    virtual void render(sf::RenderWindow &window) {}
    virtual bool handleEvent(const sf::Event &event) { return false; }

    virtual bool hidden() { return isHidden; }
    virtual void setHidden(bool shouldHide) { isHidden = shouldHide; }
private:
    bool isHidden;
};

}
}

#endif