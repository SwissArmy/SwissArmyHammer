#include "Scribes/Registrar.h"
#include "Scribes/Abstract/Model.h"

#include "Utility/log.h"

#include "Manager.h"

using namespace std;
using namespace Phobos;

namespace Mars {
namespace UI {

void Manager::initialize() {
    for (shared_ptr<Element> &e : elements) {
        e->initialize();
    }
}

void Manager::render(sf::RenderWindow &window) {
    for (shared_ptr<Element> &e : elements) {
        e->render(window);
    }
}

bool Manager::handleEvent(const sf::Event &event) {
    for (shared_ptr<Element> &e : elements) {
        if (e->handleEvent(event)) {
            return true;
        }
    }
    return false;
}

void Manager::addElement(shared_ptr<Element> element) {
    element->initialize();
    elements.push_back(element);
}

}
}