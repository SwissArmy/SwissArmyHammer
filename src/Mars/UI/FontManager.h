#ifndef MARS_UI_FONT_MANAGER_H
#define MARS_UI_FONT_MANAGER_H

#include <string>

#include <SFML/Graphics.hpp>

namespace Mars {
namespace UI {

class FontManager {
public:
    static void setSystemFont(std::string filename);
    static const sf::Font& getSystemFont();
    static const sf::Font& getFont(std::string filename);
};

}
}

#endif