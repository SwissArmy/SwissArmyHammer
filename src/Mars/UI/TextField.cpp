#include "Utility/log.h"

#include "FontManager.h"

#include "TextField.h"

#include <functional>

namespace Mars {
namespace UI {

void TextField::initialize() {
    Log().trace() << "Loading font";
    _text.setFont(FontManager::getFont("data/fonts/YanoneKaffeesatz-ExtraLight.ttf"));
    _text.setString("test");
    _text.setCharacterSize(48);
    _text.setColor(sf::Color::Black);
    _maxLines = 1;
    _chrome.setSize(sf::Vector2f(100, 50));
    _chrome.setFillColor(sf::Color(128, 128, 128, 192));
    _chrome.setOutlineColor(sf::Color(64, 64, 64, 192));
    _chrome.setOutlineThickness(5);
    active = false;
}

void TextField::render(sf::RenderWindow &window) {
    if (hidden()) {
        return;
    }
    window.draw(_chrome);
    window.draw(_text);
}

void TextField::setDimensions(f32 x, f32 y) {
    _chrome.setSize(sf::Vector2f(x, y));
    _text.setCharacterSize(y);
}

void TextField::setPosition(f32 x, f32 y) {

    _chrome.setPosition(x, y);
    sf::FloatRect bounds = _chrome.getLocalBounds();
#if SFML_VERSION_MINOR>0
    u32 textHeight = _text.getFont()->getLineSpacing(_text.getCharacterSize());
#else
    u32 textHeight = _text.getFont().getLineSpacing(_text.getCharacterSize());
#endif
    Log().trace() << "bounds.height: " << bounds.height;
    Log().trace() << "text height: " << textHeight;
    _text.setPosition(x, y - (textHeight - (bounds.height - (_chrome.getOutlineThickness() * 2))));
}

void TextField::setPositionCenter(f32 x, f32 y) {
    sf::FloatRect bounds = _chrome.getLocalBounds();
    this->setPosition(x - (bounds.width / 2.0f), y - (bounds.height / 2.0f));
}

bool TextField::handleEvent(const sf::Event &event) {
    if (hidden()) {
        return false;
    }
    // decide whether to listen for TextEntered events or KeyPressed events

    if (!active) {
        if (event.type == sf::Event::MouseButtonPressed) {
            sf::FloatRect bounds = _chrome.getGlobalBounds();
            // if click occurred inside bounds
            if (event.mouseButton.x >= bounds.left && event.mouseButton.x <= bounds.left + bounds.width && event.mouseButton.y >= bounds.top && event.mouseButton.y <= bounds.top + bounds.height) {
                active = true;
                return true;
            }
        }
        return false;
    }

    if (event.type == sf::Event::MouseButtonPressed) {
        sf::FloatRect bounds = _chrome.getGlobalBounds();
        // if click occurred outside of bounds
        if (!(event.mouseButton.x >= bounds.left && event.mouseButton.x <= bounds.left + bounds.width && event.mouseButton.y >= bounds.top && event.mouseButton.y <= bounds.top + bounds.height)) {
            active = false;
            return true;
        }
    } else if (event.type == sf::Event::TextEntered) {
        std::string text = _text.getString();
        if (event.text.unicode == 8) { // backspace
            _text.setString(text.substr(0, text.length() - 1));
        } else if (event.text.unicode == 13) { // enter
            _submitHandler(_text.getString());
            _text.setString("");
            active = false;
        } else if (event.text.unicode == 27) { // escape
            _text.setString("");
            active = false;
        } else if (event.text.unicode < 128) {
            Log().trace() << event.text.unicode;
            text.push_back(static_cast<char>(event.text.unicode));
            _text.setString(text);
        }
        return true;
    } else if (event.type == sf::Event::KeyPressed) {
        // discard keypress events while text input is active
        return true;
    }

    return false;
}

void TextField::setHidden(bool shouldHide) {
    Element::setHidden(shouldHide);

    if (shouldHide) {
        active = false;
    }
}

void TextField::setFontSize(u32 fontSize) {
    _text.setCharacterSize(fontSize);
}

void TextField::setFontFace(std::string &fontFace) {
    auto font = FontManager::getFont("data/fonts/" + fontFace + ".ttf");
    _text.setFont(font);
}

void TextField::setMaxLines(int lines) {
    // TODO: make this actually affect something
    _maxLines = lines;
}

}
}

