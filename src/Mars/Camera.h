#ifndef MARS_CAMERA_H
#define MARS_CAMERA_H

#include "types.h"

namespace Mars {

//! A Camera in its basic form is a location, an up vector, and an orientation vector.
class Camera {
public:
    // camera state
    Vector position;
    Vector orientation;
    Vector up;
    Scalar orbitDistance;

    // config parameters
    f64 fieldOfView, zNear, zFar, aspect;

    // movement parameters
    f64 orbitSpeed;
    f64 rotateSpeed;
    f64 zoomSpeed;
    f64 panSpeed;

    Camera() {
        fieldOfView = 45;
        zNear = 0.1f;
        zFar = 9000;
        position = Vector(0,0,0);
        orientation = Vector(0,0,0);
        up = Vector(0,1,0);
        orbitDistance = 20;
        orbitSpeed = 0.01f;
        rotateSpeed = 0.01f;
        zoomSpeed = 0.01f;
        panSpeed = 0.005f;
    }

	//! Yaw, pitch, and roll are all measured in degrees.
	virtual void orbit(Scalar yaw, Scalar pitch=0, Scalar roll=0);

	//! pans the camera in the plane perpendicular to the orientation vector
	//! w is the amount to pan left/right in the plane (relative to the up vector)
	//! h is the amount to pan up/down (again relative to the up vector)
	virtual void pan(Scalar w, Scalar h);

	virtual void zoom(Scalar z);

    virtual void rotate(Scalar yaw, Scalar pitch=0, Scalar roll=0);

    //! Essentially a pan, but perpendicular to the up vector.
    virtual void move(Scalar w, Scalar h);

	virtual void render() const;
};

}

#endif
