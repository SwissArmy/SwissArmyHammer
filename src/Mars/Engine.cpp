// SFML includes
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

// GLM includes
#define GLM_FORCE_RADIANS
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

// Boost includes
#include <boost/lexical_cast.hpp>

// Scribe includes
#include "Scribes/Registrar.h"

// UI includes
#include "UI/TextField.h"

// Local includes
#include "System/File.h"
#include "Utility/log.h"

#include "Engine.h"

using namespace std;
using namespace Phobos;

namespace Mars {

void Engine::run() {
    LOG_ENTER();

    axis_line = 0.05f;
    window_width = 800;
    window_height = 600;
    window_aspect = (float)window_width / (float)window_height;
    leftDown = false;
    middleDown = false;
    rightDown = false;
    left_drag_position = glm::vec2(0,0);
    middle_drag_position = glm::vec2(0,0);
    right_drag_position = glm::vec2(0,0);

    Log().trace() << "Validating preconditions";

    // TODO: this is a hack that doesn't work if there is no model in the scene.
    assert(scene.root);
    shared_ptr<Model> model;
    if (!scene.root->leaves.empty()) {
        model = scene.root->leaves.front()->model;
    }
    assert(model);

    Log().trace() << "Centering camera";

	// Set the initial lookAt point to roughly the center of the object.
    diameter = model->getDiameter();
    Log().trace() << "Diameter is " << diameter;
    const Vector center = model->getCenter();
    Log().info() << "Model is located at <" << center.x << "," << center.y << "," << center.z << ">";

    Log().trace() << "Positioning camera";

    camera.position = Vector(center.x - (diameter * 2), center.y, center.z);
    camera.orbitDistance = diameter * 2;
    camera.orientation = Vector(1,0,0);//camera.position - center;

	camera.zoomSpeed *= diameter;
	camera.panSpeed *= diameter;
    axis_line *= diameter;

    // Initialize SFML.

    bool fullscreen = false;

    sf::RenderWindow window;

    if (fullscreen) {
        sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
        window_width = desktop.width;
        window_height = desktop.height;
        window.create(desktop, "Swiss Army Hammer", sf::Style::None);
    } else {
        window_width = 800;
        window_height = 600;
        window.create(sf::VideoMode(800,600), "Swiss Army Hammer", sf::Style::Default);
    }

    window.setVerticalSyncEnabled(true);

    shared_ptr<UI::TextField> textField(new UI::TextField());
    ui.addElement(textField);
    ui.initialize();

    textField->setDimensions(window_width * 0.8f, 50);
    textField->setPositionCenter(window_width / 2.0f, window_height / 2.0f);
    textField->setSubmitHandler([&] (string text) {
        Log().trace() << text;
        // set up a scribe, try to open the file
    });

    // Set up OpenGL
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glClearColor(1.0, 1.0, 1.0, 1.0);   // white background
    // resize the window
    glViewport(0, 0, window_width, window_height);

    camera.aspect = (f64) window_width / (f64) window_height;

    Vector cam_start = camera.position;
    Vector cam_orient = camera.orientation;
    Vector cam_up = camera.up;

    Log().trace() << "Entering main loop";

    while (true) {

        // Handle events with SFML.
        // TODO: move this out to the GUI once it has a proper idea of who is controlling what
        sf::Event event;
        while (window.pollEvent(event)) {

            if (ui.handleEvent(event)) {
                continue;
            }

            if (event.type == sf::Event::Closed) {
                return;
            } else if (event.type == sf::Event::MouseButtonPressed) {
                if (event.mouseButton.button == sf::Mouse::Left) {
                    leftDown = true;
                    left_drag_position.x = event.mouseButton.x;
                    left_drag_position.y = event.mouseButton.y;
                } else if (event.mouseButton.button == sf::Mouse::Middle) {
                    middleDown = true;
                    middle_drag_position.x = event.mouseButton.x;
                    middle_drag_position.y = event.mouseButton.y;
                } else if (event.mouseButton.button == sf::Mouse::Right) {
                    rightDown = true;
                    right_drag_position.x = event.mouseButton.x;
                    right_drag_position.y = event.mouseButton.y;
                }
            } else if (event.type == sf::Event::MouseButtonReleased) {
                if (event.mouseButton.button == sf::Mouse::Left) {
                    leftDown = false;
                } else if (event.mouseButton.button == sf::Mouse::Middle) {
                    middleDown = false;
                } else if (event.mouseButton.button == sf::Mouse::Right) {
                    rightDown = false;
                }
            } else if (event.type == sf::Event::MouseMoved) {
                this->mouseMoved(event.mouseMove.x, event.mouseMove.y);
            } else if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::F) {
                    scene.toggleFaceNormals();
                } else if (event.key.code == sf::Keyboard::V) {
                    scene.toggleVertNormals();
                } else if (event.key.code == sf::Keyboard::R) {
                    scene.cycleMode();
                } else if (event.key.code == sf::Keyboard::Escape || event.key.code == sf::Keyboard::Q) {
                    return;
                } else if (event.key.code == sf::Keyboard::O) {
                    textField->setHidden(!textField->hidden());
                    if (!textField->hidden()) {
                        textField->active = true;
                    }
                }
            }
        }

        // OpenGL rendering goes here...

        // clear last frame
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Adjust view based on camera settings
        camera.render();

        // PERFORM ALL DRAWING HERE
        scene.render();

        // render some camera rays
        /*glBegin(GL_LINES);
        glColor3d(0.8,0,0.8);
        glVertex3dv((GLdouble*) &cam_start);
        Vector orient_ray = cam_start + (camera->orientation * 10.0);
        glVertex3dv((GLdouble*) &orient_ray);
        glEnd();
        glBegin(GL_LINES);
        glColor3d(0,0.8,0.8);
        glVertex3dv((GLdouble*) &cam_start);
        Vector up_ray = cam_start + (camera->up * 10.0);
        glVertex3dv((GLdouble*) &up_ray);
        glEnd();*/

        glFlush(); // finish the drawing commands

        window.pushGLStates();
        glDisable(GL_CULL_FACE);

        // Do 2D rendering in here.

        // TODO: add a "monitor" type system to GUI to handle this
        /*
        if (!textInputActive) {
            if (scene.mode == SceneGraph::RenderMode::POINT) {
                text.setString("Point");
            } else if (scene.mode == SceneGraph::RenderMode::WIREFRAME) {
                text.setString("Wireframe");
            } else if (scene.mode == SceneGraph::RenderMode::FLAT) {
                text.setString("Flat");
            } else if (scene.mode == SceneGraph::RenderMode::SHADED) {
                text.setString("Shaded");
            }
        }*/

        ui.render(window);

        // End 2D rendering.

        glEnable(GL_CULL_FACE);
        window.popGLStates();
        window.display();   // end current frame with SFML

        GLenum err = glGetError();
        if (err == GL_INVALID_ENUM) {
            Log().error() << "GLError: Invalid enum";
        } else if (err == GL_INVALID_VALUE) {
            Log().error() << "GLError: Invalid value";
        } else if (err == GL_INVALID_OPERATION) {
            Log().error() << "GLError: Invalid operation";
        } else if (err == GL_OUT_OF_MEMORY) {
            Log().error() << "GLError: Out of memory";
        } else if (err == GL_STACK_UNDERFLOW) {
            Log().error() << "GLError: Stack underflow";
        } else if (err == GL_STACK_OVERFLOW) {
            Log().error() << "GLError: Stack overflow";
        } else if (err != GL_NO_ERROR){
            Log().error() << "GLError: " << ((u64) err);
        }
    }
}

void Engine::loadShader(string filename) {
    /*
    GLchar *fbuf = NULL;
    GLint res, num;
    GLsizei length;
    GLint size;
    GLenum type;

    // load and compile shaders, report errors
    GLuint vertShader = readShaderFile(GL_VERTEX_SHADER, filename + ".vert");
    GLuint fragShader = readShaderFile(GL_FRAGMENT_SHADER, filename + ".frag");

    prog_num = glCreateProgram();
    glAttachShader(prog_num, vertShader);
    glAttachShader(prog_num, fragShader);

    // link program, report errors
    cerr << "Linking... ";
    glLinkProgram(prog_num);
    glGetProgramiv(prog_num, GL_LINK_STATUS, &res);
    if (res != GL_TRUE) {
        cerr << "failed!\n";
        glGetProgramiv(prog_num, GL_INFO_LOG_LENGTH, &res);

        fbuf = new GLchar[res];
        glGetProgramInfoLog(prog_num, res, &res, fbuf);

        cerr << fbuf << endl;
        delete [] fbuf;
        exit(1);
    } else {
        cerr << "successful.\n";
    }

    // report uniforms
    glGetProgramiv(prog_num, GL_ACTIVE_UNIFORMS, &num);
    glGetProgramiv(prog_num, GL_ACTIVE_UNIFORM_MAX_LENGTH, &res);
    fbuf = new GLchar[res+1];
    cerr << "Active uniforms:\n";
    for (int i = 0; i < num; i++) {
        glGetActiveUniform(prog_num, i, res, &length, &size, &type, fbuf);
        cerr << "\t" << i << ": " << fbuf << endl;
    }
    delete [] fbuf;

    // report attributes
    glGetProgramiv(prog_num, GL_ACTIVE_ATTRIBUTES, &num);
    glGetProgramiv(prog_num, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &res);
    fbuf = new GLchar[res+1];
    cerr << "Active attributes:\n";
    for (int i = 0; i < num; i++) {
        glGetActiveAttrib(prog_num, i, res, &length, &size, &type, fbuf);
        cerr << "\t" << i << ": " << fbuf << endl;
    }
    delete [] fbuf;
    */
}

GLuint readShaderfile(GLuint type, string filename) {
    GLint flen = 0;
    GLint res;
    GLchar *fbuf = NULL;
    GLuint num = 0;
    /*

    ifstream infile;

    infile.open(file);

    if (!infile) {
        cerr << "Couldn't open file!\n";
        exit(1);
    }

    infile.seekg(0, ios::end);
    flen = infile.tellg();
    infile.seekg(0, ios::beg);
    fbuf = new GLchar[flen+1];
    fbuf[flen] = '\0';
    infile.read(fbuf, flen);
    infile.close();

    num = glCreateShader(kind);

    glShaderSource(num, 1, (const GLchar**)&fbuf, NULL);
    delete [] fbuf;

    cerr << "Compiling... ";
    glCompileShader(num);

    glGetShaderiv(num, GL_COMPILE_STATUS, &res);
    if (res != GL_TRUE) {
        cerr << "failed!\n";
        glGetShaderiv(num, GL_INFO_LOG_LENGTH, &flen);

        fbuf = new GLchar[flen];
        glGetShaderInfoLog(num, flen, &res, fbuf);

        cerr << fbuf << endl;
        delete [] fbuf;
        exit(1);
    } else {
        cerr << "successful.\n";
    }
*/

    return num;
}

void Engine::printMatrices() {
    GLfloat matrix[16];
    glGetFloatv (GL_MODELVIEW_MATRIX, matrix);
    // TODO: translate this to new logging framework

    Log().info() << "Modelview Matrix";
    Log().info() << "[" << matrix[0] << "," << matrix[1] << "," << matrix[2] << "," << matrix[3] << "]";
    /*
    info("[" + to_string(matrix[4]) + "," + to_string(matrix[5]) + "," + to_string(matrix[6]) + "," + to_string(matrix[7]) + "]");
    info("[" + to_string(matrix[8]) + "," + to_string(matrix[9]) + "," + to_string(matrix[10]) + "," + to_string(matrix[11]) + "]");
    info("[" + to_string(matrix[12]) + "," + to_string(matrix[13]) + "," + to_string(matrix[14]) + "," + to_string(matrix[15]) + "]");*/
    glGetFloatv (GL_PROJECTION_MATRIX, matrix);
    Log().info() << "Projection Matrix";
    /*
    info("[" + to_string(matrix[0]) + "," + to_string(matrix[1]) + "," + to_string(matrix[2]) + "," + to_string(matrix[3]) + "]");
    info("[" + to_string(matrix[4]) + "," + to_string(matrix[5]) + "," + to_string(matrix[6]) + "," + to_string(matrix[7]) + "]");
    info("[" + to_string(matrix[8]) + "," + to_string(matrix[9]) + "," + to_string(matrix[10]) + "," + to_string(matrix[11]) + "]");
    info("[" + to_string(matrix[12]) + "," + to_string(matrix[13]) + "," + to_string(matrix[14]) + "," + to_string(matrix[15]) + "]");*/
}

//! This function is called whenever the user moves the mouse.
//! x and y represent the mouse's new location.
void Engine::mouseMoved(int x, int y) {
    if (leftDown) {
        f64 yDelta = y - left_drag_position.y;
        f64 xDelta = x - left_drag_position.x;

        camera.orbit(-xDelta, yDelta);

        left_drag_position.y = y;
        left_drag_position.x = x;
    }
    if (rightDown) {
        f64 yDelta = y - right_drag_position.y;
        f64 xDelta = x - right_drag_position.x;

        camera.zoom(yDelta);

        right_drag_position.y = y;
        right_drag_position.x = x;
    }
    if (middleDown) {
        f64 yDelta = y - middle_drag_position.y;
        f64 xDelta = x - middle_drag_position.x;

        camera.pan(-xDelta, -yDelta);

        middle_drag_position.y = y;
        middle_drag_position.x = x;
    }
}

}