#ifndef MARS_SCENE_GRAPH_H
#define MARS_SCENE_GRAPH_H

#include <memory>
#include <vector>

#include "Phobos/Model.h"

namespace Mars {

struct SceneLeaf {
    std::shared_ptr<Phobos::Model> model;
    int frame;
};

struct SceneNode {
    glm::mat4 transform;
    std::vector<std::shared_ptr<SceneNode>> children;
    std::vector<std::shared_ptr<SceneLeaf>> leaves;
};

class SceneGraph {
public:
    // defs
    enum RenderMode {POINT=0, WIREFRAME=1, FLAT=2, SHADED=3};

    // vars
    std::shared_ptr<SceneNode> root = std::make_shared<SceneNode>(SceneNode());
    int mode = SHADED;

    // funs
    void render() const;
    void toggleFaceNormals() { showFaceNormals = !showFaceNormals; }
    void toggleVertNormals() { showVertNormals = !showVertNormals; }
    void cycleMode() { mode = (mode + 1) % (SHADED + 1); }
private:
    bool showVertNormals = false;
    bool showFaceNormals = false;

    // funs
    void render(std::shared_ptr<SceneNode> node, const glm::mat4 &lhs) const;
    void render(std::shared_ptr<SceneLeaf> leaf, const glm::mat4 &lhs) const;
};

}

#endif
