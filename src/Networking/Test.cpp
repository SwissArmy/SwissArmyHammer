#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>

#include "Test.h"

using boost::asio::ip::tcp;

using namespace std;

namespace Networking {

void Test::runtest() {

    // Notes:
    // EQEmu world server listens on port 9000
    // Look at world/console.cpp to see comm format.
    // Can bring up a zone via the world server.
    // Do this and then find out how the zone server responds to messages.

    try {
        boost::asio::io_service io_service;
        tcp::resolver resolver(io_service);
        tcp::resolver::query query("127.0.0.1", "9000");
        tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
        tcp::socket socket(io_service);
        boost::asio::connect(socket, endpoint_iterator);
        boost::asio::streambuf request;
        std::ostream requestStream(&request);
        requestStream << "GET /index.html HTTP/1.1\r\n"
                      << "Connection: Keep-Alive\r\n"
                      << "Host: 127.0.0.1\r\n\r\n";

        boost::asio::write(socket, request);
        while (true) {
            boost::array<char, 128> buf;
            boost::system::error_code error;
            size_t len = socket.read_some(boost::asio::buffer(buf), error);
            if (error == boost::asio::error::eof) {
                break;
            } else if (error) {
                throw boost::system::system_error(error);
            }
            cout.write(buf.data(), len);
        }
    } catch (exception &e) {
        cerr << e.what() << endl;
    }
}

}