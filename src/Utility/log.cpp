
// C++ includes
#include <iostream>

#include <cxxabi.h>

#include "console.h"

#include "log.h"

using namespace std;

int Log::indent_level = 0;

Log::~Log() {
    cout << output.str() << normal << endl << flush;
}

ostringstream& Log::trace()   { output << indent << white; return output; }
ostringstream& Log::debug()   { output << indent << cyan; return output; }
ostringstream& Log::info()    { output << indent << blue; return output; }
ostringstream& Log::warning() { output << indent << yellow; return output; }
ostringstream& Log::error()   { output << indent << red; return output; }
ostringstream& Log::fatal()   { output << indent << red; return output; }

void Log::enter(string function, const int line) {
    Log().trace() << green << "--> " << normal << function << " [" << line << "]";
    indent_level++;
}

void Log::exit(string function, const int line) {
    indent_level--;
    Log().trace() << red << "<-- " << normal << function << " [" << line << "]";
}

string classname(const char* name) {
    int status;
    char* demangled = abi::__cxa_demangle(name,0,0,&status);
    string result;
    result.assign(demangled);
    free(demangled);
    return result;
}