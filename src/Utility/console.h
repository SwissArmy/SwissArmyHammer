#ifndef CONSOLE_H
#define CONSOLE_H

#include <ostream>

// Comment this to disable colors.
#define colors

// Prints following text in the given color.
std::ostream& red    ( std::ostream& output );
std::ostream& green  ( std::ostream& output );
std::ostream& yellow ( std::ostream& output );
std::ostream& blue   ( std::ostream& output );
std::ostream& magenta( std::ostream& output );
std::ostream& cyan   ( std::ostream& output );
std::ostream& white  ( std::ostream& output );
//! Prints following text in gray (terminal standard color.)
std::ostream& normal ( std::ostream& output );
std::ostream& indent ( std::ostream& output );

#endif