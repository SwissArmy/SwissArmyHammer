#ifndef FLAGS_H
#define FLAGS_H

#include <string>

#include "types.h"
/*
std::string flagsToString(u8 flags) {
    char flagstring[9];
    
    for (int i = 0; i < 8; i++) {
        if (flags & (1 << i))
            flagstring[i] = '1';
        else
            flagstring[i] = '0';
    }
    flagstring[8] = '\0';
    
    return std::string(flagstring, 8);
}

std::string flagsToString(u16 flags) {
    char flagstring[17];
    
    for (int i = 0; i < 16; i++) {
        if (flags & (1 << i))
            flagstring[i] = '1';
        else
            flagstring[i] = '0';
    }
    flagstring[16] = '\0';
    
    return std::string(flagstring, 16);
};*/

std::string flagsToString(u32 flags);

#endif