#ifndef RANDOM_H
#define RANDOM_H

#include "types.h"

class Random {
public:
    //! Generates a random value between 0.0 and 1.0.
    static Scalar ScalarValue() {
        return (Scalar) rand() / (Scalar) RAND_MAX;
    };

    //! Generates a random value between 0.0 and max
    static Scalar ScalarValue(Scalar max) {
        return ScalarValue() * max;
    };
    //! Generates a random value between min and max
    static Scalar ScalarValue(Scalar min, Scalar max) {
        return ScalarValue(max - min) + min; 
    };

    static int IntValue() {
        return rand();
    }

    static int IntValue(int max) {
        return IntValue() % max;
    }

    static int IntValue(int min, int max) {
        return IntValue(max - min) + min;
    }
};
#endif // RANDOM
