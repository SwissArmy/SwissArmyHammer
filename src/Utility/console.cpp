#include "log.h"

#include "console.h"

#ifdef colors
std::ostream& red    ( std::ostream& output ) { return output << "\033[0;31m"; }
std::ostream& green  ( std::ostream& output ) { return output << "\033[0;32m"; }
std::ostream& yellow ( std::ostream& output ) { return output << "\033[0;33m"; }
std::ostream& blue   ( std::ostream& output ) { return output << "\033[0;34m"; }
std::ostream& magenta( std::ostream& output ) { return output << "\033[0;35m"; }
std::ostream& cyan   ( std::ostream& output ) { return output << "\033[0;36m"; }
std::ostream& white  ( std::ostream& output ) { return output << "\033[0;37m"; }
std::ostream& normal ( std::ostream& output ) { return output << "\033[0m"; }
#endif

#ifndef colors
std::ostream& red    ( std::ostream& output ) { return output; }
std::ostream& green  ( std::ostream& output ) { return output; }
std::ostream& yellow ( std::ostream& output ) { return output; }
std::ostream& blue   ( std::ostream& output ) { return output; }
std::ostream& magenta( std::ostream& output ) { return output; }
std::ostream& cyan   ( std::ostream& output ) { return output; }
std::ostream& white  ( std::ostream& output ) { return output; }
std::ostream& normal ( std::ostream& output ) { return output; }
#endif

std::ostream& indent ( std::ostream& output ) {
    for (int i = 0; i < Log::indent_level; i++) {
        output << "    ";
    }    
    return output; 
}