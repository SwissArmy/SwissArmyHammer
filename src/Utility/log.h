#ifndef LOG_H
#define LOG_H

// C++ includes
#include <string>
#include <iostream>
#include <sstream>

// Trace function entry and exit
#define LOG_ENTER() Log::enter(__PRETTY_FUNCTION__, __LINE__)
#define LOG_EXIT()  Log::exit(__PRETTY_FUNCTION__, __LINE__)

class Log {
public:
    Log() {}
    ~Log();
    
    std::ostringstream& trace();
    std::ostringstream& debug();
    std::ostringstream& info();
    std::ostringstream& warning();
    std::ostringstream& error();
    std::ostringstream& fatal();

    static void enter(std::string function, int line);
    static void exit(std::string function, int line);
    static int indent_level;
private:
    std::ostringstream output;
    
    // hide copy & assignment
    Log(const Log&);
    Log& operator =(const Log&);
};

std::string classname(const char* name);

#endif