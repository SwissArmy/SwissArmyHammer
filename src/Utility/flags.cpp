#include "flags.h"

using namespace std;

std::string flagsToString(u32 flags) {
    char flagstring[33];
    
    for (int i = 0; i < 32; i++) {
        if (flags & (1 << i))
            flagstring[i] = '1';
        else
            flagstring[i] = '0';
    }
    flagstring[32] = '\0';
    
    return std::string(flagstring, 32);
};
