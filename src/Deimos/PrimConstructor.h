#ifndef DEIMOS_PRIM_CONSTRUCTOR_H
#define DEIMOS_PRIM_CONSTRUCTOR_H

#include "Phobos/Model.h"

#include "types.h"

namespace Deimos {

//! A utility class that provides static methods for creating primitive 3D
//! objects such as spheres, cylinders, cubes, etc.
//! It also provides methods to combine meshes together into a single new mesh.
class PrimConstructor {
public:
	static std::shared_ptr<Phobos::Model> cylinder(Scalar length, u32 detail_level, Scalar radius);
	static std::shared_ptr<Phobos::Model> cylinder(Scalar length, u32 detail_level, Scalar near_radius, Scalar far_radius);
    static std::shared_ptr<Phobos::Model> cylinder(Scalar length, u32 detail_level, Scalar near_radius, Scalar far_radius, Vector2D end_translation);
    static std::shared_ptr<Phobos::Model> cube(Scalar size);
    static std::shared_ptr<Phobos::Model> sphere(Scalar radius);
    static std::shared_ptr<Phobos::Model> leaf(u32 detail_level);

    static std::shared_ptr<Phobos::Model> Union(const std::shared_ptr<Phobos::Model> m1, const std::shared_ptr<Phobos::Model> m2);
};

}

#endif
