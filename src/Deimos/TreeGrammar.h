#ifndef DEIMOS_TREE_GRAMMAR_H
#define DEIMOS_TREE_GRAMMAR_H

#include "Grammar.h"

namespace Deimos {

class TreeGrammar : public Grammar {
public:
    const std::list<FunctionNode> evaluate(u32 seed, u32 iterations);
    ~TreeGrammar() {}
private:
    const std::list<FunctionNode> evaluate(const std::list<FunctionNode> &current, u32 iterations, u32 depth);
    void addBranches(std::list<FunctionNode> &next, int num_branches, Scalar tip_diameter);
    void addLeaves(std::list<FunctionNode> &next, int num_leaves);
};

}

#endif
