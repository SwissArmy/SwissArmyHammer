#include <iostream>

#include "PrimConstructor.h"

//! \deprecated
const Scalar PI = 3.14159265357;

using namespace std;
using namespace Phobos;

namespace Deimos {

shared_ptr<Model> PrimConstructor::cylinder(Scalar length, u32 detail_level, Scalar radius) {
    return PrimConstructor::cylinder(length, detail_level, radius, radius, Vector2D(0,0));
}

shared_ptr<Model> PrimConstructor::cylinder(Scalar length, u32 detail_level, Scalar near_radius, Scalar far_radius) {
    return PrimConstructor::cylinder(length, detail_level, near_radius, far_radius, Vector2D(0,0));
}

shared_ptr<Model> PrimConstructor::cylinder(Scalar length, u32 detail_level, Scalar near_radius, Scalar far_radius, Vector2D end_translation) {

    shared_ptr<Model> m(new Model());
    m->frames.resize(1);
    Frame* frame = &m->frames[0];

    const Scalar step = (2 * PI) / detail_level;
    // this generates the points for the near circle
    for (Scalar i = 0; i <= (2*PI); i += step) {
        frame->vertices.push_back(Vertex(cos(i),sin(i),0) * near_radius);
    }

    // and this generates the triangles for it
    const int circle_points = frame->vertices.size();
    for (int i = 1; i < circle_points - 1; i++) {
        Triangle t(i+1, i, 0);
        m->addTriangle(t);
    }

    // now do it again for the far one
    for (f64 i = 0; i <= (2 * PI); i += step) {
        Vertex v(cos(i), sin(i), length);
        v *= Vector(far_radius, far_radius, 1);
        v += Vector(end_translation, 0);
        frame->vertices.push_back(v);
    }

    for (u32 i = circle_points + 1; i < frame->vertices.size() - 1; i++) {
        Triangle t(circle_points, i, i+1);
        m->addTriangle(t);
    }

    // now generate the in-between triangles
    for (int i = 0; i < circle_points - 1; i++) {
        Triangle t1(i, i+1, i + circle_points);                      // CCW winding
        Triangle t2(i+1, i + circle_points + 1, i + circle_points);
        m->addTriangle(t1);
        m->addTriangle(t2);
    }

    if (circle_points % 2 == 0) {
        m->addTriangle(Triangle(circle_points - 1, 0, (circle_points * 2) - 1));
        m->addTriangle(Triangle(0, circle_points, circle_points + circle_points - 1));
    }
    return m;
}

shared_ptr<Model> PrimConstructor::cube(Scalar side_length) {
    side_length /= 2;
    shared_ptr<Model> m(new Model());
    m->frames.resize(1);
    Frame* frame = &m->frames[0];

    frame->vertices.push_back(Vertex( side_length,  side_length,  side_length)); // 0
    frame->vertices.push_back(Vertex(-side_length,  side_length,  side_length)); // 1
    frame->vertices.push_back(Vertex( side_length, -side_length,  side_length)); // 2
    frame->vertices.push_back(Vertex(-side_length, -side_length,  side_length)); // 3
    frame->vertices.push_back(Vertex( side_length,  side_length, -side_length)); // 4
    frame->vertices.push_back(Vertex(-side_length,  side_length, -side_length)); // 5
    frame->vertices.push_back(Vertex( side_length, -side_length, -side_length)); // 6
    frame->vertices.push_back(Vertex(-side_length, -side_length, -side_length)); // 7

    // front
    m->addTriangle(Triangle(0, 1, 2));
    m->addTriangle(Triangle(1, 3, 2));

    // left side
    m->addTriangle(Triangle(1, 5, 3));
    m->addTriangle(Triangle(3, 5, 7));

    // bottom
    m->addTriangle(Triangle(2, 3, 6));
    m->addTriangle(Triangle(3, 7, 6));

    // left side
    m->addTriangle(Triangle(0, 2, 6));
    m->addTriangle(Triangle(0, 6, 4));

    // back
    m->addTriangle(Triangle(4, 7, 5));
    m->addTriangle(Triangle(4, 6, 7));

    // bottom
    m->addTriangle(Triangle(0, 5, 1));
    m->addTriangle(Triangle(0, 4, 5));

    return m;
}

shared_ptr<Model> PrimConstructor::leaf(u32 detail_level) {
    shared_ptr<Model> m(new Model());
    m->frames.resize(1);
    Frame* frame = &m->frames[0];
    frame->vertices.push_back(Vertex(0,0,0));
    frame->vertices.push_back(Vertex(1,0,0));
    frame->vertices.push_back(Vertex(0,1,-1));
    frame->vertices.push_back(Vertex(0,-1,-1));

    m->addTriangle(Triangle(0,1,2));
    m->addTriangle(Triangle(0,3,1));
    m->addTriangle(Triangle(0,2,1));
    m->addTriangle(Triangle(0,1,3));

    return m;
}

shared_ptr<Model> PrimConstructor::Union(const shared_ptr<Model> m1, const shared_ptr<Model> m2) {
    assert(m1->frames.size() == m2->frames.size());

    shared_ptr<Model> result(new Model());
    result->frames.resize(m1->frames.size());

    for (u32 f = 0; f < m1->frames.size(); f++) {
        for (u32 v = 0; v < m1->frames[f].vertices.size(); v++) {
            result->frames[f].vertices.push_back( m1->frames[f].vertices[v] );
        }
    }

    for (u32 i = 0; i < m1->triangleCount(); i++) {
        result->addTriangle(m1->triangle(i));
    }

    for (u32 f = 0; f < m2->frames.size(); f++) {
        for (u32 v = 0; v < m2->frames[f].vertices.size(); v++) {
            result->frames[f].vertices.push_back( m2->frames[f].vertices[v] );
        }
    }

    u32 v_count = m1->frames[0].vertices.size();

    if (m1->frames[0].vertices.size() == 0) {
        v_count = 0;
    }

    for (u32 i = 0; i < m2->triangleCount(); i++) {
        Triangle new_t(m2->triangle(i).vertices[0] + v_count, m2->triangle(i).vertices[1] + v_count, m2->triangle(i).vertices[2] + v_count);
        result->addTriangle(new_t);
    }

    return result;
}

}