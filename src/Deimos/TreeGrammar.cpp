#include <iostream>

#include "TreeGrammar.h"

#include "Utility/Random.h"

using namespace std;

namespace Deimos {

// Frequency-related parameters.
const int TRUNK_BRANCHES_MIN = 10;   //!< Maximum number of branches that can grow from the trunk
const int TRUNK_BRANCHES_MAX = 30;
const int LEAF_CHANCE = 50;      //!< Percent chance that a branch will end in leaves or keep growing.
const int LEAF_COUNT_MIN = 5;
const int LEAF_COUNT_MAX = 10;
const int BRANCH_COUNT_MIN = 1;
const int BRANCH_COUNT_MAX = 3;
const int BRANCH_DEPTH_MAX = 10;

// Function argument related parameters.
// Rotation
const Scalar YAW_ANGLE_MIN = -45.0;
const Scalar YAW_ANGLE_MAX = 45.0;
const Scalar PITCH_ANGLE_MIN = -45.0;
const Scalar PITCH_ANGLE_MAX = 45.0;
const Scalar ROLL_ANGLE_MIN = 0.0;
const Scalar ROLL_ANGLE_MAX = 0.0;
// Branches
const Scalar BRANCH_LENGTH_MIN = 15.0;
const Scalar BRANCH_LENGTH_MAX = 35.0;
const Scalar BRANCH_BASE_DIAMETER = 0.3;
const Scalar BRANCH_END_TRANSLATION_MIN = 0.1;
const Scalar BRANCH_END_TRANSLATION_MAX = 0.5;
const Scalar BRANCH_TIP_DIAMETER_MIN = 0.1;
// Trunk
const Scalar TRUNK_LENGTH_MIN = 10.0;
const Scalar TRUNK_LENGTH_MAX = 20.0;
const Scalar TRUNK_DIAMETER_MIN = 1.0;
const Scalar TRUNK_DIAMETER_MAX = 2.0;
// Leaves
const Scalar LEAF_JITTER_MIN = 0.0;
const Scalar LEAF_JITTER_MAX = 0.0;
const Scalar LEAF_SLIDE_MIN = 0.3;
const Scalar LEAF_SLIDE_MAX = 0.5;

const list<FunctionNode> TreeGrammar::evaluate(u32 seed, u32 iterations) {
    srand(seed);
    list<FunctionNode> start;
    start.push_back(FunctionNode("seed"));
    return evaluate(start, iterations, 0);
}

//! This function defines the grammar for tree-based L-System
const list<FunctionNode> TreeGrammar::evaluate(const list<FunctionNode> &current, u32 iterations, u32 depth) {

    if (iterations <= 0) {
        return current;
    }

    list<FunctionNode> next;

    // +---------+
    // | Grammar |
    // +---------+
    //
    // +-----------+
    // | Terminals |
    // +-----------+---------------------+--------------------------------------------+
    // | Name      | Description         | Arguments                                  |
    // +-----------+---------------------+--------------------------------------------+
    // | trunk     | draws a trunk       | trunk_height, trunk_diameter               |
    // | branch    | draws a branch      | branch_length, near_diameter, far_diameter |
    // | leaves    | draws some leaves   |                                            |
    // | translate | moves to a location | x, y, z                                    |
    // | rotate    | rotates by an angle | yaw, pitch, roll                           |
    // | scale     | scales by a volume  | x, y, z                                    |
    // | push      | push the matrix     |                                            |
    // | pop       | pop the matrix      |                                            |
    // +-----------+---------------------+--------------------------------------------+
    //
    // +-----------+
    // | Variables |
    // +-----------+--+-----------------+----------------------+
    // | Name         | Description     | Arguments            |
    // +--------------+-----------------+----------------------+
    // | seed         | Axiom           |                      |
    // | branch-tip   | End of a branch | branch_near_diameter |
    // +--------------+-----------------+----------------------+
    //
    // +----------------+
    // | Variable rules |
    // +----------------+-------------------------+
    // | Name            Turns Into               |
    // +------------------------------------------+
    // | seed         -> trunk + [1-n] branches   |
    // | branch-tip   -> leaves OR [1-n] branches |
    // +------------------------------------------+

    for (const FunctionNode &n : current) {
        if (n.type.compare("seed") == 0) {
            // add the trunk itself
            FunctionNode trunk("trunk");
            const Scalar length = Random::ScalarValue(TRUNK_LENGTH_MIN, TRUNK_LENGTH_MAX);
            trunk.add_arg("length", length);
            trunk.add_arg("diameter", Random::ScalarValue(TRUNK_DIAMETER_MIN, TRUNK_DIAMETER_MAX));
            next.push_back(trunk);
            next.push_back(FunctionNode("push"));
            // move ourselves to the top of the tree
            FunctionNode translate("translate");
            translate.add_arg("x", 0);
            translate.add_arg("y", 0);
            translate.add_arg("z", length * 0.9);
            next.push_back(translate);
            addBranches(next, Random::IntValue(TRUNK_BRANCHES_MIN, TRUNK_BRANCHES_MAX), BRANCH_BASE_DIAMETER);
            next.push_back(FunctionNode("pop"));
        } else if (n.type.compare("branch-tip") == 0) {
            FunctionNode x(n);
            if (x.args["tip-diameter"] < BRANCH_TIP_DIAMETER_MIN || depth > BRANCH_DEPTH_MAX) {
                int num_leaves = Random::IntValue(LEAF_COUNT_MIN, LEAF_COUNT_MAX);
                addLeaves(next, num_leaves);
            } else {
                int num_branches = Random::IntValue(BRANCH_COUNT_MIN, BRANCH_COUNT_MAX);
                addBranches(next, num_branches, x.args["tip-diameter"]);
            }
        } else {
            next.push_back(n);
        }
    }

    return evaluate(next, iterations - 1, depth+1);
}

void TreeGrammar::addLeaves(list<FunctionNode> &next, int num_leaves) {
    for (int i = 0; i < num_leaves; i++) {
        next.push_back(FunctionNode("push"));
        FunctionNode translate("translate");
        translate.add_arg("x", Random::ScalarValue(LEAF_JITTER_MIN, LEAF_JITTER_MAX));
        translate.add_arg("y", Random::ScalarValue(LEAF_JITTER_MIN, LEAF_JITTER_MAX));
        translate.add_arg("z", Random::ScalarValue(LEAF_SLIDE_MIN, LEAF_SLIDE_MAX));
        next.push_back(translate);
        next.push_back(FunctionNode("leaves"));
        next.push_back(FunctionNode("pop"));
    }
}

void TreeGrammar::addBranches(list<FunctionNode> &next, int num_branches, Scalar tip_diameter) {
    for (int i = 0; i < num_branches; i++) {
        const Scalar yaw = Random::ScalarValue(YAW_ANGLE_MIN, YAW_ANGLE_MAX);
        const Scalar pitch = Random::ScalarValue(PITCH_ANGLE_MIN, PITCH_ANGLE_MAX);
        //const Scalar roll = Random::ScalarValue(ROLL_ANGLE_MIN, ROLL_ANGLE_MAX);
        FunctionNode rotation("rotate");
        rotation.add_arg("yaw", yaw);
        rotation.add_arg("pitch", pitch);
        rotation.add_arg("roll", 0);
        FunctionNode branch("branch");
        const Scalar branch_length = Random::ScalarValue(BRANCH_LENGTH_MIN * tip_diameter, BRANCH_LENGTH_MAX * tip_diameter);
        const Scalar branch_near_diameter = tip_diameter;
        const Scalar branch_far_diameter = Random::ScalarValue(BRANCH_TIP_DIAMETER_MIN * 0.9, tip_diameter);
        const Scalar end_translation_x = Random::ScalarValue(BRANCH_END_TRANSLATION_MIN, BRANCH_END_TRANSLATION_MAX);
        const Scalar end_translation_y = Random::ScalarValue(BRANCH_END_TRANSLATION_MIN, BRANCH_END_TRANSLATION_MAX);
        branch.add_arg("length", branch_length);
        branch.add_arg("near_diameter", branch_near_diameter);
        branch.add_arg("far_diameter", branch_far_diameter);
        branch.add_arg("end_translation_x", end_translation_x);
        branch.add_arg("end_translation_y", end_translation_y);

        next.push_back(FunctionNode("push"));
        next.push_back(rotation);
        next.push_back(branch);
        next.push_back(FunctionNode("push"));
            FunctionNode translate("translate");
            // here we calculate the vector that we need to translate along
            translate.add_arg("x", end_translation_x);
            translate.add_arg("y", end_translation_y);
            translate.add_arg("z", branch_length * 0.5);
            next.push_back(translate);
            FunctionNode branch_tip("branch-tip");
            branch_tip.add_arg("tip-diameter", branch_far_diameter);
            next.push_back(branch_tip);
        next.push_back(FunctionNode("pop"));
        next.push_back(FunctionNode("pop"));
    }
}

}