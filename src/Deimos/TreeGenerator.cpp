#include <iostream>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "TreeGenerator.h"
#include "PrimConstructor.h"

#include "Phobos/MatrixStack.h"

using namespace std;
using namespace Phobos;

namespace Deimos {

//! \deprecated
const Scalar PI = 3.14159265357;

const int TRUNK_DETAIL = 20;
const int BRANCH_DETAIL = 10;
const int LEAF_DETAIL = 10;

vector<shared_ptr<Model> > TreeGenerator::generateModel(u32 seed, u32 iterations) {
    assert(grammar != NULL);

    list<FunctionNode> nodes = grammar->evaluate(seed, iterations);

    MatrixStack m_stack;
    shared_ptr<Model> m(new Model);
    shared_ptr<Model> l(new Model);
    m->frames.resize(1);
    l->frames.resize(1);
    int leafcount = 0;
    // +-----------+
    // | Terminals |
    // +-----------+---------------------+--------------------------------------------+
    // | Name      | Description         | Arguments                                  |
    // +-----------+---------------------+--------------------------------------------+
    // | trunk     | draws a trunk       | trunk_height, trunk_diameter               |
    // | branch    | draws a branch      | branch_length, near_diameter, far_diameter |
    // | leaves    | draws some leaves   |                                            |
    // | translate | moves to a location | x, y, z                                    |
    // | rotate    | rotates by an angle | yaw, pitch, roll                           |
    // | scale     | scales by a volume  | x, y, z                                    |
    // | push      | push the matrix     |                                            |
    // | pop       | pop the matrix      |                                            |
    // +-----------+---------------------+--------------------------------------------+

    for (FunctionNode &n : nodes) {
        if (n.type.compare("trunk") == 0) {
            cout << "t";
            const shared_ptr<Model> t = PrimConstructor::cylinder(n.args["length"], TRUNK_DETAIL, n.args["diameter"]);
            t->transform(m_stack.top());
            const shared_ptr<Model> c = PrimConstructor::Union(m, t);
            m = c;
        } else if (n.type.compare("branch") == 0) {
            cout << "b";
            const shared_ptr<Model> b = PrimConstructor::cylinder(n.args["length"], BRANCH_DETAIL, n.args["near_diameter"], n.args["far_diameter"], Vector2D(n.args["end_translation_x"],n.args["end_translation_y"]));
            b->transform(m_stack.top());
            const shared_ptr<Model> c = PrimConstructor::Union(m, b);
            m = c;
        } else if (n.type.compare("leaves") == 0) {
            cout << "l";
            const shared_ptr<Model> leaf = PrimConstructor::leaf(LEAF_DETAIL);
            leaf->transform(m_stack.top());
            const shared_ptr<Model> c = PrimConstructor::Union(l, leaf);
            leafcount++;
            l = c;
        } else if (n.type.compare("translate") == 0) {
            cout << "m";
            const Matrix tm = glm::translate(m_stack.pop(), Vector(n.args["x"], n.args["y"], n.args["z"]));
            m_stack.push(tm);
        } else if (n.type.compare("rotate") == 0) {
            cout << "r";
            Matrix rm = glm::rotate(m_stack.pop(), n.args["yaw"], Vector(0, 1, 0));
            rm = glm::rotate(rm, n.args["pitch"], Vector(1,0,0));
            rm = glm::rotate(rm, n.args["roll"], Vector(0,0,1));
            m_stack.push(rm);
        } else if (n.type.compare("scale") == 0) {
            cout << "s";
            const Matrix sm = glm::scale(m_stack.pop(), Vector(n.args["x"], n.args["y"], n.args["z"]));
            m_stack.push(sm);
        } else if (n.type.compare("push") == 0) {
            cout << "[";
            m_stack.push(m_stack.top());
        } else if (n.type.compare("pop") == 0) {
            cout << "]";
            m_stack.pop();
        } else if (n.type.compare("branch-tip") == 0) {
            cout << "^";
        }
    }
    cout << endl;
    // TODO: rewrite this to use trigroups, materials, etc.
    //m->color = Vector(.25, .15, .09);
    //l->color = Vector(.15, .55, .15);
    vector<shared_ptr<Model> > r;
    r.push_back(m);
    r.push_back(l);
    return r;
}

}