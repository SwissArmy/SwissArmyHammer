#ifndef DEIMOS_GENERATOR_H
#define DEIMOS_GENERATOR_H

#include <string>

#include "Grammar.h"

#include "types.h"

namespace Deimos {

class Generator {
public:
    Generator() { grammar = NULL; }
    ~Generator() { setGrammar(NULL); }

    bool setGrammar(Grammar* g) {
        if (grammar != NULL) {
            delete grammar;
        }
        grammar = g;
        return true;
    }
protected:
    Grammar* grammar;
};

}

#endif
