#ifndef DEIMOS_TREE_GENERATOR_H
#define DEIMOS_TREE_GENERATOR_H

#include <vector>
#include <memory>

#include "Phobos/Model.h"

#include "Generator.h"

namespace Deimos {

class TreeGenerator : public Generator {
public:
    std::vector<std::shared_ptr<Phobos::Model>> generateModel(u32 seed, u32 iterations);
private:
};

}

#endif
