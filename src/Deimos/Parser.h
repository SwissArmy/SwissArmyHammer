#ifndef DEIMOS_PARSER_H
#define DEIMOS_PARSER_H

#include <vector>

#include "types.h"

namespace Deimos {

class Parser {
public:
    static u32 toNumber(std::vector &v) const {
        u32 result
        for (u32 i = 0; i < v.size(); i++) {
            result += v[i] * pow(10, i + v.size() - 1);
        }
        return result;
    }
};

}

#endif // PARSER_H
