#ifndef DEIMOS_GRAMMAR_H
#define DEIMOS_GRAMMAR_H

#include <string>
#include <map>
#include <list>

#include "types.h"

namespace Deimos {

class FunctionNode {
public:
    FunctionNode(std::string t) { type = t; }

    void add_arg(std::string name, Scalar arg) {
        args.insert(std::pair<std::string, Scalar>(name, arg));
    }

    std::string type;
    std::map<std::string, Scalar> args;
};

class Grammar {
public:
    virtual const std::list<FunctionNode> evaluate(u32 seed, u32 iterations) = 0;
    virtual ~Grammar() {}
};

}

#endif // GRAMMAR_H
