#include "MatrixStack.h"

using namespace std;

namespace Phobos {

MatrixStack::MatrixStack() {
    matrix_stack.push(Matrix(1.0));
}

bool MatrixStack::push(Matrix m) {
    matrix_stack.push(m);
    return true;
}

Matrix MatrixStack::pop() {
    if (matrix_stack.size() == 1) {
        return matrix_stack.top();
    } else {
        const Matrix m = matrix_stack.top();
        matrix_stack.pop();
        return m;
    }
}

Matrix MatrixStack::top() const {
    return matrix_stack.top();
}

}