#ifndef PHOBOS_SKIN_H
#define PHOBOS_SKIN_H

#include <vector>

#include "Texture.h"

namespace Phobos {

//! A skin consists of a single texture. Each model can have multiple skins.
class Skin {
public:
    std::vector<Texture> textures;
};

}

#endif
