#ifndef PHOBOS_ZONE_H
#define PHOBOS_ZONE_H

#include <vector>

namespace Phobos {

class Zone {
public:
    std::vector<ZoneObject> objects;
    std::vector<ZoneLights> lights;
    BSPTree* bspTree;
};

}

#endif