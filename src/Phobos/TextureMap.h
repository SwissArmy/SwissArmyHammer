#ifndef PHOBOS_TEXTURE_MAP_H
#define PHOBOS_TEXTURE_MAP_H

#include <vector>
#include <glm/glm.hpp>

#include "Skin.h"

namespace Phobos {

typedef glm::vec2 TextureCoordinate;

class TextureMap {
public:
    std::vector<Skin> skins;
    std::vector<TextureCoordinate> texture_coordinates;
};

}

#endif
