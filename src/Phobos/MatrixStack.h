#ifndef PHOBOS_MATRIX_STACK_H
#define PHOBOS_MATRIX_STACK_H

#include <stack>

#include <glm/glm.hpp>

#include "types.h"

namespace Phobos {

class MatrixStack {
public:
    MatrixStack();

    bool push(Matrix m);
    Matrix pop();
    Matrix top() const;
private:
    std::stack<Matrix> matrix_stack;
};

}

#endif
