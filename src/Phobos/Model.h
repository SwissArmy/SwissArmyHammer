#ifndef PHOBOS_MODEL_H
#define PHOBOS_MODEL_H

#include <memory>
#include <vector>
#include <map>

#include <glm/glm.hpp>

#include "TextureMap.h"

namespace Phobos {

// ############
// # Typedefs #
// ############

typedef Vector Vertex;
typedef Vector Normal;
typedef glm::dvec2 TexCoord;

// ###########
// # Structs #
// ###########

struct Triangle {
    Triangle() {
        vertices[0] = 0;
        vertices[1] = 0;
        vertices[2] = 0;
        texCoords[0] = 0;
        texCoords[1] = 0;
        texCoords[2] = 0;
    }

    Triangle(u32 p1, u32 p2, u32 p3) {
        vertices[0] = p1;
        vertices[1] = p2;
        vertices[2] = p3;
        texCoords[0] = 0;
        texCoords[1] = 0;
        texCoords[2] = 0;
    }

    u32 vertices[3];   // Indices to the vertices making up the points of the triangle.
    u32 texCoords[3];  // Indices to the texture coordinates used by each vertex.
};

struct Frame {
    std::vector<Vertex> vertices;
    std::vector<Normal> vertex_normals;
    std::vector<std::vector<Normal>> face_normal_groups;
};

struct GroupProperties {
    std::string name;   //!< not guaranteed to be unique
    u32 texture;
    u32 smoothingGroups; //!< A bitfield that indicates which of the 32 smoothing groups this group belongs to.
};

//! \brief Stores mesh, animation, and texture data for conversion and rendering.
//! \details
//! This class focuses on flexibility over optimization.\n
//! A Model contains frames, triangle groups, and textures
//! Each frame contains vertices and normals.
//! Each group contains properties including face normals and texture properties.
class Model {
public:
    // vars
    std::vector<Frame> frames;
    std::vector<TexCoord> textureCoordinates;
    std::string name;

    // ctors
    Model();

    // funs
    Triangle& triangle(u32 i) { return triangles[i]; }
    u32 triangleCount() { return triangles.size(); }
    void addTriangle(const Triangle &t, u32 group=0);

    void addTexture(const std::shared_ptr<Texture> texture, const std::string &name);

    void setGroupName(u32 group, const std::string &name);
    void setGroupTexture(u32 group, const std::string &texture);
    void setGroupSmoothing(u32 group, u32 smoothing);

    const Vertex getCenter(u32 frame=0) const;
    const Scalar getDiameter(u32 frame=0) const;

    void transform(Matrix m);
    void rotate(Scalar x_angle, Scalar y_angle=0, Scalar z_angle=0);
    void translate(Vector translation);
    void scale(Vector scale);

    void calculateAllNormals();
    void calculateVertexNormals();
    void calculateFaceNormals();

    //! \deprecated
	void clean();

    std::vector<std::vector<Triangle>> triGroups;
    std::vector<GroupProperties> groupProperties;
    const Vertex faceCenter(const Triangle &t, const Frame &f) const;
    std::vector<std::shared_ptr<Texture>> textures;
protected:
    // Triangle groups and related data.
    std::vector<Triangle> triangles;                //!< \deprecated


    // Texture related data.
    // TODO: Instead of storing textures as part of the model,
    // they should be stored by a Resource Manager and referred to by
    // the model.
    std::map<std::string, u32> textureNames;
private:
    // helper functions for normal calculation
    const Normal faceNormal(const Triangle &t, const Frame &f) const;
};

}

#endif
