#ifndef PHOBOS_ZONE_OBJECT_H
#define PHOBOS_ZONE_OBJECT_H

#include <memory>

#include <glm/glm.hpp>

#include "Model.h"

namespace Phobos {

class ZoneObject {
public:
    std::shared_ptr<Model> model;
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;
};

}

#endif
