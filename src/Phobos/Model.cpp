#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "Utility/log.h"

#include "Model.h"

using namespace std;

namespace Phobos {

Model::Model() {
    // initialize texture list with default texture
    shared_ptr<Texture> texture(new Texture());
    texture->name = "default";
    texture->ambient.r = 0.5;
    texture->ambient.g = 0.5;
    texture->ambient.b = 0.5;
    addTexture(texture, texture->name);
}

void Model::addTriangle(const Triangle &t, u32 group) {
    if (triGroups.size() <= group) {
        triGroups.resize(group + 1);
        groupProperties.resize(group + 1);
    }
    triangles.push_back(t);
    triGroups[group].push_back(t);
}

void Model::addTexture(const shared_ptr<Texture> texture, const string &name) {
    textures.push_back(texture);
    textureNames[name] = textures.size() - 1;
}

void Model::setGroupName(u32 group, const std::string &name) {
    if (groupProperties.size() <= group) {
        groupProperties.resize(group + 1);
    }
    groupProperties[group].name = name;
}

void Model::setGroupTexture(u32 group, const std::string &texture) {
    if (groupProperties.size() <= group) {
        groupProperties.resize(group + 1);
    }
    groupProperties[group].texture = textureNames[texture];
}

void Model::setGroupSmoothing(u32 group, u32 smoothing) {
    if (groupProperties.size() <= group) {
        groupProperties.resize(group + 1);
    }
    groupProperties[group].smoothingGroups = smoothing;
}

// \todo add test for this function
const Vertex Model::getCenter(u32 frame) const {
    Vertex center;
    const Scalar size = frames[frame].vertices.size();
    for (const Vertex &v : frames[frame].vertices) {
        center += v / size;
    }
    return center;
}

// \todo add test for this function
const Scalar Model::getDiameter(u32 frame) const {
    Scalar diameter = 0;
    for (const Vertex &v : frames[frame].vertices) {
        const Scalar m = glm::length(v);
        if (m > diameter) {
            diameter = m;
        }
    }
    return diameter;
}

void Model::transform(Matrix m) {
    for (Frame &f : frames) {
        for (Vertex &v : f.vertices) {
            Quaternion q =  m * Quaternion(v, 1);
            v.x = q.x;
            v.y = q.y;
            v.z = q.z;
        }
    }
}

//! \bug This function doesn't rotate the face normals.
void Model::rotate(Scalar yaw, Scalar pitch, Scalar roll) {
    for (Frame &f : frames) {
        for (Vertex &v : f.vertices) {
            v = glm::rotateX(v, pitch);
            v = glm::rotateY(v, yaw);
            v = glm::rotateZ(v, roll);
        }
        for (Normal &n : f.vertex_normals) {
            n = glm::rotateX(n, pitch);
            n = glm::rotateY(n, yaw);
            n = glm::rotateZ(n, roll);
        }
    }
}

void Model::translate(Vector translation) {
    for (Frame &f : frames) {
        for (Vertex &v : f.vertices) {
            v += translation;
        }
    }
}

void Model::scale(Vector scale) {
    for (Frame &f : frames) {
        for (Vertex &v : f.vertices) {
            v.x *= scale.x;
            v.y *= scale.y;
            v.z *= scale.z;
        }
    }
}

void Model::calculateAllNormals() {
    calculateFaceNormals();
    calculateVertexNormals();
}

//! \pre Face normals must already be calculated or invalid results will be produced.
//! \pre This function assumes that all frames have the same number of vertices.
void Model::calculateVertexNormals() {
    assert(frames.size() >= 1);

    // Create vertex face lists.
    map<u32, vector<pair<u32, u32>>> vertex_face_lists; // map vertex IDs to list of pair<trigroup ID, triangle ID>
    for (u32 g = 0; g < triGroups.size(); g++) {
        for (u32 t = 0; t < triGroups[g].size(); t++) {
            vertex_face_lists[triGroups[g][t].vertices[0]].push_back(pair<u32,u32>(g,t));
            vertex_face_lists[triGroups[g][t].vertices[1]].push_back(pair<u32,u32>(g,t));
            vertex_face_lists[triGroups[g][t].vertices[2]].push_back(pair<u32,u32>(g,t));
        }
    }

    // TODO: To support smoothing groups, write the following pseudocode
    // For each vertex:
    //   If any of the triangles in the vertex's face list are from different smoothing groups:
    //     Create new vertices at the same location as the old vertex,
    //     and point the triangles from different smoothing groups at them, so that
    //     tri

    // \bug This doesn't work right when multiple triangles are in the same plane (i.e. on the faces of a cube.)
    for (Frame &f : frames) {
        f.vertex_normals.clear();
        f.vertex_normals.resize(f.vertices.size());

        // Calculate the normals.
        for (const auto &triGroup : triGroups) {
            for (const Triangle &t : triGroup) {
                const Normal fn = faceNormal(t, f);
                f.vertex_normals[t.vertices[0]] += fn;
                f.vertex_normals[t.vertices[1]] += fn;
                f.vertex_normals[t.vertices[2]] += fn;
            }
        }

        for (auto &v : f.vertex_normals) {
            v = glm::normalize(v);
        }
    }
}

void Model::calculateFaceNormals() {
    for (Frame &f : frames) {
        f.face_normal_groups.clear();
        f.face_normal_groups.resize(triGroups.size());

        auto faceGroup = f.face_normal_groups.begin();
        for (const auto &triGroup : triGroups) {
            for (const Triangle &t : triGroup) {
                const Normal fn = faceNormal(t, f);
                if (glm::length(fn) == 0) {
                    Log().warning() << "Encountered a face normal with length zero.";
                    faceGroup->push_back(fn);
                } else {
                    const Normal n = glm::normalize(fn);
                    faceGroup->push_back(n);
                }
            }
            faceGroup++;
        }
    }
}

const Vertex Model::faceCenter(const Triangle &t, const Frame &f) const {
    const Vertex v0 = f.vertices[t.vertices[0]];
    const Vertex v1 = f.vertices[t.vertices[1]];
    const Vertex v2 = f.vertices[t.vertices[2]];
    return Vertex((v0.x+v1.x+v2.x) / (Scalar) 3, (v0.y+v1.y+v2.y) / (Scalar) 3, (v0.z+v1.z+v2.z) / (Scalar) 3);
}

//! Calculates the face normal for a given triangle.
//! \note Does not normalize the normal, you must do that yourself.
const Normal Model::faceNormal(const Triangle & t, const Frame &f) const {
    const Vertex v1 = f.vertices[t.vertices[1]] - f.vertices[t.vertices[0]];
    const Vertex v2 = f.vertices[t.vertices[2]] - f.vertices[t.vertices[0]];
    return glm::cross(v1,v2);
}


// TODO: There is a bug in this code where it sometimes goes crazy
// and tries to delete non-existent vertices. It shouldn't be relied
// on until that bug is fixed. think I may have fixed this now.
//! \deprecated
void Model::clean() {
#ifdef DEBUG
    if (frames.size() < 1) {
        Log().error() << "Tried to clean a model with no frames." << endl;
        return;
    }
#endif
    const u32 v_count = frames[0].vertices.size();
    // TODO: rewrite this with for ( : )
    for (std::vector<Triangle>::iterator t = triangles.begin(); t != triangles.end(); t++) {
    	if (t->vertices[0] >= v_count) {
            Log().error() << "Triangle referenced vertex " << t->vertices[0] << " which did not exist. (There are only " << v_count << " vertices in the model.)";
        	t = triangles.erase(t);
    	} else if (t->vertices[1] >= v_count) {
            Log().error() << "Triangle referenced vertex " << t->vertices[1] << " which did not exist. (There are only " << v_count << " vertices in the model.)";
            t = triangles.erase(t);
        } else if (t->vertices[2] >= v_count) {
            Log().error() << "Triangle referenced vertex " << t->vertices[2] << " which did not exist. (There are only " << v_count << " vertices in the model.)";
        	t = triangles.erase(t);
    	}
	}
    calculateAllNormals();	// need to recalculate these if we deleted any triangles
}

}