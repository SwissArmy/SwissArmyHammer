#ifndef PHOBOS_TEXTURE_H
#define PHOBOS_TEXTURE_H

#include <SFML/OpenGL.hpp>

#include <string>

#include "types.h"

namespace Phobos {

struct Color {
    f32 r;
    f32 g;
    f32 b;
};

struct Texture {
    std::string name;
    GLuint texture_id = 0;
    Color ambient; //!< The ambient color of the material.
    Color diffuse;
};

}

#endif
