#ifndef PHOBOS_ZONE_LIGHT_H
#define PHOBOS_ZONE_LIGHT_H

#include <glm/glm.hpp>

namespace Phobos {

class ZoneLight {
public:
    glm::vec3 position;
    f32 r, g, b;
    f32 radiosity;
};

}

#endif
