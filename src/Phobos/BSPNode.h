#ifndef PHOBOS_BSP_NODE_H
#define PHOBOS_BSP_NODE_H

namespace Phobos {

class BSPNode {
public:
    s64 nodeID;
    glm::vec3 normals;
    f32 splitdistance;
    s64 region;
    s32 special;
    BSPNode* left;
    BSPNode* right;
private:
};

}

#endif
