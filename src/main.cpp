/**
\mainpage
Swiss Army Hammer is a 3D model viewer.
*/

// C++ Headers
#include <string>
#include <iostream>

// C Headers
#include <libusb-1.0/libusb.h>

// Mars
#include "Mars/Engine.h"

// Scribes
#include "Scribes/Registrar.h"
#include "Scribes/Everquest/S3D.h"
#include "Scribes/Everquest/WLD.h"
#include "Scribes/GuildWars/DAT.h"
#include "Scribes/Morrowind/ESM.h"
#include "Scribes/Minecraft/Anvil.h"
#include "Scribes/Quake2/MD2.h"
#include "Scribes/Standard/OBJ.h"

// Local includes
#include "UserInterface/CommandPrompt/Prompt.h"

#include "Utility/log.h"

using namespace std;

int main(const int argc, char* argv[]) {

    Prompt prompt;

    // TODO: libusb won't always be needed, lazy load it more intelligently
    libusb_init(nullptr);

    // Initialize scribe registry.
    Scribe::Registrar::add(make_shared<Scribe::Everquest::S3D>());
    Scribe::Registrar::add(make_shared<Scribe::Everquest::WLD>());
    Scribe::Registrar::add(make_shared<Scribe::GuildWars::DAT>());
    Scribe::Registrar::add(make_shared<Scribe::Morrowind::ESM>());
    Scribe::Registrar::add(make_shared<Scribe::Minecraft::Anvil>());
    Scribe::Registrar::add(make_shared<Scribe::Quake2::MD2>());
    Scribe::Registrar::add(make_shared<Scribe::Standard::OBJ>());

    if (argc <= 1) {
        Log().trace() << "No command line parameters found; running in GUI mode.";
        Mars::Engine().run();
        return 0;
    }

    // Read the command in from the command line.
    string verb = "load";
    vector<string> flags;
    vector<string> arguments;

    if (argc > 1) {
        verb = argv[1];
    }

    for (int i = 2; i < argc; i++) {
        if (argv[i][0] == '-') {
            flags.push_back(string(argv[i]));
        } else {
            arguments.push_back(string(argv[i]));
        }
    }

    return prompt.execute(verb, arguments, flags);
}
