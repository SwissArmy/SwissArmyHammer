//
// Created by nova on 10/20/24.
//

#ifndef MILIEU_H
#define MILIEU_H

#include <memory>
#include <Utility/log.h>

namespace Society {
    class Flaneur;
    class Bourgeoisie;

    /**
     * Like a context, but with set properties (union, intersect, etc)
     * Originally intended to facilitate binding scene graphs to Lua objects.
     */
    class Milieu {
    public:
        virtual ~Milieu() = default;

        /**
         * @param bourg
         * @return False if milieu is already owned by this or another bourgeoisie.
         */
        virtual bool bindOwner(const std::shared_ptr<Bourgeoisie> &bourg) {
            if (bourg == nullptr) {
                Log().warning() << "Tried to bind milieu " << this << " to a null bourgeoisie.";
                return false;
            }
            if (owner != nullptr) {
                Log().warning() << "Tried to bind milieu " << this << " to bourgeoisie " << bourg.get();
                return false;
            }

            owner = bourg;
            return true;
        }
    private:
        std::shared_ptr<Bourgeoisie> owner;
    };

    /**
     * A class that acts as the owner of milieux.
     * Milieu lifecycle should be bound to the owning bourgeoisie's lifecycle.
     * A bourgeoisie can have many milieux but a milieu can only belong to one bourgeoisie.
     */
    class Bourgeoisie {
    public:
        virtual ~Bourgeoisie() = default;

        /**
         * @param milieu
         * @return False if this milieu was already bound.
         */
        virtual bool possessMilieu(std::unique_ptr<Milieu> milieu);
        virtual bool registerFlaneur(std::shared_ptr<Flaneur> flaneur);
    };

    /**
     * Observes milieux, mediated by the bourgeoisie.
     */
    class Flaneur {
    public:
        virtual ~Flaneur() = default;
        virtual bool observe(std::shared_ptr<Bourgeoisie> bourg);
    };
}

#endif //MILIEU_H
