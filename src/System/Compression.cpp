#include <zlib.h>

#include "Compression.h"

int decompressZlib(char* inBuffer, char* outBuffer, int inLength, int outLength) {
    z_stream d_stream;

    d_stream.zalloc = (alloc_func)0;
    d_stream.zfree = (free_func)0;
    d_stream.opaque = (voidpf)0;

    d_stream.next_in  = (Bytef*) inBuffer;
    d_stream.avail_in = inLength;
    d_stream.next_out = (Bytef*) outBuffer;
    d_stream.avail_out = outLength;

    inflateInit(&d_stream);

    /**
     * inflate() returns
     * Z_OK if some progress has been made (more input processed or more output produced),
     * Z_STREAM_END if the end of the compressed data has been reached and all uncompressed output has been produced,
     * Z_NEED_DICT if a preset dictionary is needed at this point,
     * Z_DATA_ERROR if the input data was corrupted (input stream not conforming to the zlib format or incorrect check value),
     * Z_STREAM_ERROR if the stream structure was inconsistent (for example if next_in or next_out was NULL),
     * Z_MEM_ERROR if there was not enough memory,
     * Z_BUF_ERROR if no progress is possible or if there was not enough room in the output buffer when Z_FINISH is used.
     * Note that Z_BUF_ERROR is not fatal, and inflate() can be called again with more input and more output space to continue decompressing.
     * If Z_DATA_ERROR is returned, the application may then call inflateSync() to look for a good compression block if a partial recovery of the data is desired.
     */
    int status = inflate(&d_stream, Z_NO_FLUSH);

    inflateEnd(&d_stream);

    return status;
}