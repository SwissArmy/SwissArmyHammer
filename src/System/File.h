#ifndef FILE_H
#define FILE_H

#include <fstream>
#include <iostream>
#include <memory>

//! Represents a file in the UNIX sense of the word. Can be a stream connected
//! to a socket, a network location, a region of memory, or the contents of a
//! disk. The destructor will automatically close any open handles as necessary.
class File {
public:
    File() {}
    // TODO: disallow fstream here (should use openDiskSource instead)
    File(std::shared_ptr<std::iostream> _source, bool _is_binary=true) { source = _source; is_binary = _is_binary;}
    ~File() { closeSource(); }

    bool openDiskSource(std::shared_ptr<std::string> _filename);
    bool openDiskSourceBinary(std::shared_ptr<std::string> _filename);

    bool openUSBSource();

    bool isBinary() { return is_binary; }

    //! Returns true if the file is ready for reading and writing.
    bool ready() { return source->good();}

    //! Resets the read and write pointers to the beginning of the file.
    void reset() { source->seekg(0); source->seekp(0); }

    // Read functions
    template <class T>
    T read() { T data; source->read((char*) &data, sizeof(T)); return data; }
    std::streampos readPos() { return source->tellg(); }
    std::shared_ptr<std::istream> readSource() const { return source; }

    // Write functions
    std::shared_ptr<std::ostream> writeSource() const { return source; }

    //! Returns the base name of the file, with no directory path or file extension.
    std::string basename() const;
    std::string fileExtension() const;

    std::shared_ptr<std::string> filename;
private:
    void closeSource();

    // DATA
    std::shared_ptr<std::iostream> source;
    bool is_binary;
};

#endif
