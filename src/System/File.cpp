#include "File.h"

#include <libusb-1.0/libusb.h>

#include "Utility/log.h"

using namespace std;

bool File::openDiskSource(shared_ptr<string> _filename) {
    closeSource();
    source.reset(new fstream(_filename->c_str()));
    if (source->good()) {
        filename = _filename;
        is_binary = false;
        return true;
    } else {
        closeSource();
        source = nullptr;
        return false;
    }
}

bool File::openDiskSourceBinary(shared_ptr<string> _filename) {
    closeSource();
    source.reset(new fstream(_filename->c_str(), fstream::in | fstream::out | fstream::binary));
    if (source->good()) {
        filename = _filename;
        is_binary = true;
        return true;
    } else {
        closeSource();
        source = nullptr;
        return false;
    }
}

bool File::openUSBSource() {
  // TODO
  closeSource();

}

string File::basename() const {
    size_t start = filename->find_last_of('/');
    size_t end = filename->find_last_of('.');
    if (start == string::npos) {
        return filename->substr(0, end);
    } else {
        return filename->substr(start+1, end - (start+1));
    }
}

string File::fileExtension() const {
    size_t dotIndex = filename->find_last_of('.');
    if (dotIndex == string::npos || dotIndex == (filename->length() - 1)) {
        dotIndex = 0;
    }
    // TODO: set tolower before returning
    return filename->substr(dotIndex + 1, filename->length());
}

void File::closeSource() {
    // if the source stream is a file, make sure to close it on exit
    shared_ptr<fstream> file = dynamic_pointer_cast<fstream>(source);
    if (file && file->is_open()) {
        file->close();
    }
    source = shared_ptr<iostream>();    // Boost equivalent of NULL
}
