#ifndef TASK_SCHEDULER_H
#define TASK_SCHEDULER_H

// STL headers
#include <map>
#include <memory>

#include "types.h"

struct Task {
    u32 taskID;
    u32 parentID;       //!< When the task is done, it reports back to the parent.
    u32 activeChildren; //!< When a child is done, decrement this count. When it reaches 0 and our work is done, we are done.
    u32 dependencyID;   //!< Can't start until this task is done.
    // TODO: add function pointer
};

class TaskScheduler {
public:
    bool addTask(std::shared_ptr<Task> task);
    
private:
    // TODO: maintain thread list
    std::map<u32, std::shared_ptr<Task> > tasks;
};

#endif