#ifndef COMPRESSION_H
#define COMPRESSION_H

#include "types.h"

void compressHuffman();
void decompressHuffman();

void compressZlib(char* inBuffer, char* outBuffer, int inLength, int outLength);
int decompressZlib(char* inBuffer, char* outBuffer, int inLength, int outLength);

#endif