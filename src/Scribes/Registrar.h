#ifndef SCRIBE_REGISTRY_H
#define SCRIBE_REGISTRY_H

#include <memory>
#include <string>
#include <vector>

#include "Scribes/Abstract/Base.h"

namespace Scribe {

class Registrar {
public:
    static void add(std::shared_ptr<Abstract::Base> scribe);
    static std::vector<std::shared_ptr<Abstract::Base>> determineScribes(std::shared_ptr<std::string> filename);
private:
    Registrar();
    ~Registrar();
    static std::vector<std::shared_ptr<Abstract::Base>> scribes;
};

}

#endif