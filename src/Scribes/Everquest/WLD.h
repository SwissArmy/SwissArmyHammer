#ifndef SCRIBE_EVERQUEST_WLD_H
#define SCRIBE_EVERQUEST_WLD_H

// C++ includes
#include <string>
#include <sstream>
#include <list>
#include <iostream>
#include <fstream>
#include <map>
#include <memory>

#include "Phobos/Texture.h"
#include "Phobos/ZoneObject.h"
#include "Phobos/ZoneLight.h"
#include "Phobos/BSPNode.h"

#include "Scribes/Abstract/Model.h"

// local includes
#include "types.h"

namespace Scribe {
namespace Everquest {

//! \brief WLD files store Everquest's models and zones.
//! \details
//! They are broken up into "fragments", each of which store a different type
//! of data identified by a 4 byte ID.\n
//! Many models can be stored in the same WLD file, but usually each zone has
//! its own set of WLD files, which are stored with their textures (in DDS
//! format) inside an S3D archive.
//! \par Fragment Types
class WLD : public Abstract::Model {
public:
    // inherited from Abstract::Base
    bool canOpen(std::shared_ptr<std::string> file) const override;
    std::string getPersonalityName() const override {
        return "WLD";
    }

    // inherited from Abstract::Model
    std::vector<std::shared_ptr<Phobos::Model>> readModels(std::shared_ptr<File> source) const override;

    // deprecated
    bool print(std::shared_ptr<File> source) const;
private:
    // Private data structures
    struct WLD_State;
    struct ResultPackage;

    // Fragment loading functions.
    bool loadFragment(WLD_State &state) const;

    bool loadFragmentType0x03(WLD_State &state) const;
    bool loadFragmentType0x04(WLD_State &state) const;
    bool loadFragmentType0x05(WLD_State &state) const;
    bool loadFragmentType0x06(WLD_State &state) const;
    bool loadFragmentType0x07(WLD_State &state) const;
    bool loadFragmentType0x08(WLD_State &state) const;
    bool loadFragmentType0x09(WLD_State &state) const;
    bool loadFragmentType0x10(WLD_State &state) const;
    bool loadFragmentType0x11(WLD_State &state) const;
    bool loadFragmentType0x12(WLD_State &state) const;
    bool loadFragmentType0x13(WLD_State &state) const;
    bool loadFragmentType0x14(WLD_State &state) const;
    bool loadFragmentType0x15(WLD_State &state) const;
    bool loadFragmentType0x16(WLD_State &state) const;
    bool loadFragmentType0x17(WLD_State &state) const;
    bool loadFragmentType0x18(WLD_State &state) const;
    bool loadFragmentType0x1b(WLD_State &state) const;
    bool loadFragmentType0x1c(WLD_State &state) const;
    bool loadFragmentType0x21(WLD_State &state) const;
    bool loadFragmentType0x22(WLD_State &state) const;
    bool loadFragmentType0x26(WLD_State &state) const;
    bool loadFragmentType0x27(WLD_State &state) const;
    bool loadFragmentType0x28(WLD_State &state) const;
    bool loadFragmentType0x29(WLD_State &state) const;
    bool loadFragmentType0x2a(WLD_State &state) const;
    bool loadFragmentType0x2c(WLD_State &state) const;
    bool loadFragmentType0x2d(WLD_State &state) const;
    bool loadFragmentType0x2f(WLD_State &state) const;
    bool loadFragmentType0x30(WLD_State &state) const;
    bool loadFragmentType0x31(WLD_State &state) const;
    bool loadFragmentType0x32(WLD_State &state) const;
    bool loadFragmentType0x33(WLD_State &state) const;
    bool loadFragmentType0x34(WLD_State &state) const;
    bool loadFragmentType0x35(WLD_State &state) const;
    bool loadFragmentType0x36(WLD_State &state) const;
    bool loadFragmentType0x37(WLD_State &state) const;

    void getResults(const WLD_State &state, ResultPackage &results) const;
};

}
}

#endif
