// SOIL includes
#include <SOIL.h>

// C++ includes
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>

// Phobos includes
#include "Phobos/Texture.h"

// local includes
#include "Utility/console.h"
#include "Utility/log.h"

#include "WLD.h"

using namespace std;
using namespace Phobos;

namespace Scribe {
namespace Everquest {

// ###########
// # Structs #
// ###########

struct Placeable {
    //float trans[3], rot[3], scale[3];
    Vertex position;
    glm::dvec3 rotation;
    glm::dvec3 scale;
    u32 meshID;
    //Mesh* mesh;
};

struct WLD_VertexColor {
	u8 color[4];
};

//! map texture coordinates to a triangle?
struct WLD_TextureMap {
    u16 triangleCount;
    u16 textureID;
};

struct WLD_Texture {
    u32 frames;                 //!< Number of animation frames
    vector<string> filenames;   //!< Texture filenames
    u32 delay;                  //!< Animation delay
};

struct WLD_ZoneObject {
    u32 blah;
};

struct WLD_Header {
    u32 magicCookie;        //!< Must always be 0x54503d02
    u32 version;            //!< The version of the WLD format that this file uses.
    u32 fragmentCount;		//!< Number of fragments in the file minus one.
    u32 bspCount;			//!< Number of 0x22 BSP fragments in the file.
    u32 header4;			//!< Unknown. Should always contain 0x000680D4.
    u32 stringHashSize;		//!< Size of string hash.
    u32 header6;			//!< Unknown.
};

struct FragmentHeader {
	u32 size;
	u32 type;
	u32 name;	//!< A 4 byte identifier for the fragment. Used to look up mesh and texture ID numbers...?
};

struct WLD::WLD_State {
    // file I/O
    shared_ptr<File> file;
    shared_ptr<istream> inFile;
    // configuration
    bool printing;
    // current fragment
    u32 frag_id;
    FragmentHeader frag;
    // data
    u32 version;
    u8* stringhash;

    // Maps to link fragments to data.
    map<u32, shared_ptr<WLD_Texture>> textures;
    map<u32, shared_ptr<vector<shared_ptr<WLD_Texture>>>> textureLists;
    map<u32, shared_ptr<Phobos::Model>> models;
    map<u32, shared_ptr<WLD_ZoneObject>> zoneObjects;
};

struct WLD::ResultPackage {
    vector<shared_ptr<Phobos::Texture>> textures;
    vector<shared_ptr<Phobos::Model>> models;
};

const string fragmentTypes[] = {
    "###", //0x00
    "###", //0x01
    "###", //0x02
    "texture filenames", //0x03
    "texture animation", //0x04
    "texture metadata", //0x05
    "unimplemented", //0x06
    "unimplemented", //0x07
    "unimplemented", //0x08
    "unimplemented", //0x09
    "###", //0x0a
    "###", //0x0b
    "###", //0x0c
    "###", //0x0d
    "###", //0x0e
    "###", //0x0f
    "unknown objects", //0x10
    "unimplemented", //0x11
    "unimplemented", //0x12
    "unimplemented", //0x13
    "unimplemented", //0x14
    "placeable", //0x15
    "unimplemented", //0x16
    "unimplemented", //0x17
    "unimplemented", //0x18
    "###", //0x19
    "###", //0x1a
    "light", //0x1b
    "pointer", //0x1c
    "###", //0x1d
    "###", //0x1e
    "###", //0x1f
    "###", //0x20
    "bsp tree", //0x21
    "bsp region", //0x22
    "###", //0x23
    "###", //0x24
    "###", //0x25
    "texture ???", //0x26
    "unimplemented", //0x27
    "light ?", //0x28
    "zone data", //0x29
    "unimplemented", //0x2a
    "###", //0x2b
    "unimplemented", //0x2c
    "unimplemented", //0x2d
    "###", //0x2e
    "animated vertices", //0x2f
    "texture transparency", //0x30
    "texture array", //0x31
    "unimplemented", //0x32
    "unimplemented", //0x33
    "unimplemented", //0x34
    "empty/unimplemented", //0x35
    "mesh data", //0x36
    "unimplemented", //0x37
    "###"  //0x38
};

// #############
// # Constants #
// #############

const u32 WLDMagicCookie = 0x54503d02;
const u8 encodeArray[] = {0x95, 0x3A, 0xC5, 0x2A, 0x95, 0x7A, 0x95, 0x6A};
enum wldVersion { OLD=0x00015500, NEW=0x1000c800 };

// ####################
// # Helper functions #
// ####################

//! modifies in place
inline void decode(u8* str, int len) {
    for (int i = 0; i < len; i++)
        str[i] ^= encodeArray[i % 8];
};

// ####################
// # Scribe functions #
// ####################

bool WLD::canOpen(const std::shared_ptr<std::string> filename) const {
    assert(filename);

    shared_ptr<File> source(new File());

    if (!source->openDiskSourceBinary(filename)) {
        return false;
    }

    shared_ptr<istream> file = source->readSource();

    assert(file);

    assert(source->ready());

    source->reset();

    if (source->fileExtension().compare("wld") == 0) {
        WLD_Header header = source->read<WLD_Header>();
        source->reset();
        if (header.version != OLD && header.version != NEW) {
            return false;
        }
        if (header.magicCookie != WLDMagicCookie) {
            return false;
        }
        return true;
    }

    return false;
}

// #########
// # Print #
// #########

bool WLD::print(const shared_ptr<File> source) const {
    assert(source);
    assert(source->ready());

    WLD_State state;
    state.printing = true;
    state.file = source;
    state.inFile = source->readSource();

    // Read the header.
    WLD_Header header = state.file->read<WLD_Header>();

    cout << "= Reading WLD file " << source->filename << " =" << endl;
    cout << "== " << yellow << "File Header" << normal << " ==" << endl;
    cout << "    Version: ";
    if (header.version == OLD) {
        cout << yellow << header.version << " (old)" << normal << endl;
    } else if (header.version == NEW) {
        cout << green << header.version << " (new)" << normal << endl;
    } else {
        cout << red << header.version << " (unknown)" << normal << endl;
    }
    cout << "    Number of fragments: " << header.fragmentCount << endl;
    cout << "    Number of BSPs: " << header.bspCount << endl;
    cout << "    Unknown (header4): 0x" << hex << header.header4 << dec << endl;
    cout << "    String hash size: " << header.stringHashSize << endl;
    cout << "    Unknown (header6): " << header.header6 << endl;

    if (header.magicCookie != WLDMagicCookie) {
        cout << "Warning: " << source->filename << " has the wrong magic cookie. Probably not a WLD file." << endl;
    }

    state.version = header.version;

    // Read the string hash.

    state.stringhash = new u8[header.stringHashSize];
    state.inFile->read((char*) state.stringhash, header.stringHashSize);

    decode(state.stringhash, header.stringHashSize);

    //cout << "    String hash: " << string((char*) state.stringhash, header.stringHashSize) << endl;

    // The WLD format counts fragments starting from 1, so we do the same.
    for (u32 i = 1; i <= header.fragmentCount; i++) {

        state.inFile->read((char*) &state.frag, sizeof(FragmentHeader));
        if (state.frag.type != 0x35) {
            cout << "==" << magenta << " 0x" << setfill('0') << setw(2) << hex << state.frag.type << dec << " Fragment (" << fragmentTypes[state.frag.type] << ") ID:" << i << " N:" << state.frag.name << " S:" << state.frag.size << normal << " ==" << endl;
        }
        streampos initialPosition = state.inFile->tellg();
        state.frag_id = i;
        loadFragment(state);
        streampos distanceTraveled = state.inFile->tellg() - initialPosition;
        if (distanceTraveled != (streampos) (state.frag.size - 4) && state.frag.type != 0x03) {
            s32 bytesLeft = (state.frag.size - 4) - distanceTraveled;
            cout << "    " << bytesLeft << " Extra bytes: " << red;

            if (bytesLeft > 0) {
                for (s32 j = 0; j < bytesLeft; j++) {
                    if (state.inFile->good()) {
                        u8 byte;
                        state.inFile->read((char*) &byte, sizeof(u8));
                        cout << hex << setfill('0') << setw(2) << (int) byte << dec << " ";
                    }
                }
            }
            cout << normal << endl;

        }
        state.inFile->seekg(initialPosition + (streampos) (state.frag.size - 4));
    }

    delete [] state.stringhash;

    return true;
}

// ########
// # Read #
// ########

vector<shared_ptr<Phobos::Model>> WLD::readModels(const shared_ptr<File> source) const {
    assert(source);
    assert(source->ready());

    WLD_State state;
    state.file = source;
    state.inFile = source->readSource();

    // Read the header.
    WLD_Header header = state.file->read<WLD_Header>();

    if (header.magicCookie != WLDMagicCookie || (header.version != OLD && header.version != NEW)) {
        Log().error() << source->filename << " was not a valid WLD file.";
        vector<shared_ptr<Phobos::Model>> err;
        return err;
    }

    state.version = header.version;

    // Read the string hash.
    state.stringhash = new u8[header.stringHashSize];
    source->readSource()->read((char*) state.stringhash, header.stringHashSize);

    decode(state.stringhash, header.stringHashSize);

    // Read fragments
    // The WLD format counts fragments starting from 1, so we do the same.
    for (u32 i = 1; i <= header.fragmentCount; i++) {
        state.frag = state.file->read<FragmentHeader>();
        state.frag_id = i;
        streampos initialPosition = state.file->readPos();
        loadFragment(state);
        state.inFile->seekg(initialPosition + (streampos) (state.frag.size - 4));
    }

    delete [] state.stringhash;

    ResultPackage results;
    getResults(state, results);

    return results.models;
}

// ####################
// # Fragment helpers #
// ####################

bool WLD::loadFragment(WLD_State &state) const {
    if (state.frag.type == 0x03) {
        return loadFragmentType0x03(state);
    } else if (state.frag.type == 0x04) {
        return loadFragmentType0x04(state);
    } else if (state.frag.type == 0x05) {
        return loadFragmentType0x05(state);
    } else if (state.frag.type == 0x06) {
        return loadFragmentType0x06(state);
    } else if (state.frag.type == 0x07) {
        return loadFragmentType0x07(state);
    } else if (state.frag.type == 0x08) {
        return loadFragmentType0x08(state);
    } else if (state.frag.type == 0x09) {
        return loadFragmentType0x09(state);
    } else if (state.frag.type == 0x10) {
        return loadFragmentType0x10(state);
    } else if (state.frag.type == 0x11) {
        return loadFragmentType0x11(state);
    } else if (state.frag.type == 0x12) {
        return loadFragmentType0x12(state);
    } else if (state.frag.type == 0x13) {
        return loadFragmentType0x13(state);
    } else if (state.frag.type == 0x14) {
        return loadFragmentType0x14(state);
    } else if (state.frag.type == 0x15) {
        return loadFragmentType0x15(state);
    } else if (state.frag.type == 0x16) {
        return loadFragmentType0x16(state);
    } else if (state.frag.type == 0x17) {
        return loadFragmentType0x17(state);
    } else if (state.frag.type == 0x18) {
        return loadFragmentType0x18(state);
    } else if (state.frag.type == 0x1b) {
        return loadFragmentType0x1b(state);
    } else if (state.frag.type == 0x1c) {
        return loadFragmentType0x1c(state);
    } else if (state.frag.type == 0x21) {
        return loadFragmentType0x21(state);
    } else if (state.frag.type == 0x22) {
        return loadFragmentType0x22(state);
    } else if (state.frag.type == 0x26) {
        return loadFragmentType0x26(state);
    } else if (state.frag.type == 0x27) {
        return loadFragmentType0x27(state);
    } else if (state.frag.type == 0x28) {
        return loadFragmentType0x28(state);
    } else if (state.frag.type == 0x29) {
        return loadFragmentType0x29(state);
    } else if (state.frag.type == 0x2a) {
        return loadFragmentType0x2a(state);
    } else if (state.frag.type == 0x2c) {
        return loadFragmentType0x2c(state);
    } else if (state.frag.type == 0x2d) {
        return loadFragmentType0x2d(state);
    } else if (state.frag.type == 0x2f) {
        return loadFragmentType0x2f(state);
    } else if (state.frag.type == 0x30) {
        return loadFragmentType0x30(state);
    } else if (state.frag.type == 0x31) {
        return loadFragmentType0x31(state);
    } else if (state.frag.type == 0x32) {
        return loadFragmentType0x32(state);
    } else if (state.frag.type == 0x33) {
        return loadFragmentType0x33(state);
    } else if (state.frag.type == 0x34) {
        return loadFragmentType0x34(state);
    } else if (state.frag.type == 0x35) {
        return loadFragmentType0x35(state);
    } else if (state.frag.type == 0x36) {
        return loadFragmentType0x36(state);
    } else if (state.frag.type == 0x37) {
        return loadFragmentType0x37(state);
    } else {
        Log().warning() << "Unknown fragment type " << state.frag.type;
        return false;
    }
}

void WLD::getResults(const WLD_State &state, ResultPackage &results) const {
    // TODO: link textures to models

    // uniquify the list of models
    set<shared_ptr<Phobos::Model>> unique_models;
    for (const auto &model : state.models) {
        // TODO: this assert fails because there is a bug somewhere. but it's a hard one to track down.
        //assert(model.second != nullptr);
        if (model.second != nullptr) {
            unique_models.insert(model.second);
        }
    }
    for (const auto &model : unique_models) {
        model->calculateFaceNormals();
    }
    // copy the set to a vector
    copy(unique_models.begin(), unique_models.end(), back_inserter(results.models));
}

//! \class WLD
//! \par 0x03 - Texture Filenames
//! \code
//! u32 - Number of textures
//!
//! // for each texture
//!     u16            - Filename length
//!     char* {length} - Filename (null terminated, null included)
//! \endcode
bool WLD::loadFragmentType0x03(WLD_State &state) const {
    u32 count = state.file->read<u32>();

    if (count == 0) {
        count = 1;
    }

    shared_ptr<WLD_Texture> texture(new WLD_Texture());
    texture->frames = count;
    texture->filenames.resize(count);

    for (u32 i = 0; i < count; i++) {

        u16 nameLength = state.file->read<u16>();

        u8* filename = new u8[nameLength];
        state.inFile->read((char*) filename, nameLength);

        decode(filename, nameLength);

        if (state.printing) {
            cout << "    [" << i << "] - " << (c8*) filename << endl;
        }

        texture->filenames[i].assign((c8*) filename);
    }

    state.textures[state.frag_id] = texture;

    return true;
}

//! \class WLD
//! \par 0x04 - Animated Texture Data
//! \code
//! u32 - flags
//!   // 0x4     - Read extra byte
//!   // 0x8     - Texture is animated.
//!   // 0x10    - Unknown
//!   // 0x40000 - Read extra byte (?)
//!
//! // if (flags & 0x4)
//!   u32 - unknown
//! // if (flags & 0x8)
//!   u32 - Animation delay.
//!
//! u32 - Number of elements
//!
//! // for each element
//!   u32 - fragment ID (0x03 fragments only?)
//! \endcode
bool WLD::loadFragmentType0x04(WLD_State &state) const {

    u32 flags = state.file->read<u32>();
    u32 count = state.file->read<u32>();

    if (flags & 0xfffbffe3) {
        cout << red << "    Unrecognized flag 0x" << hex << flags << dec << endl;
    }

    if (flags & 0x4) {
        u32 unknown = state.file->read<u32>();
        if (state.printing) {
            cout << "    FLAG: Unknown (0x4)" << endl;
            cout << "    Unknown: " << unknown << endl;
        }
    }
    if (flags & 0x8) {
        u32 delay = state.file->read<u32>();
        if (state.printing) {
            cout << "    FLAG: Animated" << endl;
            cout << "    Animation delay: " << delay << endl;
        }
    }
    if (flags & 0x10) {
        if (state.printing) {
            cout << "    FLAG: Unknown (0x10)" << endl;
        }
    }
    if (flags & 0x40000) {
        if (state.printing) {
            cout << "    FLAG: Unknown (0x40000)" << endl;
        }
    }

    if (count == 0) {
        count = 1;
    }

    if (count == 1) {
        u32 fragmentPointer = state.file->read<u32>();
        state.textures[state.frag_id] = state.textures[fragmentPointer];
        if (state.printing) {
            cout << "    [0] - Fragment pointer: " << fragmentPointer << endl;
        }
    } else {
        shared_ptr<WLD_Texture> texture(new WLD_Texture());
        texture->frames = count;
        texture->filenames.resize(count);

        for (u32 i = 0; i < count; i++) {
            u32 fragmentPointer = state.file->read<u32>();
            shared_ptr<WLD_Texture> linkedTexture = state.textures[fragmentPointer];
			texture->filenames[i] = linkedTexture->filenames[linkedTexture->frames - 1];
            if (state.printing) {
                cout << "    [" << i << "] - Fragment pointer: " << fragmentPointer << endl;
            }
        }
        state.textures[state.frag_id] = texture;
    }

	return true;
}

//! \class WLD
//! \par 0x05 - Texture Metadata
//! \code
//! u32 - Fragment ID
//! u32 - flags
//!     // 0x10 - unknown
//!     // 0x40 - unknown
//! \endcode
bool WLD::loadFragmentType0x05(WLD_State &state) const {

    u32 fragmentPointer = state.file->read<u32>();
	u32 flags = state.file->read<u32>();

    if (flags & 0xffffffaf) {
        cout << red << "    Unrecognized flag: " << hex << flags << dec << endl;
    }

    if (flags & 0x10) {
        if (state.printing) {
            cout << "    FLAG: Unknown (0x10)" << endl;
        }
    }
    if (flags & 0x40) {
        if (state.printing) {
            cout << "    FLAG: Unknown (0x40)" << endl;
        }
    }

    bool unknown = true;
    if (state.textures[fragmentPointer] != nullptr) {
        if (state.printing) {
            cout << "    Pointer to texture fragment: " << fragmentPointer << endl;
        }
        state.textures[state.frag_id] = state.textures[fragmentPointer];
        unknown = false;
    }
    if (state.models[fragmentPointer] != nullptr) {
        if (state.printing) {
            cout << "    Pointer to model fragment: " << fragmentPointer << endl;
        }
        state.models[state.frag_id] = state.models[fragmentPointer];
        unknown = false;
    }
	if (unknown) {
        Log().error() << "Unknown pointer type in 0x05 fragment " << state.frag_id << ": Pointer to fragment " << fragmentPointer;
		return false;
	}

    return false;
}

//! \class WLD
//! \par 0x06 - Equipment information?
//! Only found in gequip.wld.
bool WLD::loadFragmentType0x06(WLD_State &state) const {
	return false;
}

//! \class WLD
//! \par 0x07 - Equipment information?
//! Only found in gequip.wld.
bool WLD::loadFragmentType0x07(WLD_State &state) const {
	return false;
}

//! \class WLD
//! \par 0x08 - Zone information?
//! Found in zone files.\n
//! Always seems to be 108 bytes.\n
//! The only byte that seems to change is byte 25 (seen values of 11 and 16)
//!
//! \code
//! u32[26] - Unknown
//! \endcode
bool WLD::loadFragmentType0x08(WLD_State &state) const {

	for (int i = 0; i < 26; i++) {
		u32 byte = state.file->read<u32>();
        if (state.printing) {
            cout << "    Byte " << i << ": " << byte << endl;
        }
	}
	return false;
}

//! \class WLD
//! \par 0x08 - Zone information?
//! \todo record where this fragment was seen
//!
//! \code
//! u32 - Pointer to an 0x08 fragment.
//! u32 - Unknown
//! \endcode
bool WLD::loadFragmentType0x09(WLD_State &state) const {
	u32 pointer = state.file->read<u32>();   // Pointer to a 0x08 fragment.
	u32 unknown2 = state.file->read<u32>();

    if (state.printing) {
        cout << "    Pointer: " << pointer << endl;
        cout << "    Unknown2: " << unknown2 << endl;
    }

	return false;
}

//! \class WLD
//! \par 0x10 - Zone information?
//! Found in *_chr.s3d files.
//!
//! \code
//! u32 - flags
//!     // 0x2   - unknown
//!     // 0x200 - unknown
//! u32 - Number of elements
//! u32 - Pointer to a fragment
//!
//! // for each element
//!     u32[6] - unknown
//! \endcode
//! In addition, there seems to always be at least 12 extra bytes somewhere in the fragment either before or after the array.\n
//! Sometimes there are more, up to 20 extra bytes have been seen.\n
//! Their presence probably depends on the flag settings.
bool WLD::loadFragmentType0x10(WLD_State &state) const {
    u32 flags = state.file->read<u32>();
    u32 count = state.file->read<u32>();
    u32 fragment = state.file->read<u32>();

    if (state.printing) {
        cout << "    Flags: 0x" << hex << flags << dec << endl;
        cout << "    Number of elements: " << count << endl;
        cout << "    Fragment pointer: " << fragment << endl;
    }

	//u32 unknown1 = state.file->read<u32>();
    //u32 unknown2 = state.file->read<u32>();
    //u32 unknown3 = state.file->read<u32>();
	//cout << "    'unknown1': " << unknown1 << endl;
	//cout << "    'unknown2': " << unknown2 << endl;
	//cout << "    'unknown3': " << unknown3 << endl;

	for (u32 i = 0; i < count; i++) {

        struct WLD_Fragment10Piece {
            u32 unknown1; //!< Possibly flags.
            u32 unknown2;
            u32 unknown3;
            u32 unknown4; //!< Coordinate?
            u32 unknown5; //!< Coordinate?
            u32 unknown6; //!< Coordinate?
        } piece = state.file->read<WLD_Fragment10Piece>();

        if (state.printing) {
            cout << "    Piece " << i << endl;
            cout << "        u1=" << piece.unknown1 << " u2=" << piece.unknown2 << " u3=" << piece.unknown3 << endl;
            cout << "        u4=" << piece.unknown4 << " u5=" << piece.unknown5 << " u6=" << piece.unknown6 << endl;
        }
	}

    return false;
}

//! \class WLD
//! \par 0x11 - ???
bool WLD::loadFragmentType0x11(WLD_State &state) const {
	return false;
}

//! \class WLD
//! \par 0x12 - ???
//! u32 - Unknown
//! u32 - flags
//!     // 0x1  - unknown
//!     // 0x4  - unknown
//!     // 0x10 -
//! u32 - Unknown
//! u32 - Unknown
//! u32 - Unknown
bool WLD::loadFragmentType0x12(WLD_State &state) const {
    u32 unknown1 = state.file->read<u32>();
    u32 flags = state.file->read<u32>();
    u32 unknown2 = state.file->read<u32>();
    u32 unknown3 = state.file->read<u32>();
    u32 unknown4 = state.file->read<u32>();
    u32 unknown5 = state.file->read<u32>();

    cout << "    Unknown1: " << unknown1 << endl;
    cout << "    Flags: 0x" << flags << endl;
    cout << "    Unknown2: " << hex << unknown2 << dec << endl;
    cout << "    Unknown3: " << unknown3 << endl;
    cout << "    Unknown4: " << unknown4 << endl;
    cout << "    Unknown5: " << unknown5 << endl;

    if (flags & 0x14) {
        struct Extra {
            u32 unknown[52];
        };
        Extra extra = state.file->read<Extra>();
        for (int i = 0; i < 52; i++) {
            cout << "    Extra " << i << ": " << extra.unknown[i] << endl;
        }
    }

	return true;
}

//! \class WLD
//! \par 0x13 - ???
//! \code
//! u32 - Pointer to an 0x12 fragment.
//! u32 - flags
//!     // 0x1 - Read 4 extra bytes.
//!     // 0x4 - Unknown.
//! \endcode
bool WLD::loadFragmentType0x13(WLD_State &state) const {
    u32 fragmentPointer = state.file->read<u32>();
    u32 flags = state.file->read<u32>();

    cout << "    Fragment pointer: " << fragmentPointer << endl;
    cout << "    Flags: 0x" << hex << flags << dec << endl;

    if (flags & 0x1) {
        u32 unknown1 = state.file->read<u32>();
        cout << "    Unknown1: " << unknown1 << endl;
    }
	return true;
}

//! \class WLD
//! \par 0x14 - ???
bool WLD::loadFragmentType0x14(WLD_State &state) const {
	return false;
}

//! \class WLD
//! \par 0x10 - Placeable
//! \code
//! u32    - ref
//! u32    - flags
//!     // 0x2  - Unknown
//!     // 0x4  - Unknown
//!     // 0x8  - Unknown
//!     // 0x20 - Unknown
//! u32    - Pointer to an 0x16 fragment.
//! f32[3] - Translation xyz
//! f32[3] - Rotation xyz
//! f32[3] - Scale xyz
//! u32    - Fragment pointer (Sometimes 0.)
//! u32    - flags2
//!     // 0x4   - Unknown
//!     // 0x8   - Unknown
//!     // 0x10  - Unknown
//!     // 0x20  - Unknown
//!     // 0x40  - Unknown
//!     // 0x80  - Unknown
//!     // 0x200 - Unknown
//!     // 0x400 - Unknown
//!     // 0x800 - Unknown
//! \endcode
bool WLD::loadFragmentType0x15(WLD_State &state) const {

    struct WLD_Fragment15Header {
        u32 ref;
        u32 flags;
        u32 fragment1;    //!< Pointer to an 0x16 fragment.
        f32 trans[3];
        f32 rot[3];
        f32 scale[3];
        u32 fragment2;    //!< Sometimes 0.
        u32 flags2;
    } header = state.file->read<WLD_Fragment15Header>();

    if (state.printing) {
        cout << "    Mesh String Header Ref: " << header.ref << endl;
        cout << "    Flags: 0x" << hex << header.flags << dec << endl;
        cout << "    fragment1: " << header.fragment1 << endl;
        cout << "    Translation: x=" << header.trans[0] << " y=" << header.trans[1] << " z=" << header.trans[2] << endl;
        cout << "    Rotation: x=" << header.rot[0] << " y=" << header.rot[1] << " z=" << header.rot[2] << endl;
        cout << "    Scale: x=" << header.scale[0] << " y=" << header.scale[1] << " z=" << header.scale[2] << endl;
        cout << "    fragment2: " << header.fragment2 << endl;
        cout << "    Flags2: 0x" << hex << header.flags2 << dec << endl;
        //cout << "    Mesh Name: " << string((char*) &state.stringhash[-(int)header.ref]) << endl;
        cout << "    Mesh ID: " << (int) state.stringhash[-(int)header.ref] << endl;
    }

    if (((signed long) -header.ref) < (signed long) 0) {
        Log().error() << "0x15 fragment contained invalid header ref: " << header.ref;
        return false;
    }

    ZoneObject* zObject = new ZoneObject;

    zObject->position.x = header.trans[0];
    zObject->position.y = header.trans[1];
    zObject->position.z = header.trans[2];

    zObject->rotation.x = header.rot[2] / 512.f * 360.f;
    zObject->rotation.y = header.rot[1] / 512.f * 360.f;
    zObject->rotation.z = header.rot[0] / 512.f * 360.f;

    zObject->scale.x = header.scale[2];
    zObject->scale.y = header.scale[1];
    zObject->scale.z = header.scale[0];

    // TODO: Go through and link all zone objects to the correct mesh afterwards.
	//place->meshID = (int) sHash[-(int)header.ref];

	return true;
}

//! \class WLD
//! \par 0x16 - ???
bool WLD::loadFragmentType0x16(WLD_State &state) const {
	return false;
}

//! \class WLD
//! \par 0x17 - ???
bool WLD::loadFragmentType0x17(WLD_State &state) const {
	return false;
}

//! \class WLD
//! \par 0x18 - ???
bool WLD::loadFragmentType0x18(WLD_State &state) const {
	return false;
}

//! \class WLD
//! \par 0x1b - Light
//! \code
//! u32    - flags
//!     // 0x4  - Unknown
//!     // 0x8  - Light contains color values
//!     // 0x10 - Unknown
//! u32    - 'params1'
//! u32    - 'params3b'
//! f32[3] - color rgb
//! \endcode
bool WLD::loadFragmentType0x1b(WLD_State &state) const {

    struct Header {
        u32 flags;
        u32 params1;
        u32 params3b;
    } header = state.file->read<Header>();

    if (state.printing) {
        cout << "    Flags: 0x" << hex << header.flags << dec << endl;
        cout << "    'params1': " << header.params1 << endl;
        cout << "    'params3b': " << header.params3b << endl;
    }

    ZoneLight* zLight = new ZoneLight;

    zLight->radiosity = 0;

    if (header.flags & 0x8) {
        struct Color {
            f32 r;
            f32 g;
            f32 b;
        };
        Color c = state.file->read<Color>();
        if (state.printing) {
            cout << "    Color: r=" << c.r << " g=" << c.g << " b=" << c.b << endl;
        }
        zLight->r = c.r;
        zLight->g = c.g;
        zLight->b = c.b;
    } else {
        zLight->r = 1.0f;
        zLight->g = 1.0f;
        zLight->b = 1.0f;
    }

	return false;
}

//! \class WLD
//! \par 0x1c - Zone light information (?)
//! Found in zone files.
//! \code
//! u32 - Pointer to a 0x1b fragment.
//! u32 - unknown
//! \endcode
bool WLD::loadFragmentType0x1c(WLD_State &state) const {
    u32 fragmentPointer = state.file->read<u32>();
	u32 unknown = state.file->read<u32>();

    if (state.printing) {
        cout << "    Pointer to fragment " << fragmentPointer << endl;
        cout << "    Unknown: " << unknown << endl;
    }

	return false;
}

//! \class WLD
//! \par 0x21 - BSP tree
//! \code
//! u32    - Number of nodes in the BSP tree.
//!
//! // Node
//! f32[3] - Normal
//! f32    - 'splitdistance'
//! u32    - Link to a 0x22 region fragment. (0 if no link is needed.)
//! u32    - Index of the left child node.
//! u32    - Index of the right child node.
//! \endcode
bool WLD::loadFragmentType0x21(WLD_State &state) const {

    u32 nodeCount = state.file->read<u32>();

    Log().info() << nodeCount << " BSP nodes.";

    for (u32 i = 0; i < nodeCount; i++) {

        struct Node {
            f32 normal[3];
            f32 splitdistance;
            u32 region;
            u32 leftNode;
            u32 rightNode;
        } node = state.file->read<Node>();

        if (state.printing) {
            cout << "    Node " << i << endl;
            cout << "        Normal: <" << node.normal[0] << ", " << node.normal[1] << ", " << node.normal[2] << ">" << endl;
            cout << "        'splitdistance': " << node.splitdistance << endl;
            cout << "        Region fragment " << node.region << endl;
            cout << "        Left node: " << node.leftNode << endl;
            cout << "        Right node: " << node.rightNode << endl;
        }
    }

	return false;
}

//! \class WLD
//! \par 0x22 - BSP region
//! \code
//! u32 - flags
//!     // 0x1   - unknown
//!     // 0x8   - 'word PVS'
//!     // 0x80  - unknown
//!     // 0x100 - unknown
//! u32 - Fragment pointer
//! u32 - 'size1'
//! u32 - 'size2'
//! u32 - 'params1'
//! u32 - 'size3'
//! u32 - 'size4'
//! u32 - 'params2'
//! u32 - 'size5'
//! u32 - 'size6'
//! \endcode
bool WLD::loadFragmentType0x22(WLD_State &state) const {

    struct Header {
        u32 flags;
        u32 fragment1;
        u32 size1;
        u32 size2;
        u32 params1;
        u32 size3;
        u32 size4;
        u32 params2;
        u32 size5;
        u32 size6;
    } header = state.file->read<Header>();

    if (state.printing) {
        cout << "    Flags: 0x" << hex << header.flags << dec << endl;
        cout << "    fragment1: " << header.fragment1 << endl;						// According to azone, the sizes are as follows:
        cout << "    Unknown (size1): "   << hex << header.size1   << dec << endl;	// 12 bytes
        cout << "    Unknown (size2): "   << hex << header.size2   << dec << endl;   // 8 bytes
        cout << "    Unknown (params1): " << hex << header.params1 << dec << endl;
        cout << "    Unknown (size3): "   << hex << header.size3   << dec << endl;   // ??
        cout << "    Unknown (size4): "   << hex << header.size4   << dec << endl;   // ??
        cout << "    Unknown (params2): " << hex << header.params2 << dec << endl;
        cout << "    Unknown (size5): "   << hex << header.size5   << dec << endl;   // 28 bytes
        cout << "    Unknown (size6): "   << hex << header.size6   << dec << endl;   // 2 bytes for size + 16 bytes + ???
    }

	/*
    for (int i = 0; i < header.size1; i++) {
        u32 unknown1 = state.file->read<u32>();
        u32 unknown2 = state.file->read<u32>();
        u32 unknown3 = state.file->read<u32>();
        cout << "        'size1' " << i << ": u1=" << unknown1 << " u2=" << unknown2 << " u3=" << unknown3 << endl;
    }

    for (int i = 0; i < header.size2; i++) {
        u32 unknown1 = state.file->read<u32>();
        u32 unknown2 = state.file->read<u32>();
        cout << "        'size2' " << i << ": u1=" << unknown1 << " u2=" << unknown2 << endl;
    }

    if (header.size3 != 0) {
        cerr << "Nonzero value 'size3' in fragment " << fragmentID << " (type 0x22). Can't handle." << endl;
        return false;
    }
    if (header.size4 != 0) {
        cerr << "Nonzero value 'size4' in fragment " << fragmentID << " (type 0x22). Can't handle." << endl;
        return false;
    }

    for (int i = 0; i < header.size5; i++) {
        u32 unknown[7];
        state.inFile->read((char*) unknown, sizeof(u32) * 7);
		cout << "        'size5' " << i << ": ";
		for (int j = 0; j < 7; j++) {
			cout << " u" << j << "=" << unknown[j];
		}
        cout << endl;
    }*/

    // TODO: see azone2 code for more stuff to put here
    return false;
}

//! \par 0x26 - texture ???
//! \code
//! u32 - Unknown. Possibly flags.
//! u32 - Pointer to an 0x05 fragment.
//! u32 - Unknown.
//! \endcode
bool WLD::loadFragmentType0x26(WLD_State &state) const {

    struct Header {
        u32 unknown1;        //!< flags?
        u32 fragmentPointer; //!< pointer to an 0x05 fragment
        u32 unknown2;        //!< ???
    } header = state.file->read<Header>();

    cout << "    Unknown1 (flags?): 0x" << hex << header.unknown1 << dec << endl;
    cout << "    Fragment pointer: " << header.fragmentPointer << endl;
    cout << "    Unknown2: " << header.unknown2 << endl;

	return true;
}

//! \par 0x27 - ???
bool WLD::loadFragmentType0x27(WLD_State &state) const {
    return false;
}

//! \par 0x28 - ???
//! \code
//! u32 flags;
//! f32 xyz[3];
//! f32 rad;
//! \endcode
bool WLD::loadFragmentType0x28(WLD_State &state) const {

    struct Header {
        u32 flags;
        float x;
        float y;
        float z;
        float rad;
    } header = state.file->read<Header>();

    if (state.printing) {
        cout << "    Flags: " << hex << header.flags << dec << endl;
        cout << "    x: " << header.x << endl;
        cout << "    y: " << header.y << endl;
        cout << "    z: " << header.z << endl;
        cout << "    Radiosity: " << header.rad << endl;
    }

	return false;
}

//! \par 0x29 - ???
//! \code
//! u32  - Region Count
//! u32* - Region Array
//! u32  - String Length
//! c8*  - String
//! u32  - Region Type
//!    // -1 = unknown
//!    //  1 = water
//!    //  2 = lava
//!    // more exist, see azone code
//! \endcode
bool WLD::loadFragmentType0x29(WLD_State &state) const {

    struct Header {
        u32 regionCount;
        u32* regionArray;
        u32 strlen;
        char* str;
        u32 regionType;
    } header = state.file->read<Header>();

    if (state.printing) {
        cout << "    Regions: " << header.regionCount << endl;
        cout << "    Name length: " << header.strlen << endl;
        cout << "    Region type: " << header.regionType << endl;
    }

	return false;
}

//! \par 0x2a - ???
bool WLD::loadFragmentType0x2a(WLD_State &state) const {
	return false;
}

//! \par 0x2c - ???
bool WLD::loadFragmentType0x2c(WLD_State &state) const {
	return false;
}

//! \par 0x2d - ???
bool WLD::loadFragmentType0x2d(WLD_State &state) const {
    u32 unknown1 = state.file->read<u32>();
    u32 unknown2 = state.file->read<u32>();

    cout << "    Unknown1: " << unknown1 << endl;
    cout << "    Unknown2: " << unknown2 << endl;
	return false;
}

//! \par 0x2f - Animated Vertices
bool WLD::loadFragmentType0x2f(WLD_State &state) const {
	return false;
}

//! \par 0x30 - Texture transparency flags
//! \code
//! u32    - flags
//!     // 0x4 - skip pair of bytes
//! u32    - flags2
//!     // 0x1  - unknown
//!     // 0x2  - not transparent
//!     // 0x4  - masked
//!     // 0x8  - semitransparent and not masked
//!     // 0x10 - semitransparent and masked
//!     // 0x20 - not semitransparent but is masked
//!     // 0x80000000 - unknown
//! u32    - 'params2'
//! f32[2] - 'params3'
//! u32[2] - pair of bytes (optional)
//! u32    - 0x05 texture fragment pointer
//! \endcode
bool WLD::loadFragmentType0x30(WLD_State &state) const {

    struct Header {
        u32 flags;
        u32 flags2;
        u32 params2;
        float params3[2];
    } header = state.file->read<Header>();

    if (header.flags & 0xfffffffd) {
        cout << red << "    Unrecognized flag: 0x" << hex << header.flags << dec << normal << endl;
    }

    if (state.printing) {
        cout << "    flags2: 0x" << hex << header.flags2 << dec << endl;
        cout << "    'params2': " << header.params2 << endl;
        cout << "    'params3': " << header.params3[0] << " " << header.params3[1] << endl;
    }

	if (header.flags & ~0x4) {
        u32 unknown1 = state.file->read<u32>();
        u32 unknown2 = state.file->read<u32>();
        if (state.printing) {
            cout << "    Unknown1: " << unknown1 << endl;
            cout << "    Unknown2: " << unknown2 << endl;
        }
	}

	u32 textureReference = state.file->read<u32>();

    if (state.printing) {
        cout << "    Texture pointer: " << textureReference << endl;
    }

    bool masked = false;
    bool semitransparent = false;
    bool transparent = false;
    bool empty = false;

    if (header.flags2 == 0x0) {
        transparent = true;
    } else {
        if (header.flags2 & 0x2) {
            transparent = false;
        }
        if (header.flags2 & 0x4) {
            semitransparent = true;
        }
        if (header.flags2 & 0x8) {
            semitransparent = true;
            masked = true;
        }
        if (header.flags2 & 0x10) {
            masked = true;
        }
    }

    if (textureReference == 0) {
        empty = true;
    }

	return true;
}

//! \par 0x31 - Texture Array
//! Contains an array of textures in the order that they appear in the render list.
//! u32 - unknown
//! u32 - Number of textures in the list
//!
//! // Textures
//! u32 - Link to 0x30 texture transparency fragment.
bool WLD::loadFragmentType0x31(WLD_State &state) const {

    struct Header {
        u32 unknown;
        u32 textureCount;
    } header = state.file->read<Header>();

    if (state.printing) {
        cout << "    Unknown: " << header.unknown << endl;
    }

    shared_ptr<vector<shared_ptr<WLD_Texture>>> textureArray(new vector<shared_ptr<WLD_Texture>>());
    textureArray->resize(header.textureCount);

	for (u32 i = 0; i < textureArray->size(); i++) {
		u32 textureReference = state.file->read<u32>();
        if (state.printing) {
            cout << "    [" << i << "] - Fragment pointer to " << textureReference << endl;
        }
        textureArray->at(i) = state.textures[textureReference];
	}

    state.textureLists[state.frag_id] = textureArray;

	return true;
}

//! \par 0x32 - ???
bool WLD::loadFragmentType0x32(WLD_State &state) const {
	return false;
}

//! \par 0x33 - ???
bool WLD::loadFragmentType0x33(WLD_State &state) const {
	return false;
}

//! \par 0x34 - ???
//! u32[20] - unknown
//! u32     - Link to an 0x26 texture ??? fragment
bool WLD::loadFragmentType0x34(WLD_State &state) const {
    struct Header {
        u32 unknown[20];
        u32 fragmentLink;
    } header = state.file->read<Header>();

    for (u32 i = 0; i < 20; i++) {
        cout << "    Unknown " << i << ": " << header.unknown[i] << endl;
    }
    cout << "    Texture fragment link: " << header.fragmentLink << endl;

	return true;
}

//! \par 0x35 - empty
//! These fragments never seem to contain any additional data.
bool WLD::loadFragmentType0x35(WLD_State &state) const {
    if (state.frag.size != 4) {
        Log().warning() << "Non-empty 0x35 fragment!";
        return false;
    }
	return true;
}

//! \par 0x36 - Mesh
//! A fragment containing model data.
//! Includes both mesh and skin data.
//! \code
//! // Header
//! u32    - flags
//!     // 0x1     - unknown
//!     // 0x2     - unknown
//!     // 0x4000  - Indicates this is a placeable mesh.
//!     // 0x8000  - Indicates this is a zone mesh.
//!     // 0x10000 - unknown
//! u32    - Pointer to a 0x31 texture array fragment.
//! u32    - Pointer to a 0x2f animated vertices fragment.
//! u32    - Pointer to an unknown fragment.
//! u32    - Pointer to an 0x03 texture name fragment (unused)
//! f32[3] - Coordinates of the mesh center.
//! u32[2] - Unknown.
//! f32    - Radius of the mesh.
//! f32[3] - Location of the lower corner for the mesh bounding box.
//! f32[3] - Location of the upper corner for the mesh bounding box.
//! s16    - Number of vertices in the fragment.
//! s16    - Number of texture coordinates in the fragment.
//! s16    - Number of vertex normals in the fragment.
//! s16    - Number of 'color' elements in the fragment.
//! s16    - Number of triangles in the fragment.
//! s16    - Number of 'vertex pieces' in the fragment. (Animated meshes only.)
//! s16    - Number of 'triangle textures' in the fragment.
//! s16    - Number of 'vertex textures' in the fragment.
//! s16    - Number of 'data9' elements in the fragment. (Animated meshes only.)
//! s16    - A scale factor applied to the entire mesh upon loading.
//!
//! // Vertices
//! s16 - x
//! s16 - y
//! s16 - z
//!     // To apply these, add the center coordinate and multiply by (1.0f / pow(2, scale))
//!
//!
//! \endcode
bool WLD::loadFragmentType0x36(WLD_State &state) const {

    struct Header {
        u32 flags;
        u32 texturesLink;   //!< Link to a 0x31 texture array fragment
        u32 animationFragment;      //!< Link to a 0x2F animated vertices fragment
        u32 fragmentLink3;          //!< Link to a fragment containing ???
        u32 fragmentLink4;          //!< Link to a 0x03 texture name fragment (why?)
        f32 centerX;                //!< X coordinate of the mesh center
        f32 centerY;                //!< Y coordinate of the mesh center
        f32 centerZ;                //!< Z coordinate of the mesh center
        u32 params2[3];             //!< Unknown. (comment by openeq:48)
        f32 radius;                 //!< Radius of the mesh from the center.
        f32 minX;                   //
        f32 minY;                   // Min and max define the bounding box for the mesh.
        f32 minZ;                   //
        f32 maxX;                   //
        f32 maxY;                   //
        f32 maxZ;                   //
        s16 vertexCount;            //!< The number of vertices in the mesh.
        s16 texCoordsCount;         //!< The number of texture coordinates in the mesh.
        s16 normalsCount;           //!< The number of normals in the mesh.
        s16 colorCount;             //!< The number of colors in the mesh.
        s16 triangleCount;          //!< The number of triangles in the mesh.
        s16 vertexPieceCount;       //!< The number of vertex pieces in animated meshes only.
        s16 triangleTextureCount;   //!< The number of triangle textures in the mesh.
        s16 vertexTextureCount;     //!< The number of vertex textures in the mesh.
        s16 count9;                 //!< Only used in animated meshes. The number of 'data9' elements in the mesh.
                                    //!  Possibly texture coordinate related?
        s16 scale;                  //!< Scale factor applied to the entire mesh upon loading.
    } header = state.file->read<Header>();

    const f32 scale = 1.0f / (f32) (1 << header.scale);

    // just do static models for now, we can figure out the animated stuff later
    shared_ptr<Phobos::Model> model(new Phobos::Model());
    model->frames.resize(1);
    model->name = string((char*) &state.stringhash[-(int)state.frag.name]);

    if (state.printing) {
        Log().info() << "Mesh Name: " << model->name;
        if (header.texturesLink != 0) {
            Log().info() << "Texture Array Fragment: " << header.texturesLink;
        }
        if (header.animationFragment != 0) {
            Log().info() << "Animation Fragment: " << header.animationFragment;
        }
        if (header.fragmentLink3 != 0) {
            Log().info() << "Fragment Link 3: " << header.fragmentLink3;
        }
        if (header.fragmentLink4 != 0) {
            Log().info() << "Fragment Link 4: " << header.fragmentLink4;
        }
        cout << "    Center: <" << header.centerX << ", " << header.centerY << ", " << header.centerZ << ">" << endl;
        cout << "    params2: " << header.params2[0] << " " << header.params2[1] << " " << header.params2[2] << endl;
        cout << "    Radius: " << header.radius << endl;
        cout << "    min: <" << header.minX << ", " << header.minY << ", " << header.minZ << ">" << endl;
        cout << "    max: <" << header.maxX << ", " << header.maxX << ", " << header.maxZ << ">" << endl;
        cout << "    Scale: " << header.scale << endl;

        if (header.flags & 0x1) {
            Log().info() << "FLAG: Unknown (0x1)";
        }
        if (header.flags & 0x2) {
            Log().info() << "FLAG: Unknown (0x2)";
        }
        if (header.flags & 0x4000) {
            Log().info() << "This is a zone.";
        }
        if (header.flags & 0x8000) {
            Log().info() << "This is a placeable.";
        }
        if (!(header.flags & 0x4000) && !(header.flags & 0x8000)) {
            Log().error() << "    Not a zone or a placeable. Please check flags: 0x" << hex << header.flags << dec;
        }
        if (header.flags & 0x10000) {
            Log().info() << "FLAG: Unknown (0x10000)";
        }
    }

    if (header.flags & 0xfffe3ffc) {
        Log().error() << "    Unrecognized flag: 0x" << hex << header.flags << dec;
    }

    // Load the textures from the texture array
    /*
    auto wldTextures = state.textureLists[header.texturesLink];
    for (auto wldTexture : (*wldTextures)) {
        for (auto &file : wldTexture->filenames) {
            shared_ptr<Texture> texture(new Texture());
            texture->name = file;
            texture->texture_id = SOIL_load_OGL_texture(file.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_DDS_LOAD_DIRECT);
            model->addTexture(texture, texture->name);
            //! \bug this is a hack
            model->setGroupTexture(0, texture->name);
        }
    }*/

    // Load vertices

    Log().info() << header.vertexCount << " Vertices";

    for (int i = 0; i < header.vertexCount; i++) {

        struct WLD_Vertex {
            s16 x;
            s16 y;
            s16 z;
        } wldVertex = state.file->read<WLD_Vertex>();

        Vertex v;

        v.x = header.centerX + wldVertex.x * scale;
        v.y = header.centerY + wldVertex.y * scale;
        v.z = header.centerZ + wldVertex.z * scale;

        model->frames[0].vertices.push_back(v);
    }

    // Load texture coordinates.

    if (state.version == OLD) {

        Log().info() << header.texCoordsCount << " Texture Coordinates (old format)";

        for (int i = 0; i < header.texCoordsCount; i++) {

            struct WLD_TexCoord {
                s16 x;
                s16 y;
            } wldTexCoord = state.file->read<WLD_TexCoord>();

            TexCoord tc;
            tc.x = ((f64) wldTexCoord.x) / 256.0;
            tc.y = ((f64) wldTexCoord.y) / 256.0;
            model->textureCoordinates.push_back(tc);
        }
    } else {
        if (state.version != NEW) {
            Log().warning() << "Unknown version. Attempting to read texture coordinates anyways...";
        }

        Log().info() << header.texCoordsCount << " Texture Coordinates (new format)";

        for (int i = 0; i < header.texCoordsCount; i++) {

            struct WLD_TexCoord {
                f32 x;
                f32 y;
            } wldTexCoord = state.file->read<WLD_TexCoord>();

            TexCoord tc;
            tc.x = wldTexCoord.x;
            tc.y = wldTexCoord.y;
            model->textureCoordinates.push_back(tc);
		}
	}

	// Load normals

    Log().info() << header.normalsCount << " Normals";

	for (int i = 0; i < header.normalsCount; i++) {

        struct WLD_VertNorm {
        	s8 x;
        	s8 y;
        	s8 z;
        } wldVertexNormal = state.file->read<WLD_VertNorm>();

        Normal n;

        n.x = ((f32) wldVertexNormal.x) / 127.0f;
        n.y = ((f32) wldVertexNormal.y) / 127.0f;
        n.z = ((f32) wldVertexNormal.z) / 127.0f;
        n = glm::normalize(n);

        model->frames[0].vertex_normals.push_back(n);
	}

	// Read the colors.
	if (header.colorCount > 0) {
        Log().info() << header.colorCount << " Color elements";

	    for (s16 c = 0; c < header.colorCount; c++) {
            struct WLD_Color {
                u8 r;
                u8 g;
                u8 b;
                u8 a;
            } color = state.file->read<WLD_Color>();
	    }
	}

	// Read the triangles.
    Log().info() << header.triangleCount << " Triangles";

	for (int i = 0; i < header.triangleCount; i++) {

        struct WLD_Triangle {
            u16 flags;    //!< As far as I can tell, these are unused.
            u16 v1;
            u16 v2;
            u16 v3;
        } wldTriangle = state.file->read<WLD_Triangle>();

        if (state.printing) {
            //cout << "        Triangle " << i << ": <" << wldTriangle.v1 << ", " << wldTriangle.v2 << ", " << wldTriangle.v3 << ">";
            if (wldTriangle.flags != 0x0) {
                Log().error() << " Unknown flag: 0x" << hex << wldTriangle.flags << dec;
            }
            //cout << endl;
        }

        Triangle triangle;

		triangle.vertices[0] = wldTriangle.v3;
		triangle.vertices[1] = wldTriangle.v2;
		triangle.vertices[2] = wldTriangle.v1;

        model->addTriangle(triangle);
	}

    if (header.vertexPieceCount > 0) {
        Log().info() << header.vertexPieceCount << " Vertex Pieces";

        for (s32 i = 0; i < header.vertexPieceCount; i++) {
	        u32 unknown = state.file->read<u32>();

            if (state.printing) {
                cout << "        Unknown " << i << ": " << unknown << endl;
            }
	   }
    }

	// TODO: I think this is splitting triangles into groups and applying different textures to each group.

    Log().info() << header.triangleTextureCount << " Triangle Textures";

	u32 triCount = 0;

	for (int i = 0; i < header.triangleTextureCount; i++) {
		WLD_TextureMap textureMap = state.file->read<WLD_TextureMap>();
        if (state.printing) {
            //cout << "        Triangle Texture " << i << ": triangles=" << textureMap.triangleCount << " textureID=" << textureMap.textureID << endl;
        }

		for (int j = 0; j < textureMap.triangleCount; j++) {
            if (triCount >= model->triangleCount()) {
				break;
            }
			// TODO: figure out how skins will work with the model files
			//mesh->triangles[triCount].textureID = textureMap.textureID;
			triCount++;
		}
	}

    Log().info() << header.count9 << " data9 elements";

    for (s16 i = 0; i < header.count9; i++) {
        struct Data9 {
            u16 unknown1;
            u16 unknown2;
            u16 unknown3;
        } data9 = state.file->read<Data9>();
        //cout << "    Data9 " << i << ": u1=" << data9.unknown1 << " u2=" << data9.unknown2 << " u3=" << data9.unknown3 << endl;
    }

    state.models[state.frag_id] = model;

	return true;
}

//! \par 0x37 - ???
bool WLD::loadFragmentType0x37(WLD_State &state) const {
	return false;
}

}
}
