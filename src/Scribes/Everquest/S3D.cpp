//! \file S3D.cpp

// C++ includes
#include <iostream>
#include <sstream>
#include <algorithm>

// C includes
#include <cstring> // for memset

// Local includes
#include "Utility/console.h"
#include "System/Compression.h"

#include "S3D.h"

using namespace std;

namespace Scribe {
namespace Everquest {

// #############
// # Constants #
// #############

const u32 filenameCRC = 0x61580AC9;

// ###########
// # Structs #
// ###########

struct S3D_MasterHeader {
  u32 directoryOffset;  //!< Offset of the S3D directory.
  char magicNumber[4];  //!< A 4 byte magic cookie that always contains "PFS "
  u32 unknown;          //!< This always contains the value 131072.
};

struct S3D_DirectoryHeader {
  u32 fileCount;        //!< Number of files in the directory.
};

struct S3D_DirectoryEntry {
  u32 CRC;              //!< CRC hash of the filename. Calculated with the standard IEEE 802.3 Ethernet CRC-32 algorithm.
  u32 dataOffset;       //!< Position in the archive of the compressed data for this entry.
  u32 inflatedSize;     //!< The size of the data once inflated.
};

struct S3D_DataBlockHeader {
  u32 deflatedLength;   //!< Size of the data block when compressed.
  u32 inflatedLength;   //!< Size of the data block when uncompressed.
};

struct S3D_FilenameHeader {
  u32 filenameCount;    //!< Number of files.
};

struct S3D_FilenameEntry {
  u32 filenameLength;   //!< Length of the filename.
};

//! \todo Right now the scribe just treats the footer as another compressed file and spits out garbage.
struct S3D_Footer {
    char checkSteve[5]; //!< Always contains "STEVE"
    u32 dateUpdated;    //!< Used to track when a file is updated by Sony.
};

// ####################
// # Scribe functions #
// ####################

bool S3D::canOpen(const std::shared_ptr<std::string> filename) const {
    assert(filename);

    const shared_ptr<File> source(new File());

    if (!source->openDiskSourceBinary(filename)) {
        return false;
    }

    const shared_ptr<istream> file = source->readSource();

    assert(file);

    if (source->fileExtension().compare("s3d") == 0) {
        S3D_MasterHeader header;
        file->read((char*) &header, sizeof(S3D_MasterHeader));
        file->seekg(0);
        if (strncmp(header.magicNumber, "PFS ", 4) == 0) {
            return true;
        }
    }

    return false;
}

// #########
// # Print #
// #########

bool S3D::print(const shared_ptr<File> source) const {
    assert(source);
    assert(source->readSource());

    const shared_ptr<istream> file = source->readSource();

    // Read the master header.

    S3D_MasterHeader masterHeader;
    file->read((char*) &masterHeader, sizeof(S3D_MasterHeader));

    cout << "== " << yellow << "File Header" << normal << " ==" << endl;
    cout << blue << "    Directory Offset: " << normal << "0x" << hex << masterHeader.directoryOffset << dec << endl;
    if (strncmp(masterHeader.magicNumber, "PFS ", 4)) {
        cout << blue << "    Magic Number: " << red << string(masterHeader.magicNumber,4) << normal << endl;
    } else {
        cout << blue << "    Magic Number: " << normal << string(masterHeader.magicNumber,4) << endl;
    }
    if (masterHeader.unknown != 131072) {
        cout << green << "    Unknown: " << normal << masterHeader.unknown << endl;
    } else {
        cout << blue << "    Unknown: " << normal << masterHeader.unknown << endl;
    }

    // Read the directory header.

    file->seekg(masterHeader.directoryOffset);

    S3D_DirectoryHeader dir_header;
    file->read((char*) &dir_header, sizeof(S3D_DirectoryHeader));

    cout << "== " << yellow << "Directory Header" << normal << " ==" << endl;
    cout << blue << "    File Count: " << normal << dir_header.fileCount - 1 << endl;

    // Read the directory entries

    u32 running = 0;
    vector<u32> offsets;
    vector<u32> fileIndexes;
    vector<string> filenames;
    offsets.resize(dir_header.fileCount - 1);
    filenames.resize(dir_header.fileCount - 1);
    fileIndexes.resize(dir_header.fileCount - 1);

    for (u32 i = 0; i < dir_header.fileCount; i++) {
        S3D_DirectoryEntry dir_entry;
        file->read((char*) &dir_entry, sizeof(S3D_DirectoryEntry));

        //cout << "    === " << magenta << "Directory Entry " << i << normal << "===" << endl;
        //cout << blue << "        CRC: " << normal << dir_entry.CRC << endl;
        //cout << blue << "        Data Offset: " << normal << "0x" << hex << dir_entry.dataOffset << dec << endl;
        //cout << blue << "        Inflated Size: " << normal << dir_entry.inflatedSize << endl;

        // Check if this is the filename block.
        if (dir_entry.CRC == filenameCRC) {
            const u32 directoryStart = file->tellg();

            // Seek to the start of the compressed data
            file->seekg(dir_entry.dataOffset);

            char* decompressedData = decompressDataBlock(file, dir_entry.inflatedSize);

            // Rewind back to where we were.
            file->seekg(directoryStart);

            // Now we are seeking through the decompressed data, rather than the file.
            const S3D_FilenameHeader* filenameHeader = (S3D_FilenameHeader *) decompressedData;
            u32 pos = sizeof(S3D_FilenameHeader);
            cout << "        Number of files in archive: " << filenameHeader->filenameCount << endl;
            for (u32 i = 0; i < filenameHeader->filenameCount; i++) {
                const S3D_FilenameEntry* filenameEntry = (S3D_FilenameEntry *) &decompressedData[pos];
                char* filename = new char [filenameEntry->filenameLength + 1];
                filename[filenameEntry->filenameLength] = 0;
                memcpy(filename, &decompressedData[pos + sizeof(S3D_FilenameEntry)], filenameEntry->filenameLength);
                filenames[i].assign(filename);

                //cout << "        File " << i << ": " << string(filename) << endl;

                pos += sizeof(S3D_FilenameEntry) + filenameEntry->filenameLength;
            }

            delete decompressedData;
        } else {
            //cout << "        debug: running=" << running << " dataoffset=" << dir_entry.dataOffset << " fileindex: " << (u32) file->tellg() - 12 << endl;
            fileIndexes[running] = (u32) file->tellg() - 12;
            offsets[running] = dir_entry.dataOffset;
            running++;
        }
    }

    // Down here we do something with those mysterious offsets we've been keeping track of.
    for(u32 i = dir_header.fileCount - 2; i > 0; i--) {
        for(u32 j = 0; j < i; j++) {
            if (offsets[j] > offsets[j+1]) {
                swap(offsets[j], offsets[j + 1]);
                swap(fileIndexes[j], fileIndexes[j + 1]);
            }
        }
    }

    for (u32 i = 0; i < dir_header.fileCount - 1; i++) {
        cout << " File " << i << " " << filenames[i] << "\t\t" << hex << fileIndexes[i] << " " << offsets[i] << dec << endl;
    }

    return true;
}

// ########
// # Read #
// ########

//! Read files from an S3D archive.
vector<shared_ptr<File> > S3D::readFiles(const shared_ptr<File> archive) const {
    assert(archive);
    assert(archive->readSource());

    shared_ptr<istream> file = archive->readSource();
    vector<shared_ptr<File> > files;

    // Read the master file header

    S3D_MasterHeader masterHeader;
    file->read((char*) &masterHeader, sizeof(S3D_MasterHeader));

    assert(strncmp(masterHeader.magicNumber, "PFS ", 4) == 0);

    // Go to the offset specified in the master header to get the directory header.
    file->seekg(masterHeader.directoryOffset);

    S3D_DirectoryHeader directoryHeader;
    file->read((char*) &directoryHeader, sizeof(S3D_DirectoryHeader));

    vector<u32> offsets;
    vector<u32> fileIndexes;
    vector<string> filenames;
    u32 running = 0;

    // for some reason the number of files s3d claims to contain is actually 1 greater than what it contains
    fileIndexes.resize(directoryHeader.fileCount - 1, 0);
    offsets.resize(directoryHeader.fileCount - 1);
    filenames.resize(directoryHeader.fileCount - 1);

    for (u32 i = 0; i < directoryHeader.fileCount; i++) {
        S3D_DirectoryEntry dir;
        file->read((char*) &dir, sizeof(S3D_DirectoryEntry));

        // Check to see if this is the filename index block.
        if (dir.CRC == filenameCRC) {
            u32 directoryStart = file->tellg();
            file->seekg(dir.dataOffset);                     // Seek to the start of the directory.

            //! \todo Rewrite this to use stringstreams.
            char* inflatedData = new char [dir.inflatedSize];   // Create a buffer to store the data after we inflate it.
            memset(inflatedData, 0, dir.inflatedSize);          //! Initialize the buffer to 0's. \todo better way to do this?
            u32 inflateIndex = 0;                               // We have to read the data blocks one at a time, so we use an index to keep track.
            while (inflateIndex < dir.inflatedSize) {
                S3D_DataBlockHeader dataBlock;
                file->read((char*) &dataBlock, sizeof(S3D_DataBlockHeader));       // Read size info for this block.
                char* deflatedData = new char [dataBlock.deflatedLength];   // Create a buffer for the compressed data.
                file->read(deflatedData, dataBlock.deflatedLength);         // Read the data, decompress it and stick it in the inflated buffer.
                decompressZlib(deflatedData, inflatedData + inflateIndex, dataBlock.deflatedLength, dataBlock.inflatedLength);
                delete deflatedData;                                        // Delete the compressed data once we're done with it.
                inflateIndex += dataBlock.inflatedLength;                   // Move the inflation index to the next unused part of the buffer.
            }
            file->seekg(directoryStart);

            // Now we are seeking through the decompressed data, rather than the file.
            S3D_FilenameHeader* filenameHeader = (S3D_FilenameHeader *) inflatedData;
            u32 pos = sizeof(S3D_FilenameHeader);
            for (u32 i = 0; i < filenameHeader->filenameCount; i++) {
                S3D_FilenameEntry* filenameEntry = (S3D_FilenameEntry *) &inflatedData[pos];
                char* filename = new char [filenameEntry->filenameLength + 1];
                filename[filenameEntry->filenameLength] = 0;
                memcpy(filename, &inflatedData[pos + sizeof(S3D_FilenameEntry)], filenameEntry->filenameLength);
                filenames[i].assign(filename);
                pos += sizeof(S3D_FilenameEntry) + filenameEntry->filenameLength;
            }
        } else {
            fileIndexes[running] = (u32) file->tellg() - 12;
            offsets[running] = dir.dataOffset;
            running++;
        }
    }

    // Down here we do something with those mysterious offsets we've been keeping track of.
    for(u32 i = directoryHeader.fileCount - 2; i > 0; i--) {
        for(u32 j = 0; j < i; j++) {
            if (offsets[j] > offsets[j+1]) {
                swap(offsets[j], offsets[j + 1]);
                swap(fileIndexes[j], fileIndexes[j + 1]);
            }
        }
    }

    // now read the files into the File structures

    for (u32 i = 0; i < directoryHeader.fileCount - 1; i++) {
        // read the directory entry for this file
        S3D_DirectoryEntry directoryEntry;
        file->seekg(fileIndexes[i]);
        file->read((char*) &directoryEntry, sizeof(S3D_DirectoryEntry));

        // read the data block for this file
        file->seekg(directoryEntry.dataOffset);

        //! \todo rewrite this to not use char arrays
        char* buffer = new char[directoryEntry.inflatedSize];
        u64 inf = 0;
        while(inf < directoryEntry.inflatedSize) {
            S3D_DataBlockHeader dataBlock;
            file->read((char*) &dataBlock, sizeof(S3D_DataBlockHeader));
            char* defBuffer = new char[dataBlock.deflatedLength];
            file->read(defBuffer, dataBlock.deflatedLength);
            decompressZlib(defBuffer, (char *) buffer + inf, dataBlock.deflatedLength, dataBlock.inflatedLength);
            delete defBuffer;
            inf += dataBlock.inflatedLength;
        }

        //! \todo when rewriting the char arrays out, make sure the stringstream is not copying the string uselessly
        shared_ptr<stringstream> data(new stringstream(string((char*) buffer, directoryEntry.inflatedSize), ios_base::in | ios_base::out | ios_base::binary));
        shared_ptr<File> decompressedFile(new File(data));
        decompressedFile->filename->assign(filenames[i]);
        //cout << "filename: " << filenames[i] << endl;
        files.push_back(decompressedFile);

        delete buffer;
    }

    return files;
}

// #########
// # Write #
// #########

//! \todo Implement this function.
bool S3D::writeFiles(const shared_ptr<File> archive, const vector<shared_ptr<File> > files) const {
    assert(archive);

    // TODO
    return false;
}

// ####################
// # Helper functions #
// ####################

char* S3D::decompressDataBlock(shared_ptr<istream> file, const u32 decompressedSize) const {
    assert(file);

    char* result = new char[decompressedSize];
    memset(result, 0, decompressedSize);

    // Read data blocks until we've filled the allocated emmory.
    u32 inflateIndex = 0;
    while (inflateIndex < decompressedSize) {
        S3D_DataBlockHeader dataBlock;
        file->read((char*) &dataBlock, sizeof(S3D_DataBlockHeader));       // Read size info for this block.

        char* deflatedData = new char [dataBlock.deflatedLength];   // Create a buffer for the compressed data.
        file->read(deflatedData, dataBlock.deflatedLength);          // Read the data, decompress it and stick it in the inflated buffer.
        decompressZlib(deflatedData, result + inflateIndex, dataBlock.deflatedLength, dataBlock.inflatedLength);
        delete deflatedData;                                        // Delete the compressed data once we're done with it.
        inflateIndex += dataBlock.inflatedLength;                   // Move the inflation index to the next unused part of the buffer.
   }

    return result;
}

}
}