#ifndef SCRIBE_EVERQUEST_S3D_H
#define SCRIBE_EVERQUEST_S3D_H

#include <string>
#include <vector>
#include <fstream>

#include "Scribes/Abstract/Archive.h"

#include "types.h"

namespace Scribe {
namespace Everquest {

//! \brief A Scribe for the S3D archive files used by Everquest.
//! \details
//!
//! \par Structure of S3D files
//! The first 4 bytes of an S3D file is an offset containing the location of the directory
//! block. Immediately after this is 8 additional bytes containing a "magic cookie" value.
//! \see S3D_MasterHeader
//!
//! \par Directory block
//! The directory block starts with a 4 byte value containing the number of data blocks in the archive.
//! This will be followed by that number of directory entries. Each directory entry is composed of
//! three 4 byte values: the CRC hash of the filename, an offset pointer to the compressed file data block,
//! and the size of the file data once it is uncompressed.
//! One of the directory blocks will contain a list of filenames for all the files in the archive, this block
//! will always have a CRC hash of 0x61580AC9.
//! \see S3D_DirectoryHeader
//! \see S3D_DirectoryEntry
//!
//! \par Filename block
//! The filename block is a compressed data block just like any other. The difference is that the compressed data
//! does not contain a file, but instead a list of filenames for the files stored in the archive.
//! The first 4 bytes of the decompressed filename block is a count of the number of filenames in the block.
//! This is followed by that number of strings stored in Pascal format (the size of the string as a 4 byte value,
//! followed by the non-null terminated string.)
//! \see S3D_FilenameHeader
//! \see S3D_FilenameEntry
//!
//! \par Data block
//! Data blocks have a 2 byte header. The first byte is the size of the block when compressed and the second byte is
//! the size of the block after decompression. These counts do not include the header in the size of the block.
//! \see S3D_DataBlockHeader
//!
//! \par Compression
//! zlib
//!
//! \see S3D_Footer
class S3D : public Abstract::Archive {
public:
    // inherited from Abstract::Base
    bool canOpen(std::shared_ptr<std::string> file) const override;
    std::string getPersonalityName() const override {
        return "S3D";
    }

    // inherited from Abstract::Archive
    std::vector<std::shared_ptr<File> > readFiles(std::shared_ptr<File> archive) const override;
    bool writeFiles(std::shared_ptr<File> archive, std::vector<std::shared_ptr<File>> files) const override;

    // deprecated
    bool print(std::shared_ptr<File> source) const;
private:
    char* decompressDataBlock(std::shared_ptr<std::istream> file, u32 decompressedSize) const;
};

}
}

#endif

