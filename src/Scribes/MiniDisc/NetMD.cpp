#include "NetMD.h"

using namespace std;

namespace Scribe {
namespace MiniDisc {

// ###########
// # Structs #
// ###########

// ####################
// # Scribe functions #
// ####################

bool NetMD::canOpen(const std::shared_ptr<std::string> filename) const {
    assert(filename);

    // TODO: NetMD can only read from USB, so the Scribe framework will
    // have to be refactored to handle that...
    return false;
}

// #########
// # Print #
// #########

bool NetMD::print(const shared_ptr<File> source) const {
    assert(source);
    assert(source->readSource());
    return false;
}

}
}
