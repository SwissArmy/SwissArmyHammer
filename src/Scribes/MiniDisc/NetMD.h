#ifndef SCRIBE_MINIDISC_NETMD_H
#define SCRIBE_MINIDISC_NETMD_H

#include "types.h"
#include "Scribes/Abstract/Base.h"

namespace Scribe {
namespace MiniDisc {

class NetMD : public Abstract::Base {
public:
    // inherited from Scribe
    bool print(const std::shared_ptr<File> source) const;
    bool canOpen(const std::shared_ptr<std::string> file) const;
private:
};

}
}

#endif
