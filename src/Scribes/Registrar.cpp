// C includes
#include <cassert>

#include <filesystem>

#include "Utility/log.h"

#include "types.h"

#include "Registrar.h"

using namespace std;

namespace Scribe {

vector<shared_ptr<Abstract::Base>> Registrar::scribes;

void Registrar::add(shared_ptr<Abstract::Base> scribe) {
    scribes.push_back(scribe);
}

vector<shared_ptr<Abstract::Base>> Registrar::determineScribes(shared_ptr<string> filename) {
    LOG_ENTER();
    assert(!filename->empty());

    vector<shared_ptr<Abstract::Base>> results;

    Log().info() << "Checking if file '" << filename->c_str() << "' exists";

    if (!std::__fs::filesystem::exists(filename->c_str())) {
        Log().error() << "'" << *filename << "' does not exist.";
        LOG_EXIT();
        return results;
    }

    Log().info() << "Checking " << scribes.size() << " scribes for compatibility with file '" << filename->c_str() << "'";

    for (const shared_ptr<Abstract::Base> &s: scribes) {

        Log().info() << "Can " << s->getPersonalityName() << " open?";

        if (s->canOpen(filename)) {
            Log().info() << "  YES";
            results.push_back(s);
        } else {
            Log().info() << "  NO";
        }
    }

    LOG_EXIT();
    return results;
}

}