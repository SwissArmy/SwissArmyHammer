#ifndef SCRIBE_ABSTRACT_TEXTURE_H
#define SCRIBE_ABSTRACT_TEXTURE_H

#include <vector>

#include "Phobos/Texture.h"

#include "Base.h"

namespace Scribe {
namespace Abstract {

class Texture : public Base {
public:
    std::string getPersonalityName() const override {
        return "Texture";
    };

    virtual std::vector<std::shared_ptr<Phobos::Texture>> readTextures(const std::shared_ptr<File> source) const {
        Log().error() << classname(typeid(this).name()) << "has not implemented " << __PRETTY_FUNCTION__;
        std::vector<std::shared_ptr<Phobos::Texture>> result;
        return result;
    }
};

}
}

#endif