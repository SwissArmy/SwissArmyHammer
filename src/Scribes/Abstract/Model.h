#ifndef SCRIBE_ABSTRACT_MODEL_H
#define SCRIBE_ABSTRACT_MODEL_H

// C++ includes
#include <vector>

// Phobos includes
#include "Phobos/Model.h"

#include "Base.h"

namespace Scribe {
namespace Abstract {

//! An interface for any Scribe that reads file formats containing 3D models.
class Model : public Base {
public:
    std::string getPersonalityName() const override {
        return "Model";
    };

    virtual std::vector<std::shared_ptr<Phobos::Model>> readModels(const std::shared_ptr<File> source) const {
        Log().error() << classname(typeid(this).name()) << "has not implemented " << __PRETTY_FUNCTION__;
        std::vector<std::shared_ptr<Phobos::Model>> result;
        return result;
    }

    virtual bool writeModels(const std::shared_ptr<File> target, const std::vector<std::shared_ptr<Phobos::Model>> models) const {
        Log().error() << classname(typeid(this).name()) << "has not implemented " << __PRETTY_FUNCTION__;
        return false;
    }
};

}
}

#endif
