//
// Created by nova on 10/19/24.
//

#ifndef SCRIBE_ABSTRACT_PERSONALITY_H
#define SCRIBE_ABSTRACT_PERSONALITY_H
#include <string>

namespace Scribe::Abstract {
//! Scribes that implement Personality can be queried for user-readable information about the scribe's capabilities,
//! implementation, etc.
class Personality {
public:
    virtual ~Personality() = default;

    virtual std::string getPersonalityName() const {
        return "Personality";
    }

    virtual std::vector<std::string> getPersonalityTags() const {
        std::vector<std::string> tags;
        getPersonalityTags(tags);
        return tags;
    }
protected:
    virtual void getPersonalityTags(std::vector<std::string> tags) const {
        tags.emplace_back("Personality");
    }
};
}

#endif //SCRIBE_ABSTRACT_PERSONALITY_H
