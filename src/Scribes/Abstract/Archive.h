#ifndef SCRIBE_ABSTRACT_ARCHIVE_H
#define SCRIBE_ABSTRACT_ARCHIVE_H

#include <vector>

#include <boost/current_function.hpp>

#include "Base.h"

namespace Scribe {
namespace Abstract {

//! An interface for any Scribe that reads file formats containing compressed
//! archive data.
class Archive : public Base {
public:
    std::string getPersonalityName() const override {
        return "Archive";
    }

    //! Reads the files in the archive into memory and returns them in a vector.
    virtual std::vector<std::shared_ptr<File>> readFiles(const std::shared_ptr<File> archive) const {
        Log().error() << classname(typeid(this).name()) << "has not implemented " << BOOST_CURRENT_FUNCTION;
        std::vector<std::shared_ptr<File>> result;
        return result;
    }

    //! Writes a set of files into the archive file.
    virtual bool writeFiles(const std::shared_ptr<File> archive, const std::vector<std::shared_ptr<File>> files) const {
        Log().error() << classname(typeid(this).name()) << "has not implemented " << BOOST_CURRENT_FUNCTION;
        return false;
    }
};

}
}

#endif
