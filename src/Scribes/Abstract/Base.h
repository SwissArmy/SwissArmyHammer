#ifndef SCRIBE_ABSTRACT_BASE_H
#define SCRIBE_ABSTRACT_BASE_H

#include "Personality.h"
#include "System/File.h"
#include "Utility/log.h"

namespace Scribe {
namespace Abstract {

//! An interface for scribes that read and write a complex filetype.
//! Scribes should maintain no state between method calls.
class Base : public Personality {
public:
    // Overrides Personality
    std::string getPersonalityName() const override {
        return "Base";
    }

    //! Returns true if the file can be opened by this scribe, false otherwise.
    virtual bool canOpen(const std::shared_ptr<std::string> source) const {
        Log().error() << classname(typeid(this).name()) << "has not implemented canOpen"; return false;
    }


protected:
    std::shared_ptr<std::istream> getFileStream(const std::shared_ptr<std::string> filename) {
        std::shared_ptr<std::istream> result;

        const std::shared_ptr<File> file(new File());

        if (!file->openDiskSourceBinary(filename)) {
            return result;
        }

        result = file->readSource();
        return result;
    }
};

}
}

#endif
