#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstring>

#include "ESM.h"
#include "types.h"
#include "Utility/console.h"

using namespace std;

namespace Scribe {
namespace Morrowind {

// ###########
// # Structs #
// ###########

struct RecordHeader {
    u32 size;
    u32 unknown;
    u32 flags;
};

struct SubRecordHeader {
    c8 name[5];
    u32 size;
};

struct Record_HEDR {
    f32 version;
    u32 unknown;
    c8 company_name[32];
    c8 description[256];
    u32 num_records;
};

struct CreatureData {
    u32 type;   //!< 0 = Creature, 1 = Daedra, 2 = Undead, 3 = Humanoid
    u32 level;
    u32 strength;
    u32 intelligence;
    u32 willpower;
    u32 agility;
    u32 speed;
    u32 endurance;
    u32 personality;
    u32 luck;
    u32 health;
    u32 spell_points;
    u32 fatigue;
    u32 soul;
    u32 combat;
    u32 magic;
    u32 stealth;
    u32 attack_min_1;
    u32 attack_max_1;
    u32 attack_min_2;
    u32 attack_max_2;
    u32 attack_min_3;
    u32 attack_max_3;
    u32 gold;
};

struct ClassData {
    u32 attribute_id_1;
    u32 attribute_id_2;
    u32 specialization; //!< 0 = Combat, 1 = Magic, 2 = Stealth
    u32 minor_id_1;
    u32 major_id_1;
    u32 minor_id_2;
    u32 major_id_2;
    u32 minor_id_3;
    u32 major_id_3;
    u32 minor_id_4;
    u32 major_id_4;
    u32 minor_id_5;
    u32 major_id_5;
    u32 flags;  //!< 0x0001 = Playable
    u32 auto_calc_flags;
	/* 0x00001 = Weapon
	   0x00002 = Armor
	   0x00004 = Clothing
       0x00008 = Books
       0x00010 = Ingredient
	   0x00020 = Picks
	   0x00040 = Probes
	   0x00080 = Lights
	   0x00100 = Apparatus
	   0x00200 = Repair
	   0x00400 = Misc
	   0x00800 = Spells
	   0x01000 = Magic Items
	   0x02000 = Potions
	   0x04000 = Training
	   0x08000 = Spellmaking
	   0x10000 = Enchanting
	   0x20000 = Repair Item*/
};

struct RankData {
    u32 attribute_1;
    u32 attribute_2;
    u32 first_skill;
    u32 second_skill;
    u32 faction;
};

struct FactionData {
    u32 attribute_id_1;
    u32 attribute_id_2;
    RankData ranks[10];
    u32 skill_id[6];
    u32 unknown;
    u32 flags;  //!< 1 = Hidden from player
};

struct SkillBonus {
    u32 skill_id;
    u32 bonus;
};

struct RaceData {
    SkillBonus skill_bonuses[7];
    u32 strength[2]; //!< 0 = male, 1 = female
    u32 intelligence[2];
    u32 willpower[2];
    u32 agility[2];
    u32 speed[2];
    u32 endurance[2];
    u32 personality[2];
    u32 luck[2];
    f32 height[2];
    f32 weight[2];
    u32 flags; //!< 1 = Playable, 2 = Beast race
};

// ####################
// # Scribe functions #
// ####################

bool ESM::canOpen(const std::shared_ptr<std::string> filename) const {
    assert(filename);

    shared_ptr<File> source(new File());

    if (!source->openDiskSourceBinary(filename)) {
        return false;
    }

    shared_ptr<istream> file = source->readSource();

    assert(file);

    if (source->fileExtension().compare("esm") == 0) {
        // TODO: check magic cookie too
        // TODO: can this scribe open ESP files too?
        return true;
    }

    return false;
}

// #########
// # Print #
// #########

bool ESM::print(const shared_ptr<File> source) const {
    assert(source);
    assert(source->readSource());

    ESMScribeState state;
    state.inFile = source->readSource();
    state.outFile = nullptr;
    state.printing = true;

    // repeatedly read records until we reach the end of the file
    RecordHeader record;
    c8 record_name[5];
    while (!state.inFile->eof()) {
        state.inFile->read((char*) record_name, sizeof(c8) * 4);
        record_name[4] = '\0';
        state.inFile->read((char*) &record, sizeof(RecordHeader));
        if (state.printing) {
            cout << "== " << yellow << record_name << " record" << normal;
            cout << " (" << record.size << " bytes)" << " ==" << endl;
        }
        streampos record_end = state.inFile->tellg();  // Header bytes are not included in the size of the record.
        record_end += record.size;
        bool result;
        if (strcmp(record_name, "TES3") == 0) {
            result = record_TES3(record_end, state);
        } else if (strcmp(record_name, "GMST") == 0) {
            result = record_GMST(record_end, state);
        } else if (strcmp(record_name, "GLOB") == 0) {
            result = record_GLOB(record_end, state);
        } else if (strcmp(record_name, "CLAS") == 0) {
            result = record_CLAS(record_end, state);
        } else if (strcmp(record_name, "FACT") == 0) {
            result = record_FACT(record_end, state);
        } else if (strcmp(record_name, "RACE") == 0) {
            result = record_RACE(record_end, state);
        } else if (strcmp(record_name, "SOUN") == 0) {
            result = record_SOUN(record_end, state);
        } else if (strcmp(record_name, "SKIL") == 0) {
            result = record_SKIL(record_end, state);
        } else if (strcmp(record_name, "MGEF") == 0) {
            result = record_MGEF(record_end, state);
        } else if (strcmp(record_name, "SCPT") == 0) {
            result = record_SCPT(record_end, state);
        } else if (strcmp(record_name, "REGN") == 0) {
            result = record_REGN(record_end, state);
        } else if (strcmp(record_name, "BSGN") == 0) {
            result = record_BSGN(record_end, state);
        } else if (strcmp(record_name, "LTEX") == 0) {
            result = record_LTEX(record_end, state);
        } else if (strcmp(record_name, "STAT") == 0) {
            result = record_STAT(record_end, state);
        } else if (strcmp(record_name, "DOOR") == 0) {
            result = record_DOOR(record_end, state);
        } else if (strcmp(record_name, "MISC") == 0) {
            result = record_MISC(record_end, state);
        } else if (strcmp(record_name, "WEAP") == 0) {
            result = record_WEAP(record_end, state);
        } else if (strcmp(record_name, "CONT") == 0) {
            result = record_CONT(record_end, state);
        } else if (strcmp(record_name, "SPEL") == 0) {
            result = record_SPEL(record_end, state);
        } else if (strcmp(record_name, "CREA") == 0) {
            result = record_CREA(record_end, state);
        } else if (strcmp(record_name, "BODY") == 0) {
            result = record_BODY(record_end, state);
        } else if (strcmp(record_name, "LIGH") == 0) {
            result = record_LIGH(record_end, state);
        } else if (strcmp(record_name, "ENCH") == 0) {
            result = record_ENCH(record_end, state);
        } else if (strcmp(record_name, "NPC_") == 0) {
            result = record_NPC_(record_end, state);
        } else if (strcmp(record_name, "ARMO") == 0) {
            result = record_ARMO(record_end, state);
        } else if (strcmp(record_name, "CLOT") == 0) {
            result = record_CLOT(record_end, state);
        } else if (strcmp(record_name, "REPA") == 0) {
            result = record_REPA(record_end, state);
        } else if (strcmp(record_name, "ACTI") == 0) {
            result = record_ACTI(record_end, state);
        } else if (strcmp(record_name, "LOCK") == 0) {
            result = record_LOCK(record_end, state);
        } else if (strcmp(record_name, "PROB") == 0) {
            result = record_PROB(record_end, state);
        } else if (strcmp(record_name, "INGR") == 0) {
            result = record_INGR(record_end, state);
        } else if (strcmp(record_name, "BOOK") == 0) {
            result = record_BOOK(record_end, state);
        } else if (strcmp(record_name, "ALCH") == 0) {
            result = record_ALCH(record_end, state);
        } else if (strcmp(record_name, "LEVI") == 0) {
            result = record_LEVI(record_end, state);
        } else if (strcmp(record_name, "LEVC") == 0) {
            result = record_LEVC(record_end, state);
        } else if (strcmp(record_name, "CELL") == 0) {
            result = record_CELL(record_end, state);
        } else if (strcmp(record_name, "LAND") == 0) {
            result = record_LAND(record_end, state);
        } else if (strcmp(record_name, "PGRD") == 0) {
            result = record_PGRD(record_end, state);
        } else if (strcmp(record_name, "SNDG") == 0) {
            result = record_SNDG(record_end, state);
        } else if (strcmp(record_name, "DIAL") == 0) {
            result = record_DIAL(record_end, state);
        } else if (strcmp(record_name, "INFO") == 0) {
            result = record_INFO(record_end, state);
        }
        if (!result) {
            cout << red << "Need to implement " << record_name << "!" << normal << endl;
            cin.get();
        }
        state.inFile->seekg(record_end);
    }
    return false;
}

// ####################
// # Helper functions #
// ####################

bool ESM::read_subrecord(u32 record_end, SubRecordHeader& subrecord, ESMScribeState &state) const {
    if (state.inFile->tellg() >= record_end - sizeof(SubRecordHeader)) {
        return false;
    }
    state.inFile->read((char*) subrecord.name, sizeof(c8) * 4);
    subrecord.name[4] = '\0';
    state.inFile->read((char*) &subrecord.size, sizeof(u32));

    if (state.printing) {
        cout << "    == " << magenta << subrecord.name << " subrecord" << normal;
        cout << " (" << subrecord.size << " bytes)" << " ==" << endl;
    }
    return true;
}

string ESM::read_string(u32 length, ESMScribeState &state) const {
    c8* read_str = new c8[length + 1];
    state.inFile->read((char*) read_str, sizeof(c8) * length);
    read_str[length] = '\0';
    string result(read_str);
    delete read_str;
    return result;
}

//! Reads a main header record.
bool ESM::record_TES3(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        if (strcmp(subrecord.name, "HEDR") == 0) {
            Record_HEDR rec;
            state.inFile->read((char*) &rec, sizeof(Record_HEDR));
            if (state.printing) {
                cout << "        " << blue << "Version: " << normal << rec.version << endl;
                cout << "        " << blue << "Unknown: " << normal << rec.unknown << endl;
                cout << "        " << blue << "Company Name: " << normal << rec.company_name << endl;
                cout << "        " << blue << "Description: " << normal << rec.description << endl;
                cout << "        " << blue << "Number of Records: " << normal << rec.num_records << endl;
            }
        } else if (strcmp(subrecord.name, "MAST") == 0) {
            string master_filename = read_string(subrecord.size, state);
            if (state.printing) {
                cout << "        " << blue << "Master file: " << normal << master_filename << endl;
            }
        } else if (strcmp(subrecord.name, "DATA") == 0) {
            u64 master_size;
            state.inFile->read((char*) &master_size, sizeof(u64));
            if (state.printing) {
                cout << "        " << blue << "Size of master file: " << normal << master_size << endl;
            }
        } else {
            state.inFile->seekg(subrecord.size, ios_base::cur);
        }
    }
    return true;
}

//! Reads a game setting record.
bool ESM::record_GMST(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        if (strcmp(subrecord.name, "NAME") == 0) {
            string setting_id = read_string(subrecord.size, state);
            if (state.printing) {
                cout << "        " << blue << "Setting ID: " << normal << setting_id << endl;
            }
        } else if (strcmp(subrecord.name, "STRV") == 0) {
            string strv = read_string(subrecord.size, state);
            if (state.printing) {
                cout << "        " << blue << "Setting value (string): " << normal << strv << endl;
            }
        } else if (strcmp(subrecord.name, "INTV") == 0) {
            u32 intv;
            state.inFile->read((char*) &intv, sizeof(u32));
            if (state.printing) {
                cout << "        " << blue << "Setting value (integer): " << normal << intv << endl;
            }
        } else if (strcmp(subrecord.name, "FLTV") == 0) {
            f32 fltv;
            state.inFile->read((char*) &fltv, sizeof(f32));
            if (state.printing) {
                cout << "        " << blue << "Setting value (float): " << normal << fltv << endl;
            }
        } else {
            cout << red << "Need to implement GMST." << subrecord.name << "!" << normal << endl;
            return false;
        }
    }
    return true;
}

//! Reads a global setting record.
bool ESM::record_GLOB(u32 record_end, ESMScribeState &state) const {

    c8 type;

    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        if (strcmp(subrecord.name, "NAME") == 0) {
            string global_id = read_string(subrecord.size, state);
            if (state.printing) {
                cout << "        " << blue << "Global ID: " << normal << global_id << endl;
            }
        } else if (strcmp(subrecord.name, "FNAM") == 0) {
            state.inFile->read((char*) &type, sizeof(c8));
            if (state.printing) {
                cout << "        " << blue << "Type: " << normal << type << endl;
            }
        } else if (strcmp(subrecord.name, "FLTV") == 0) {
            f32 fltv;
            state.inFile->read((char*) &fltv, sizeof(f32));
            if (state.printing) {
                cout << "        " << blue << "Value: " << normal << fltv << endl;
            }
        } else {
            cout << red << "Need to implement GLOB." << subrecord.name << "!" << normal << endl;
            return false;
        }
    }
    return true;
}

//! Reads a class definition record.
bool ESM::record_CLAS(u32 record_end, ESMScribeState &state) const {

    string id;
    string name;
    ClassData data;
    string description;

    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        if (strcmp(subrecord.name, "NAME") == 0) {
            id = read_string(subrecord.size, state);
        } else if (strcmp(subrecord.name, "FNAM") == 0) {
            name = read_string(subrecord.size, state);
        } else if (strcmp(subrecord.name, "CLDT") == 0) {
            state.inFile->read((char*) &data, sizeof(ClassData));
        } else if (strcmp(subrecord.name, "DESC") == 0) {
            description = read_string(subrecord.size, state);
        } else {
            cout << red << "Need to implement CLAS." << subrecord.name << "!" << normal << endl;
            return false;
        }
    }

    if (state.printing) {
        cout << "        " << blue << "ID: " << normal << id << endl;
        cout << "        " << blue << "Name: " << normal << name << endl;
        cout << "        " << blue << "Attribute IDS: " << normal << data.attribute_id_1 << ", " << data.attribute_id_2 << endl;
        cout << "        " << blue << "Specialization: " << normal;
        if (data.specialization == 0) {
            cout << "Combat" << endl;
        } else if (data.specialization == 1) {
            cout << "Magic" << endl;
        } else if (data.specialization == 2) {
            cout << "Stealth" << endl;
        } else {
            cout << red << "Unknown (" << data.specialization << ")" << normal << endl;
        }
        cout << "        " << blue << "Major Skill IDs: " << normal << data.major_id_1 << ", " << data.major_id_2 << ", ";
        cout << data.major_id_3 << ", " << data.major_id_4 << ", " << data.major_id_5 << endl;
        cout << "        " << blue << "Minor Skill IDs: " << normal << data.minor_id_1 << ", " << data.minor_id_2 << ", ";
        cout << data.minor_id_3 << ", " << data.minor_id_4 << ", " << data.minor_id_5 << endl;
        cout << "        " << blue << "Playable: " << normal;
        if (data.flags == 0) {
            cout << "No" << endl;
        } else if (data.flags == 1) {
            cout << "Yes" << endl;
        } else {
            cout << red << "Unknown" << normal << endl;
        }
        cout << "        " << green << "auto_calc_flags: " << normal << data.auto_calc_flags << endl;
        cout << "        " << blue << "Class Description: " << normal << description << endl;
    }

    return true;
}

//! Reads a faction definition record.
bool ESM::record_FACT(u32 record_end, ESMScribeState &state) const {
    string id;
    string name;
    string rank_name;
    FactionData data;
    //string standing;

    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        if (strcmp(subrecord.name, "NAME") == 0) {
            id = read_string(subrecord.size, state);
        } else if (strcmp(subrecord.name, "FNAM") == 0) {
            name = read_string(subrecord.size, state);
        } else if (strcmp(subrecord.name, "RNAM") == 0) {
             rank_name = read_string(subrecord.size, state);
             if (state.printing) {
                cout << "    " << blue << "Rank name: " << normal << rank_name << endl;
             }
        } else if (strcmp(subrecord.name, "FADT") == 0) {
            state.inFile->read((char*) &data, sizeof(FactionData));
        } else if (strcmp(subrecord.name, "ANAM") == 0) {
            string anam = read_string(subrecord.size, state);
            if (state.printing) {
                cout << "    " << blue << "ANAM string: " << normal << anam << endl;
            }
        } else if (strcmp(subrecord.name, "INTV") == 0) {
            u32 intv;
            state.inFile->read((char*) &intv, sizeof(u32));
            if (state.printing) {
                cout << "    " << blue << "INTV: " << normal << intv << endl;
            }
        } else {
            cout << red << "Need to implement FACT." << subrecord.name << "!" << normal << endl;
        }
    }

    if (state.printing) {
        cout << "    " << blue << "ID: " << normal << id << endl;
        cout << "    " << blue << "Name: " << normal << id << endl;
        // TODO print FactionData struct
    }
    return true;
}

//! Reads a race definition record.
bool ESM::record_RACE(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement RACE." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a sound definition record.
bool ESM::record_SOUN(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement SOUN." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a skill definition record.
bool ESM::record_SKIL(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement SKIL." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a magic effect record.
bool ESM::record_MGEF(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement MGEF." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a script record.
bool ESM::record_SCPT(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement SCPT." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a region record.
bool ESM::record_REGN(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement REGN." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a birthsign record.
bool ESM::record_BSGN(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement BSGN." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a land texture record.
bool ESM::record_LTEX(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement LTEX." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a static model record.
bool ESM::record_STAT(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement STAT." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a door definition record.
bool ESM::record_DOOR(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement DOOR." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a miscellaneous item record.
bool ESM::record_MISC(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement MISC." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}

//! Reads a weapon item record.
bool ESM::record_WEAP(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement WEAP." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}
//! Reads a container item record.
bool ESM::record_CONT(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement CONT." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}
//! Reads a spell record.
bool ESM::record_SPEL(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        state.inFile->seekg(subrecord.size, ios_base::cur);
        cout << red << "Need to implement SPEL." << subrecord.name << "!" << normal << endl;
    }
    // TODO
    return false;
}
//! Reads a creature record.
bool ESM::record_CREA(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        if (strcmp(subrecord.name, "NAME") == 0) {
            string creature_id = read_string(subrecord.size, state);
            if (state.printing) {
                cout << "        " << blue << "Creature ID: " << normal << creature_id << endl;
            }
        } else if (strcmp(subrecord.name, "MODL") == 0) {
            string model_name = read_string(subrecord.size, state);
            if (state.printing) {
                cout << "        " << blue << "Creature model: " << normal << model_name << endl;
            }
        } else if (strcmp(subrecord.name, "FNAM") == 0) {
            string creature_name = read_string(subrecord.size, state);
            if (state.printing) {
                cout << "        " << blue << "Creature name: " << normal << creature_name << endl;
            }
        } else if (strcmp(subrecord.name, "NPDT") == 0) {
            CreatureData data;
            state.inFile->read((char*) &data, sizeof(CreatureData));
            if (state.printing) {
                cout << "        " << blue << "Creature Type: " << normal;
                if (data.type == 0) {
                    cout << "Creature" << endl;
                } else if (data.type == 1) {
                    cout << "Daedra" << endl;
                } else if (data.type == 2) {
                    cout << "Undead" << endl;
                } else if (data.type == 3) {
                    cout << "Humanoid" << endl;
                } else {
                    cout << red << "Unknown (" << data.type << ")" << normal << endl;
                }
                cout << "        " << blue << "Level: " << normal << data.level << endl;
                cout << "        " << blue << "Strength: " << normal << data.strength << endl;
                cout << "        " << blue << "Intelligence: " << normal << data.intelligence << endl;
                cout << "        " << blue << "Willpower: " << normal << data.willpower << endl;
                cout << "        " << blue << "Agility: " << normal << data.agility << endl;
                cout << "        " << blue << "Speed: " << normal << data.speed << endl;
                cout << "        " << blue << "Endurance: " << normal << data.endurance << endl;
                cout << "        " << blue << "Personality: " << normal << data.personality << endl;
                cout << "        " << blue << "Luck: " << normal << data.luck << endl;
                cout << "        " << blue << "Health: " << normal << data.health << endl;
                cout << "        " << blue << "Spell Points: " << normal << data.spell_points << endl;
                cout << "        " << blue << "Fatigue: " << normal << data.fatigue << endl;
                cout << "        " << blue << "Soul Level: " << normal << data.soul << endl;
                cout << "        " << blue << "Combat: " << normal << data.combat << endl;
                cout << "        " << blue << "Magic: " << normal << data.magic << endl;
                cout << "        " << blue << "Stealth: " << normal << data.stealth << endl;
                cout << "        " << blue << "Attack 1: " << normal << data.attack_min_1 << "-" << data.attack_max_1 << endl;
                cout << "        " << blue << "Attack 2: " << normal << data.attack_min_2 << "-" << data.attack_max_2 << endl;
                cout << "        " << blue << "Attack 3: " << normal << data.attack_min_3 << "-" << data.attack_max_3 << endl;
                cout << "        " << blue << "Gold: " << normal << data.gold << endl;
            }
        } else {
            cout << red << "Need to implement CREA." << subrecord.name << "!" << normal << endl;
            state.inFile->seekg(subrecord.size, ios_base::cur);
        }
    }
    // TODO
    return false;
}
//! Reads a body part record.
bool ESM::record_BODY(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement BODY." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}
//! Reads a light record.
bool ESM::record_LIGH(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement LIGH." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}
//! Reads an enchanting effect record.
bool ESM::record_ENCH(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement ENCH." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}
//! Reads an NPC record.
bool ESM::record_NPC_(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement NPC_." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads an armor item record.
bool ESM::record_ARMO(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement ARMO." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a clothing item record.
bool ESM::record_CLOT(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement CLOT." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a repair item record.
bool ESM::record_REPA(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement REPA." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads an activator record.
bool ESM::record_ACTI(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement ACTI." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a lockpicking item record.
bool ESM::record_LOCK(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement LOCK." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a probe item record.
bool ESM::record_PROB(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement PROB." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads an ingredient record.
bool ESM::record_INGR(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement INGR." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a book item record.
bool ESM::record_BOOK(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement BOOK." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads an alchemy item record.
bool ESM::record_ALCH(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement ALCH." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a leveled item record.
bool ESM::record_LEVI(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement LEVI." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a leveled creature record.
bool ESM::record_LEVC(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement LEVC." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a cell record.
bool ESM::record_CELL(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement CELL." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a landscape record.
bool ESM::record_LAND(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement LAND." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a path grid record.
bool ESM::record_PGRD(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement PGRD." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a sound generator record.
bool ESM::record_SNDG(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement SNDG." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a dialogue topic record.
//! This includes journal entries.
bool ESM::record_DIAL(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement DIAL." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

//! Reads a dialogue response record belonging
//! to the previous DIAL record.
bool ESM::record_INFO(u32 record_end, ESMScribeState &state) const {
    SubRecordHeader subrecord;
    while (read_subrecord(record_end, subrecord, state)) {
        cout << red << "Need to implement INFO." << subrecord.name << "!" << normal << endl;
        state.inFile->seekg(subrecord.size, ios_base::cur);
    }
    // TODO
    return false;
}

}
}