#ifndef SCRIBE_MORROWIND_ESM_H
#define SCRIBE_MORROWIND_ESM_H

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include "types.h"
#include "Scribes/Abstract/Base.h"

namespace Scribe {
namespace Morrowind {

//! Used to pass state data to internal helper functions.
struct ESMScribeState {
    std::shared_ptr<std::istream> inFile;
    std::shared_ptr<std::ostream> outFile;
    bool printing;
};

struct SubRecordHeader;

class ESM : public Abstract::Base {
public:
    // inherited from Scribe
    bool print(const std::shared_ptr<File> source) const;
    bool canOpen(const std::shared_ptr<std::string> file) const;
private:
    // helper functions
    bool read_subrecord(u32 record_end, SubRecordHeader& subrecord, ESMScribeState &state) const;
    std::string read_string(u32 length, ESMScribeState &state) const;

    bool record_TES3(u32 record_end, ESMScribeState &state) const;   // Main header
    bool record_GMST(u32 record_end, ESMScribeState &state) const;   // Game Setting
    bool record_GLOB(u32 record_end, ESMScribeState &state) const;   // Global Setting
    bool record_CLAS(u32 record_end, ESMScribeState &state) const;   // Class Definition
    bool record_FACT(u32 record_end, ESMScribeState &state) const;   // Faction Definition
    bool record_RACE(u32 record_end, ESMScribeState &state) const;   // Race Definition
    bool record_SOUN(u32 record_end, ESMScribeState &state) const;   // Sound Definition
    bool record_SKIL(u32 record_end, ESMScribeState &state) const;   // Skill Definition
    bool record_MGEF(u32 record_end, ESMScribeState &state) const;   // Magic Effect
    bool record_SCPT(u32 record_end, ESMScribeState &state) const;   // Script
    bool record_REGN(u32 record_end, ESMScribeState &state) const;   // Region
    bool record_BSGN(u32 record_end, ESMScribeState &state) const;   // Birthsign
    bool record_LTEX(u32 record_end, ESMScribeState &state) const;   // Land texture?
    bool record_STAT(u32 record_end, ESMScribeState &state) const;   // Static model
    bool record_DOOR(u32 record_end, ESMScribeState &state) const;   // Door definition
    bool record_MISC(u32 record_end, ESMScribeState &state) const;   // Miscellaneous item
    bool record_WEAP(u32 record_end, ESMScribeState &state) const;   // Weapon item
    bool record_CONT(u32 record_end, ESMScribeState &state) const;   // Container item
    bool record_SPEL(u32 record_end, ESMScribeState &state) const;   // Spell
    bool record_CREA(u32 record_end, ESMScribeState &state) const;   // Creatures
    bool record_BODY(u32 record_end, ESMScribeState &state) const;   // Body parts
    bool record_LIGH(u32 record_end, ESMScribeState &state) const;   // Lights
    bool record_ENCH(u32 record_end, ESMScribeState &state) const;   // Enchanting effect
    bool record_NPC_(u32 record_end, ESMScribeState &state) const;   // NPCs
    bool record_ARMO(u32 record_end, ESMScribeState &state) const;   // Armor
    bool record_CLOT(u32 record_end, ESMScribeState &state) const;   // Clothing
    bool record_REPA(u32 record_end, ESMScribeState &state) const;   // Repair items
    bool record_ACTI(u32 record_end, ESMScribeState &state) const;   // Activator
    bool record_LOCK(u32 record_end, ESMScribeState &state) const;   // Lockpicking items
    bool record_PROB(u32 record_end, ESMScribeState &state) const;   // Probe items
    bool record_INGR(u32 record_end, ESMScribeState &state) const;   // Ingredient items
    bool record_BOOK(u32 record_end, ESMScribeState &state) const;   // Books
    bool record_ALCH(u32 record_end, ESMScribeState &state) const;   // Alchemy
    bool record_LEVI(u32 record_end, ESMScribeState &state) const;   // Leveled items
    bool record_LEVC(u32 record_end, ESMScribeState &state) const;   // Leveled creatures
    bool record_CELL(u32 record_end, ESMScribeState &state) const;   // Cell definitions
    bool record_LAND(u32 record_end, ESMScribeState &state) const;   // Landscape
    bool record_PGRD(u32 record_end, ESMScribeState &state) const;   // Path grid
    bool record_SNDG(u32 record_end, ESMScribeState &state) const;   // Sound generator
    bool record_DIAL(u32 record_end, ESMScribeState &state) const;   // Dialogue topic (includes journals)
    bool record_INFO(u32 record_end, ESMScribeState &state) const;   // Dialogue response record that belongs to previous DIAL record
};

}
}

#endif
