// C includes
#include <cmath>

// C++ includes
#include <filesystem>

// Boost includes
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>

// Local includes
#include "Utility/log.h"

#include "Anvil.h"

// Constants

// Namespaces
using namespace std;
using namespace std::__fs;
using namespace boost::iostreams;

namespace Scribe {
namespace Minecraft {

enum {
    TAG_END,
    TAG_BYTE,
    TAG_SHORT,
    TAG_INT,
    TAG_LONG,
    TAG_FLOAT,
    TAG_DOUBLE,
    TAG_BYTE_ARRAY,
    TAG_STRING,
    TAG_LIST,
    TAG_COMPOUND,
    TAG_INT_ARRAY
};

const string TAG_TYPES[] = {
    "TAG_End",
    "TAG_Byte",
    "TAG_Short",
    "TAG_Int",
    "TAG_Long",
    "TAG_Float",
    "TAG_Double",
    "TAG_Byte_Array",
    "TAG_String",
    "TAG_List",
    "TAG_Compound",
    "TAG_Int_Array"
};

const string BLOCK_NAMES[] = {
    "Air",
    "Stone",
    "Grass",
    "Dirt",
    "Cobblestone",
    "Wood Planks",
    "Sapling",
    "Bedrock",
    "Water",
    "Stationary Water",
    "Lava",
    "Stationary Lava",
    "Sand",
    "Gravel",
    "Gold Ore",
    "Iron Ore",             // 0x0F
    "Coal Ore",
    "Wood",
    "Leaves",
    "Sponge",
    "Glass",
    "Lapis Lazuli Ore",
    "Lapis Lazuli Block",
    "Dispenser",
    "Sandstone",
    "Note Block",
    "Bed",
    "Powered Rail",
    "Detector Rail",
    "Sticky Piston",
    "Cobweb",
    "Tall Grass",           // 0x1F
    "Dead Bush",
    "Piston",
    "Piston Extension",
    "Wool",
    "Block moved by Piston",
    "Dandelion",
    "Poppy",
    "Brown Mushroom",
    "Red Mushroom",
    "Block of Gold",
    "Block of Iron",
    "Double Stone Slab",
    "Stone Slab",
    "Bricks",
    "TNT",
    "Bookshelf",
    "Moss Stone",
    "Obsidian",
    "Torch",
    "Fire",
    "Monster Spawner",
    "Oak Wood Stairs",
    "Chest",
    "Redstone Wire",
    "Diamond Ore",
    "Block of Diamond",
    "Crafting Table",
    "Wheat",
    "Farmland",
    "Furnace",
    "Burning Furnace",
    "Sign Post",
    "Wooden Door",
    "Ladders",
    "Rail",
    "Cobblestone Stairs",
    "Wall Sign",
    "Lever",
    "Stone Pressure Plate",
    "Iron Door",
    "Wooden Pressure Plate",
    "Redstone Ore",
    "Glowing Redstone Ore",
    "Redstone Torch (inactive)",
    "Redstone Torch (active)",
    "Stone Button",
    "Snow",
    "Ice",
    "Snow Block",
    "Cactus",
    "Clay",
    "Sugar Cane",
    "Jukebox",
    "Fence",
    "Pumpkin",
    "Netherrack",
    "Soul Sand",
    "Glowstone",
    "Nether Portal",
    "Jack o' Lantern",
    "Cake Block",
    "Redstone Repeater (inactive)",
    "Redstone Repeater (active)",
    "Locked Chest",
    "Trapdoor",
    "Monster Egg",
    "Stone Bricks",
    "Huge Brown Mushroom",
    "Huge Red Mushroom",
    "Iron Bars",
    "Glass Pane",
    "Melon",
    "Pumpkin Stem",
    "Melon Stem",
    "Vines",
    "Fence Gate",
    "Brick Stairs",
    "Stone Brick Stairs",
    "Mycelium",
    "Lily Pad",
    "Nether Brick",
    "Nether Brick Fence",
    "Nether Brick Stairs",
    "Nether Wart",
    "Enchantment Table",
    "Brewing Stand",
    "Cauldron",
    "End Portal",
    "End Portal Block",
    "End Stone",
    "Dragon Egg",
    "Redstone Lamp (inactive)",
    "Redstone Lamp (active)",
    "Wooden Double Slab",
    "Wooden Slab",
    "Cocoa",
    "Sandstone Stairs",
    "Emerald Ore",
    "Ender Chest",
    "Tripwire Hook",
    "Tripwire",
    "Block of Emerald",
    "Spruce Wood Stairs",
    "Birch Wood Stairs",
    "Jungle Wood Stairs",
    "Command Block",
    "Beacon",
    "Cobblestone Wall",
    "Flower Pot",
    "Carrots",
    "Potatoes",
    "Wooden Button",
    "Mob Head",
    "Anvil",
    "Trapped Chest",
    "Weighted Pressure Plate (Light)",
    "Weighted Pressure Plate (Heavy)",
    "Redstone Comparator (inactive)",
    "Redstone Comparator (active)",
    "Daylight Sensor",
    "Block of Redstone",
    "Nether Quartz Ore",
    "Hopper",
    "Block of Quartz",
    "Quartz Stairs",
    "Activator Rail",
    "Dropper",
    "Stained Clay",
    "(unused)",
    "(unused)",
    "(unused)",
    "(unused)",
    "(unused)",
    "(unused)",
    "(unused)",
    "(unused)",
    "(unused)",
    "(unused)",
    "Hay Block",
    "Carpet",
    "Hardened Clay",
    "Block of Coal",
    "Packed Ice",
    "Large Flowers"
};

bool Anvil::canOpen(const shared_ptr<std::string> source) const {
    LOG_ENTER();
    assert(source);

    if (std::__fs::filesystem::is_directory(source->c_str())) {
        // TODO: check if directory contains level.dat and other miscellanea
        LOG_EXIT();
        return true;
    }

    LOG_EXIT();
    return false;
}

bool Anvil::print(shared_ptr<File> source) const {
    assert(source);

    shared_ptr<istream> file = source->readSource();

    if (!file) {
        Log().error() << "Could not open Anvil file: " << source->filename << endl;
        return false;
    }

    int count = 0;
    while (!file->eof()) {
        u8 byte;
        file->read((char*) &byte, sizeof(byte));
        count++;
    }
    Log().info() << "Read " << count << " bytes." << endl;

    Log().error() << "Anvil::print() not yet implemented.";

    return false;
}

void Anvil::readMap(shared_ptr<string> filename) {
    LOG_ENTER();

    filesystem::path p(filename->c_str());

    if (!is_directory(p)) {
        Log().error() << p << " was not a directory.";
        return;
    }

    p /= "level.dat";

    if (!exists(p)) {
        Log().error() << "level.dat was missing, not a valid Minecraft map directory.";
        return;
    }

    shared_ptr<string> level(new string(p.string()));

    shared_ptr<File> file(new File());

    if (!file->openDiskSourceBinary(level)) {
        Log().error() << "Could not open " << *filename;
        return;
    }

    shared_ptr<istream> raw_file = file->readSource();

    filtering_streambuf<input> filter;

    filter.push(gzip_decompressor());
    filter.push(*raw_file);

    istream fin(&filter);

    // read the root tag
    readCompoundTag(fin, 1);

    LOG_EXIT();
    return;
}

void Anvil::readCompoundTag(istream &fin, int depth) {

    auto readLong = [&] () -> u64 {
        u8 bytes[8];
        fin.read((char*) &bytes, sizeof(u8) * 8);
        u64 sum = 0;
        // TODO: this is totally not correct
        for (int i = 7; i >= 0; i--) {
            sum += bytes[i] * pow(256, 7 - i);
        }
        return sum;
    };

    auto readInt = [&] () -> u32 {
        u8 bytes[4];
        fin.read((char*) &bytes, sizeof(u8) * 4);
        return bytes[3] + (bytes[2] * 256) + (bytes[1] * 256 * 256) + (bytes[0] * 256 * 256 * 256);
    };

    auto readShort = [&] () -> u16 {
        u8 byteHigh;
        u8 byteLow;
        fin.read((char*) &byteHigh, sizeof(u8));
        fin.read((char*) &byteLow, sizeof(u8));
        return byteLow + (byteHigh * 256);
    };

    auto readFloat = [&] () -> f32 {
        f32 result;
        fin.read((char*) &result, sizeof(f32));
        return result;
    };

    auto readDouble = [&] () -> f64 {
        f64 result;
        fin.read((char*) &result, sizeof(f64));
        return result;
    };

    auto readString = [&] () -> string {
        u16 length = readShort();
        string result;
        if (length > 0) {
            char c[length + 1];
            fin.read((char*) &c, sizeof(char) * length);
            c[length] = '\0';
            result.assign(c);
        }
        return result;
    };

    auto indent = [&] () -> string {
        stringstream result;
        for (int i = 0; i < depth; i++) {
            result << "  ";
        }
        return result.str();
    };

    while (true) {

        u8 tagType;
        fin.read((char*) &tagType, sizeof(u8));

        if (tagType == TAG_END) {
            depth--;
            Log().info() << indent() << "} TAG_End";
            return;
        }

        string name = readString();

        stringstream log;
        log << indent() << TAG_TYPES[tagType] << " (" << name << "): ";

        if (tagType == TAG_BYTE) {
            u8 byte;
            fin.read((char*) &byte, sizeof(u8));
            Log().info() << log.str() << hex << (int) byte << dec;
        } else if (tagType == TAG_SHORT) {
            u16 tagShort = readShort();
            Log().info() << log.str() << tagShort;
        } else if (tagType == TAG_INT) {
            u32 val = readInt();
            Log().info() << log.str() << val;
        } else if (tagType == TAG_LONG) {
            u64 val = readLong();
            Log().info() << log.str() << val;
        } else if (tagType == TAG_FLOAT) {
            f32 val = readFloat();
            Log().info() << log.str() << val;
        } else if (tagType == TAG_DOUBLE) {
            f64 val = readDouble();
            Log().info() << log.str() << val;
        } else if (tagType == TAG_BYTE_ARRAY) {
            u32 length = readInt();
            Log().info() << log.str() << "[";
            depth++;
            for (u32 i = 0; i < length; i++) {
                u8 byte;
                fin.read((char*) &byte, sizeof(u8));

                Log().info() << indent() << byte;
            }
            depth--;
            Log().info() << log.str() << "]";
        } else if (tagType == TAG_STRING) {
            string val = readString();
            Log().info() << log.str() << val;
        } else if (tagType == TAG_LIST) {
            u8 listType;
            fin.read((char*) &listType, sizeof(u8));
            u32 length = readInt();
            Log().info() << log.str() << "{";
            depth++;
            for (u32 i = 0; i < length; i++) {
                if (listType == 5) {
                    f32 val = readFloat();
                    Log().info() << indent() << val;
                } else if (listType == 10) {
                    Log().info() << indent() << "Compound {";
                    readCompoundTag(fin, depth+1);
                } else {
                    Log().info() << indent() << "can't handle type " << (int) listType << " yet";
                }
            }
            depth--;
            log << "}";
        } else if (tagType == TAG_COMPOUND) {
            Log().info() << log.str() << "{";
            readCompoundTag(fin, depth+1);
        } else if (tagType == TAG_INT_ARRAY) {
            Log().info() << log.str() << "{";
            u32 length = readInt();
            depth++;
            for (u32 i = 0; i < length; i++) {
                u32 val = readInt();
                Log().info() << indent() << val;
            }
            depth--;
        } else {
            Log().error() << "Unknown tag type " << (int) tagType;
        }
    }
}

}
}