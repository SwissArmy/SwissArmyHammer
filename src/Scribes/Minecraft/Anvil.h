#ifndef SCRIBE_MINECRAFT_ANVIL_H
#define SCRIBE_MINECRAFT_ANVIL_H

#include <string>

#include "types.h"

#include "Scribes/Abstract/Base.h"

namespace Scribe {
namespace Minecraft {

/**
 * @see http://web.archive.org/web/20110729004616/http://www.minecraft.net/docs/NBT.txt
 */
class Anvil : public Abstract::Base {
public:
    // inherited from Base
    bool canOpen(std::shared_ptr<std::string> source) const override;
    std::string getPersonalityName() const override {
        return "Anvil";
    }

    // inherited from Scribe
    bool print(std::shared_ptr<File> source) const;

    void readMap(std::shared_ptr<std::string> filename);
private:
    void readCompoundTag(std::istream &fin, int depth);
};

}
}

#endif

