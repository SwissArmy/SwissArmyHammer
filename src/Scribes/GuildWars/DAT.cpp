#include <cstring>

#include <iostream>

#include <glm/glm.hpp>

#include "Utility/console.h"
#include "Utility/flags.h"
#include "Utility/log.h"

#include "DAT.h"

using namespace std;

namespace Scribe {
namespace GuildWars {

// ####################
// # Structs and such #
// ####################

//! Gw.Dat files are composed of 512 byte blocks.\n
//! The first 512 bytes of the file are always the Root Block which contains a
//! pointer to the Main File Table.
struct RootBlock {
    c8 version[4];        //!< should always be '3AN' + 0x1A
    u32 headerSize;
    u32 blockSize;
    u32 headerCRC;        //!< CRC of the first 12 bytes. Should always be 0x70adcb4c.
    u64 MFTOffset;
    u32 MFTSize;
    u32 flags;
};

//! MFT stands for Main File Table.\n
//! It is composed of a number of 24-byte entries.\n
//! The first entry in the MFT is the header.\n
//! The second entry is a pointer to the 'bigfile' header.\n
//! The third entry is always the 'mysterious' hash table.\n
//! The fourth entry is a self-reference to the MFT.\n
//! The fifth through 16th entries are blank and reserved.\n
//! Entries after that contain content informtion for the 'bigfile'.
struct MFTHeader {
    c8  ID[4];            //!< Should always be 'MFT' + 0x1A
    u32 unknown1;
    u32 unknown2;
    u32 entryCount;       //!< Number of files in the table. The header is entry 0.
    u32 unknown4;
    u32 unknown5;
};

struct MFTEntry {
    u64 offset;
    u32 size;
    u16 compressionFlag;    //!< 0x0 = STORED\n 0x8 = COMPRESSED (likely Huffman encoded.)
    u8  contentFlags;       //!< Known possible values: 0x1, 0x3
    u8  contentType;        //!< Known possible values: 0x2, 0xb. Does not correlate with file type.
    u32 ID;                 //!< Unknown use.
    u32 CRC;
};

// #############
// # Constants #
// #############

const char gwCookie[4] = {'3', 'A', 'N', 0x1A};

// ##################
// # Scribe Methods #
// ##################

bool DAT::canOpen(const std::shared_ptr<std::string> filename) const {
    assert(filename);

    shared_ptr<File> source(new File());

    if (!source->openDiskSourceBinary(filename)) {
        return false;
    }

    shared_ptr<istream> file = source->readSource();

    assert(file);

    if (source->basename().compare("Gw") == 0 && source->fileExtension().compare("dat") == 0) {
        return true;
    }

    return false;
}

// #########
// # Print #
// #########

bool DAT::print(const shared_ptr<File> source) const {
    assert(source);
    assert(source->readSource());

    shared_ptr<istream> file = source->readSource();

    // Read the root block.

    RootBlock rootBlock;
    file->read((char*) &rootBlock, sizeof(RootBlock));

    cout << "== Root Block ==" << endl;
    cout << "    Magic Cookie: " << string(rootBlock.version,4) << endl;
    cout << "    Header Size: " << rootBlock.headerSize << endl;
    cout << "    Sector Size: " << rootBlock.blockSize << endl;
    cout << "    Header CRC: " << rootBlock.headerCRC << endl;
    cout << "    MFT Offset: " << rootBlock.MFTOffset << endl;
    cout << "    MFT Size: " << rootBlock.MFTSize << endl;
    cout << "    Flags: " << rootBlock.flags << endl;

    if (strncmp(rootBlock.version, gwCookie, 4)) {
        cerr << "Could not read Gw.dat file. Not a valid Gw.dat file. " << endl;
        return false;
    }

    // Read the MFT.

    cout << "== MFT ==" << endl;

    file->seekg(rootBlock.MFTOffset);

    // Read the first 5 reserved entries in the MFT.

    MFTHeader mftHeader;
    file->read((char*) &mftHeader, sizeof(MFTHeader));

    cout << "    === " << yellow << "Entry 0 (MFT Header)" << normal << " ===" << endl;
    cout << "        Magic Cookie: " << string(mftHeader.ID, 4) << endl;
    cout << "        Unknown 1: " << mftHeader.unknown1 << endl;
    cout << "        Unknown 2: " << mftHeader.unknown2 << endl;
    cout << "        Entries: " << mftHeader.entryCount << endl;
    cout << "        Unknown 4: " << mftHeader.unknown4 << endl;
    cout << "        Unknown 5: " << mftHeader.unknown5 << endl;

    MFTEntry bigfileHeader;
    file->read((char*) &bigfileHeader, sizeof(MFTEntry));

    cout << "    === Entry 1 (Bigfile Header) ===" << endl;
    cout << "        Offset: " << bigfileHeader.offset << endl;
    cout << "        Size: " << bigfileHeader.size << endl;
    cout << "        Compression Flags: 0x" << hex << bigfileHeader.compressionFlag << dec << endl;
    cout << "        Content Flags: 0x" << hex << (int) bigfileHeader.contentFlags << dec << endl;
    cout << "        Content Type: 0x" << hex << (int) bigfileHeader.contentType << dec << endl;
    cout << "        ID: " << bigfileHeader.ID << endl;
    cout << "        CRC: " << bigfileHeader.CRC << endl;

    MFTEntry hashtableHeader;
    file->read((char*) &hashtableHeader, sizeof(MFTEntry));

    cout << "    === Entry 2 (Hashtable Header) ===" << endl;
    cout << "        Offset: " << hashtableHeader.offset << endl;
    cout << "        Size: " << hashtableHeader.size << endl;
    cout << "        Compression Flags: 0x" << hex << hashtableHeader.compressionFlag << dec << endl;
    cout << "        Content Flags: 0x" << hex << (int) hashtableHeader.contentFlags << dec << endl;
    cout << "        Content Type: 0x" << hex << (int) hashtableHeader.contentType << dec << endl;
    cout << "        ID: " << hashtableHeader.ID << endl;
    cout << "        CRC: " << hashtableHeader.CRC << endl;

    MFTEntry mftSelfReference;
    file->read((char*) &mftSelfReference, sizeof(MFTEntry));

    cout << "    === Entry 3 (MFT Self-Reference) ===" << endl;
    cout << "        Offset: " << mftSelfReference.offset << endl;
    cout << "        Size: " << mftSelfReference.size << endl;
    cout << "        Compression Flags: 0x" << hex << mftSelfReference.compressionFlag << dec << endl;
    cout << "        Content Flags: 0x" << hex << (int) mftSelfReference.contentFlags << dec << endl;
    cout << "        Content Type: 0x" << hex << (int) mftSelfReference.contentType << dec << endl;
    cout << "        ID: " << mftSelfReference.ID << endl;
    cout << "        CRC: " << mftSelfReference.CRC << endl;

    // Skip 12 empty entries that are probably reserved for later use.

    file->seekg(12 * sizeof(MFTEntry), ios_base::cur);
    cout << "    === Entry 4 (Reserved) ===" << endl;
    cout << "    === Entry 5 (Reserved) ===" << endl;
    cout << "    === Entry 6 (Reserved) ===" << endl;
    cout << "    === Entry 7 (Reserved) ===" << endl;
    cout << "    === Entry 8 (Reserved) ===" << endl;
    cout << "    === Entry 9 (Reserved) ===" << endl;
    cout << "    === Entry 10 (Reserved) ===" << endl;
    cout << "    === Entry 11 (Reserved) ===" << endl;
    cout << "    === Entry 12 (Reserved) ===" << endl;
    cout << "    === Entry 13 (Reserved) ===" << endl;
    cout << "    === Entry 14 (Reserved) ===" << endl;
    cout << "    === Entry 15 (Reserved) ===" << endl;
    cout << "    === Entry 16 (Reserved) ===" << endl;

    // Begin reading the rest of the entries in the MFT.
    vector<MFTEntry> mftEntries;
    mftEntries.resize(mftHeader.entryCount - 16);

    for (u32 i = 0; i < mftEntries.size(); i++) {
        file->read((char*) &(mftEntries[i]), sizeof(MFTEntry));

        cout << "    === Entry " << i+17 << " ===" << endl;
        cout << "        Offset: " << mftEntries[i].offset << endl;
        cout << "        Size: " << mftEntries[i].size << endl;
        cout << "        Compression Flags: 0x" << hex << mftEntries[i].compressionFlag << dec << endl;
        cout << "        Content Flags: 0x" << hex << (int) mftEntries[i].contentFlags << dec << endl;
        cout << "        Content Type: 0x" << hex << (int) mftEntries[i].contentType << dec << endl;
        cout << "        ID: " << mftEntries[i].ID << endl;
        cout << "        CRC: " << mftEntries[i].CRC << endl;

        // read the file pointed to by the mftEntry
        if (mftEntries[i].compressionFlag == 0x0) {
            // save place in the table so we can seek back to it after we read the file
            ios::pos_type table_location = file->tellg();

            // keep track of how much we've read so we know when we get to the end
            u32 position = 0;
            file->seekg(mftEntries[i].offset);
            c8 filetype[4];
            file->read((char*) &filetype, sizeof(c8) * 4);
            position += 4;
            cout << "        File type: " << string(filetype, 4) << endl;
            if (strcmp(filetype, "ATEX") == 0) {
                c8 textureType[4];
                file->read((char*) &textureType, sizeof(c8) * 4);
                position += 4;
                cout << "        Texture type: " << string(textureType, 4) << endl;
            } else if (strcmp(filetype, "ffna") == 0) {

            }

            // print the remaining data
            file->seekg(mftEntries[i].offset);
            cout << "        Remaining raw data: " << hex;
            for (u32 j = position; j < mftEntries[i].size; j += 4) {
                u8 byte;
                file->read((char*) &byte, sizeof(u8));
                cout << (int) byte << " ";
            }
            cout << endl;

            file->seekg(table_location);
        }
    }

    return true;
}

// #############
// # readFiles #
// #############

//! \todo Stub function.
vector<shared_ptr<File> > DAT::readFiles(const shared_ptr<File> source) const {
    assert(source);
    assert(source->readSource());

    vector<shared_ptr<File>> results;

    shared_ptr<istream> file = source->readSource();

    print(source);

    return results;
}

// ##############
// # writeFiles #
// ##############

//! \todo Stub function.
bool DAT::writeFiles(const shared_ptr<File> archive, const vector<shared_ptr<File> > files) const {
    assert(archive);

    return false;
}

}
}