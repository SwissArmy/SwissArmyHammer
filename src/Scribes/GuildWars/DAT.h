#ifndef SCRIBE_GUILDWARS_DAT_H
#define SCRIBE_GUILDWARS_DAT_H

#include <string>
#include <vector>
#include <fstream>

#include "types.h"
#include "Scribes/Abstract/Archive.h"

namespace Scribe {
namespace GuildWars {

//! \brief A Scribe for the Gw.dat file included with Guild Wars.
//! \details
//! \see RootBlock\n
//! \copydetails RootBlock\n
//! \see MFTHeader\n
//! \copydetails MFTHeader\n
//! \see MFTEntry\n
//! \copydetails MFTEntry\n
//! \see MFTExpansion\n
//! \copydetails MFTExpansion\n
class DAT : public Abstract::Archive {
public:
    // inherited from Base
    bool canOpen(std::shared_ptr<std::string> file) const override;
    std::string getPersonalityName() const override {
        return "DAT";
    };

    // inherited from Scribe
    bool print(std::shared_ptr<File> source) const;

    // inherited from ArchiveScribe
    std::vector<std::shared_ptr<File> > readFiles(std::shared_ptr<File> archive) const override;
    bool writeFiles(std::shared_ptr<File> archive, std::vector<std::shared_ptr<File>> files) const override;
};

}
}

#endif

