#include <boost/algorithm/string.hpp>

#include "Utility/log.h"

#include "MTL.h"

using namespace std;
using namespace Phobos;

namespace Scribe {
namespace Standard {

bool MTL::canOpen(const std::shared_ptr<std::string> filename) const {
    assert(filename);

    shared_ptr<File> source(new File());

    if (!source->openDiskSourceBinary(filename)) {
        return false;
    }

    shared_ptr<istream> file = source->readSource();

    assert(file);

    if (source->fileExtension().compare("mtl") == 0) {
        return true;
    }
    return false;
}

vector<shared_ptr<Phobos::Texture>> MTL::readTextures(const shared_ptr<File> source) const {
    LOG_ENTER();
    assert(source);
    assert(source->readSource());

    shared_ptr<istream> file = source->readSource();
    vector<shared_ptr<Phobos::Texture>> textures;
    shared_ptr<Phobos::Texture> current_texture;

    int line_number = 1;
	string line;

	while (getline(*file, line)) {
        vector<string> tokens;
        boost::split(tokens, line, boost::is_any_of(" "));

        if (tokens[0].compare("#") == 0 || tokens[0].compare("") == 0) {
            // comment or blank line, do nothing
        } else if (tokens[0].compare("newmtl") == 0) {
            // beginning a new material
            if (current_texture != nullptr) {
                textures.push_back(current_texture);
            }
            current_texture.reset(new Phobos::Texture());
            current_texture->name = tokens[1];
        } else if (tokens[0].compare("Ka") == 0) {
            // ambient color of the material
            if (tokens.size() == 4) {
                current_texture->ambient.r = stof(tokens[1]);
                current_texture->ambient.g = stof(tokens[2]);
                current_texture->ambient.b = stof(tokens[3]);
            } else {
                Log().warning() << "MTL file had a Ka line with insufficient parameters.";
            }
        } else if (tokens[0].compare("Kd") == 0) {
            // diffuse color
            if (tokens.size() == 4) {
                current_texture->diffuse.r = stof(tokens[1]);
                current_texture->diffuse.g = stof(tokens[2]);
                current_texture->diffuse.b = stof(tokens[3]);
            } else {
                Log().warning() << "MTL file had a Kd line with insufficient parameters.";
            }
        } else if (tokens[0].compare("Ks") == 0) {
            // specular color
            Log().warning() << "Specular color in MTL files unsupported.";
        } else if (tokens[0].compare("Ns") == 0) {
            // specular coefficient
            Log().warning() << "Specular coefficient in MTL files unsupported.";
        } else if (tokens[0].compare("d") == 0 || tokens[0].compare("Tr") == 0) {
            // transparency, also called 'dissolve'
            Log().warning() << "Transparency in MTL files unsupported.";
        } else if (tokens[0].compare("illum") == 0) {
            // which illumination model to use
            Log().warning() << "Illumination models in MTL files unsupported.";
        } else if (tokens[0].compare("map_Ka") == 0) {
            // use a texture map for ambient color
            Log().warning() << "Ambient color texture maps in MTL files unsupported.";
        } else if (tokens[0].compare("map_Kd") == 0) {
            // use a texture map for diffuse color
            Log().warning() << "Diffuse color texture maps in MTL files unsupported.";
        } else if (tokens[0].compare("map_Ks") == 0) {
            // use a texture map for specular color
            Log().warning() << "Specular color texture maps in MTL files unsupported.";
        } else if (tokens[0].compare("map_Ns") == 0) {
            // use a texture map for specular coefficient
            Log().warning() << "Specular coefficient texture maps in MTL files unsupported.";
        } else if (tokens[0].compare("map_d") == 0 || tokens[0].compare("map_Tr")) {
            // use a texture map for transparency
            Log().warning() << "Transparency texture maps in MTL files unsupported.";
        } else if (tokens[0].compare("map_bump") == 0 || tokens[0].compare("bump") == 0) {
            // use a bump map
            Log().warning() << "Bump maps in MTL files unsupported.";
        } else if (tokens[0].compare("disp") == 0) {
            // use a displacement map
            Log().warning() << "Displacement maps in MTL files unsupported.";
        } else if (tokens[0].compare("decal") == 0) {
            // use a stencil decal texture
            Log().warning() << "Stencil decal textures in MTL files unsupported.";
        } else {
            Log().warning() << "Encountered unknown data type in OBJ file: '" << tokens[0] << "'";
        }

        line_number++;
    }

    if (current_texture != nullptr) {
        textures.push_back(current_texture);
    }

    LOG_EXIT();
    return textures;
}

}
}