#ifndef OBJ_SCRIBE_H
#define OBJ_SCRIBE_H

#include "Scribes/Abstract/Model.h"

namespace Scribe {
namespace Standard {

//! A plaintext open standard model format. Often used as an intermediate conversion format.
//! It is very simple and does not contain many advanced features.
class OBJ : public Abstract::Model {
public:
    // Inherited from Base
    bool canOpen(std::shared_ptr<std::string> file) const override;
    std::string getPersonalityName() const override {
        return "OBJ";
    }

    // Inherited from Scribe
    bool print(std::shared_ptr<File> source) const;

    // Inherited from ModelScribe
    std::vector<std::shared_ptr<Phobos::Model>> readModels(std::shared_ptr<File> source) const override;
    bool writeModels(std::shared_ptr<File> target, std::vector<std::shared_ptr<Phobos::Model>> model) const override;
};

}
}

#endif
