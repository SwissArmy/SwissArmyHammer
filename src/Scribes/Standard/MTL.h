#ifndef WAVEFRONT_MTL_SCRIBE
#define WAVEFRONT_MTL_SCRIBE

#include "Scribes/Abstract/Texture.h"

namespace Scribe {
namespace Standard {

class MTL : public Abstract::Texture {
public:
    // inherited from Base
    bool canOpen(std::shared_ptr<std::string> file) const override;
    std::string getPersonalityName() const override {
        return "MTL";
    }

    // inherited from TextureScribe
    std::vector<std::shared_ptr<Phobos::Texture>> readTextures(std::shared_ptr<File> source) const override;
};

}
}

#endif