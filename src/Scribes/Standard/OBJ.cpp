// C includes
#include <cmath>

// C++ includes
#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>

// Boost includes
#include <boost/algorithm/string.hpp>

#include "Phobos/Model.h"

#include "Scribes/Standard/MTL.h"

#include "Utility/console.h"
#include "Utility/log.h"

#include "OBJ.h"

using namespace std;
using namespace std::__fs;
using namespace Phobos;

namespace Scribe {
namespace Standard {

bool OBJ::canOpen(const std::shared_ptr<std::string> filename) const {
    assert(filename);

    shared_ptr<File> source(new File());

    if (!source->openDiskSourceBinary(filename)) {
        return false;
    }

    shared_ptr<istream> file = source->readSource();

    assert(file);

    if (source->fileExtension().compare("obj") == 0) {
        return true;
    }
    return false;
}

bool OBJ::print(shared_ptr<File> source) const {
    assert(source);
    assert(source->readSource());

    shared_ptr<istream> file = source->readSource();

    // TODO: implement OBJ::print

    return false;
}

vector<shared_ptr<Phobos::Model>> OBJ::readModels(shared_ptr<File> source) const {
    LOG_ENTER();
    assert(source);
    assert(source->readSource());

    shared_ptr<istream> fin = source->readSource();
    vector<shared_ptr<Phobos::Model>> models;
    shared_ptr<Phobos::Model> model(new Phobos::Model);

    // Standard OBJ files don't support animation.
    // It can be extended with .anm files but we don't support those (yet)
    model->frames.resize(1);

    int line_number = 0;
	string line;

    // Used for assigning triangle groups
    u32 current_group = 0;
    string current_texture = "default";
    string current_name;
    u32 current_smoothing = 0;

	while (getline(*fin, line)) {
        line_number++;
        vector<string> tokens;
        boost::trim(line);
        boost::split(tokens, line, boost::is_any_of(" "), boost::token_compress_on);

        if (tokens[0].compare("f") == 0) {
            // this is a face

            if (tokens.size() < 4) {
                Log().warning() << "Encountered a face with only " << (tokens.size() - 1) << " vertices on line " << line_number;
                continue;
            }

            vector<int> vertices;
            for (u32 i = 1; i < tokens.size(); i++) {
                vertices.push_back(stoi(tokens[i]));
            }

            if (vertices.size() > 3) {
                // break the polygon into triangles
                // TODO: once we support triangle fans this could be optimized
                // TODO: we're pretty naive about this, we just assume it's convex
                for (u32 i = 0; i < vertices.size() - 2; i++) {
                    Triangle t;
                    t.vertices[0] = vertices[0] - 1;
                    t.vertices[1] = vertices[1+i] - 1;
                    t.vertices[2] = vertices[2+i] - 1;
                    model->addTriangle(t, current_group);
                }
            } else {
                // we count from 0, OBJ counts from 1
                Triangle t;
                t.vertices[0] = vertices[0] - 1;
                t.vertices[1] = vertices[1] - 1;
                t.vertices[2] = vertices[2] - 1;
                model->addTriangle(t, current_group);
            }
        } else if (tokens[0].compare("v") == 0) {
            // this is a vertex
            if (tokens.size() != 4) {
                Log().warning() << "Found a " << (tokens.size() - 1) << "-dimensional vertex on line " << line_number;
                continue;
            }

            vector<f32> points;
            for (u32 i = 1; i < tokens.size(); i++) {
                points.push_back(stof(tokens[i]));
            }

            Vertex v;
            v.x = points[0];
            v.y = points[1];
            v.z = points[2];
            model->frames[0].vertices.push_back(v);
        } else if (tokens[0].compare("s") == 0) {
            // indicates a smoothing group
            if (tokens.size() != 2) {
                Log().warning() << "Malformed smoothing group token at line " << line_number;
                continue;
            }
            if (tokens[1].compare("off") == 0) {
                current_smoothing = 0;
            } else {
                current_smoothing = stoul(tokens[1]);
            }
            current_group++;
            model->setGroupName(current_group, current_name);
            model->setGroupTexture(current_group, current_texture);
            model->setGroupSmoothing(current_group, current_smoothing);
        } else if (tokens[0].compare("mtllib") == 0) {
            // references a .mtl file to open

            filesystem::path objPath = *(source->filename);
            filesystem::path mtlPath = objPath.parent_path();
            mtlPath /= tokens[1];

            Log().trace() << "Reading material library " << mtlPath;

            if (filesystem::is_empty(mtlPath)) {
                Log().error() << "Referenced material file " << mtlPath << " at line " << line_number << " is empty and could not be opened.";
            } else if (is_directory(mtlPath)) {
                Log().error() << "Referenced material file " << mtlPath << " at line " << line_number << " is a directory. Must be a .mtl file.";
            } else {
                shared_ptr<string> filename(new string);
                filename->assign(mtlPath.string());
                shared_ptr<File> file(new File);
                if (file->openDiskSource(filename)) {
                    MTL mtlScribe;
                    vector<shared_ptr<Texture>> textures = mtlScribe.readTextures(file);
                    for (shared_ptr<Texture> t : textures) {
                        model->addTexture(t, t->name);
                    }
                } else {
                    Log().error() << "Referenced material file " << mtlPath << " at line " << line_number << " could not be opened for an unknown reason.";
                }
            }

        } else if (tokens[0].compare("usemtl") == 0) {
            // the following object uses the indicated material
            if (tokens.size() != 2) {
                Log().warning() << "Malformed usemtl tag at line " << line_number;
                continue;
            }
            current_texture = tokens[1];
            current_group++;
            model->setGroupName(current_group, current_name);
            model->setGroupTexture(current_group, current_texture);
            model->setGroupSmoothing(current_group, current_smoothing);
        } else if (tokens[0].compare("g") == 0) {
            // indicates a polygon group
            if (tokens.size() > 2) {
                Log().warning() << "Malformed group identifier '" << line << "' at line " << line_number;
                continue;
            }
            if (tokens.size() == 1) {
                current_name = "";
            } else {
                current_name = tokens[1];
            }
            current_group++;
            model->setGroupName(current_group, current_name);
            model->setGroupTexture(current_group, current_texture);
            model->setGroupSmoothing(current_group, current_smoothing);
            // TODO: not sure if changing the group name resets what material is being used
        } else if (tokens[0].compare("#") == 0 || tokens[0].compare("") == 0) {
            // do nothing, this is a comment or blank line
        } else {
            Log().info() << "Encountered unknown data type in OBJ file: '" << tokens[0] << "'";
            // TODO: see spec for other tags that aren't implemented
        }
    }

    // TODO: eliminate the need for this cleaning step, if possible?
	model->clean();

    models.push_back(model);

    LOG_EXIT();
    return models;
}

bool OBJ::writeModels(shared_ptr<File> source, vector<shared_ptr<Phobos::Model>> models) const {
    assert(source);
    assert(models.size() >= 1);
    assert(source->writeSource());

    if (models.size() != 1) {
        //warning("Given " + models.size() + " models, OBJ files can only handle 1. Ignoring rest.");
        Log().warning() << "Given too many models. OBJ files can only handle 1. Ignoring rest.";
    }

    shared_ptr<ostream> fout = source->writeSource();

    shared_ptr<Phobos::Model> model = models.front();

    *fout << "# OBJ file format with ext .obj" << endl;
    *fout << "# vertex count = " << model->frames[0].vertices.size() << endl;
    *fout << "# face count = " << model->triangleCount() << endl;

    for (unsigned int i = 0; i < model->frames[0].vertices.size(); i++) {
        *fout << "v " << model->frames[0].vertices[i].x << " " << model->frames[0].vertices[i].y << " " << model->frames[0].vertices[i].z << endl;
    }
    // TODO: implement triangle grouping
    for (unsigned int i = 0; i < model->triangleCount(); i++) {
        *fout << "f " << (model->triangle(i).vertices[0]+1) << " " << (model->triangle(i).vertices[1]+1) << " " << (model->triangle(i).vertices[2]+1) << endl;
    }

    return true;
}

}
}