#ifndef SCRIBE_QUAKE2_MD2_H
#define SCRIBE_QUAKE2_MD2_H

#include "Scribes/Abstract/Model.h"

namespace Scribe {
namespace Quake2 {

//! A fairly simple model format used by many games, and originating in Quake 2.
class MD2 : public Abstract::Model {
public:
    // Inherited from Scribe
    bool print(std::shared_ptr<File> source) const;
    bool canOpen(const std::shared_ptr<std::string> file) const;

    // Inherited from ModelScribe
    std::vector<std::shared_ptr<Phobos::Model>> readModels(std::shared_ptr<File> source) const;
    bool writeModels(std::shared_ptr<File> target, std::vector<std::shared_ptr<Phobos::Model>> model) const;
};

}
}

#endif
