// C includes
#include <cmath>

// C++ includes
#include <iostream>
#include <fstream>

// SOIL includes
#include <SOIL.h>

// Local includes
#include "Utility/log.h"

#include "MD2.h"
#include "MD2VertexNormals.h"

using namespace std;
using namespace Phobos;

namespace Scribe {
namespace Quake2 {

// ###########
// # Structs #
// ###########

struct MD2_Header {
    u32 magicNumber;     //!< Should always be 0x32504449 ("IDP2")
    u32 version;         //!< Always 8.
    u32 skinWidth;
    u32 skinHeight;
    u32 frameSize;
    u32 numSkins;
    u32 numVertices;
    u32 numTexCoords;
    u32 numTriangles;
    u32 glCommandsSize; //!< size of the GLCommands block, in dwords (4 byte quantity)
    u32 numFrames;
    u32 offsetSkins;
    u32 offsetTexCoords;
    u32 offsetTriangles;
    u32 offsetFrames;
    u32 offsetGLCommands;
    u32 offsetEnd;
};

struct MD2_Vertex {
    u8 x;
    u8 y;
    u8 z;
    u8 lightNormalIndex;
};

struct MD2_FrameHeader {
    f32 scaleX;
    f32 scaleY;
    f32 scaleZ;
    f32 translationX;
    f32 translationY;
    f32 translationZ;
    c8 name[16];
};

struct MD2_Triangle {
	s16 vertexIndexes[3];
	s16 textureIndexes[3];
};

struct MD2_Skin {
    c8 name[64];
};

struct MD2_TextureCoordinate {
    s16 x;
    s16 y;
};

struct MD2_GLCommandVertex {
    f32 x;  //!< Used for texture coordinate mapping.
    f32 y;  //!< Used for texture coordinate mapping.
    u32 vertexIndex;
};

const u32 MAGIC_NUMBER = 0x32504449;
const u32 VERSION = 8;

bool MD2::canOpen(const std::shared_ptr<std::string> filename) const {
    assert(filename);

    shared_ptr<File> source(new File());

    if (!source->openDiskSourceBinary(filename)) {
        return false;
    }

    shared_ptr<istream> file = source->readSource();

    assert(file);

    if (source->fileExtension().compare("md2") == 0) {
        // TODO: check magic cookie too
        return true;
    }
    return false;
}

bool MD2::writeModels(shared_ptr<File> source, vector<shared_ptr<Phobos::Model>> models) const {
    assert(source);
    assert(models.size() >= 1);
	assert(source->writeSource());

    if (models.size() > 1) {
        Log().warning() << "Given " << models.size() << " models. MD2Scribe can only handle 1. Ignoring rest.";
    }

    shared_ptr<Phobos::Model> model = models.front();

    shared_ptr<ostream> file = source->writeSource();

	// Write the header.

	MD2_Header header;

	header.magicNumber = MAGIC_NUMBER;
	header.version = VERSION;

	//header.numSkins = model->textures.size();
	//header.numVertices = model->frames[0].vertices.size();
	//header.numTriangles = model->triangles.size();
	//header.numTexCoords = model->textureCoordinates.size();
	//header.glCommandsSize = model->polygons.size();   // TODO: calculate this properly
	//header.numFrames = model->frames.size();

	header.frameSize = sizeof(MD2_FrameHeader) + (header.numVertices * sizeof(MD2_Vertex));

	header.skinWidth = 0;   // TODO: calculate this
	header.skinHeight = 0;	// TODO: calculate this

	header.offsetSkins = sizeof(header);
	header.offsetTexCoords = header.offsetSkins + (header.numSkins * sizeof(MD2_Skin));
	header.offsetTriangles = header.offsetTexCoords + (header.numTexCoords * sizeof(MD2_TextureCoordinate));
	header.offsetFrames = header.offsetTriangles + (header.numTriangles * sizeof(MD2_Triangle));
	header.offsetGLCommands = header.offsetFrames + (header.numFrames * header.frameSize);
	header.offsetEnd = header.offsetGLCommands + (header.glCommandsSize * 8);

	file->write((char*) &header, sizeof(header));

    // TODO: these asserts should be removed and replaced with read failure messages,
    // TODO: they might be the result of a corrupt file and we should handle that more gracefully.
    assert(file->tellp() == (streampos) header.offsetSkins);

	// Write the skins.

	for (u32 i = 0; i < header.numSkins; i++) {
		MD2_Skin skin;

		// TODO: implement this

		file->write((char*) &skin, sizeof(MD2_Skin));
	}

    assert(file->tellp() == (streampos) header.offsetTexCoords);

	// Write the texture coordinates.

	for (u32 i = 0; i < header.numTexCoords; i++) {
		MD2_TextureCoordinate texCoord;

		// TODO: make sure these are being scaled correctly for MD2 format
		//texCoord.x = (s16) model->textureCoordinates[i].x;
		//texCoord.y = (s16) model->textureCoordinates[i].y;

		file->write((char*) &texCoord, sizeof(MD2_TextureCoordinate));
	}

    assert(file->tellp() == (streampos) header.offsetTriangles);

	// Write the triangles.

	for (u32 i = 0; i < header.numTriangles; i++) {
		MD2_Triangle triangle;

		//triangle.vertexIndexes[0] = model->triangles[i].vertexIndexes[0];
		//triangle.vertexIndexes[1] = model->triangles[i].vertexIndexes[1];
		//triangle.vertexIndexes[2] = model->triangles[i].vertexIndexes[2];
		//triangle.textureIndexes[0] = model->triangles[i].textureIndexes[0];
		//triangle.textureIndexes[1] = model->triangles[i].textureIndexes[1];
		//triangle.textureIndexes[2] = model->triangles[i].textureIndexes[2];

		file->write((char*) &triangle, sizeof(MD2_Triangle));
	}

    assert(file->tellp() == (streampos) header.offsetFrames);

	// Write the frames.

	for (u32 f = 0; f < header.numFrames; f++) {
		/*MD2_FrameHeader frameHeader;
		MD2_Vertex* frameVertices = new MD2_Vertex[header.numVertices];

		// Calculate the extremal points of the mesh so we can scale and translate it.
		f32 biggestX = 0, smallestX = 0, biggestY = 0, smallestY = 0, biggestZ = 0, smallestZ = 0;
		for (u32 v = 0; v < header.numVertices; v++) {
			f32 x = model->frames[f].vertices[v].position.X;
			f32 y = model->frames[f].vertices[v].position.Y;
			f32 z = model->frames[f].vertices[v].position.Z;
			if (z < smallestZ)	smallestZ = z;
			if (z > biggestZ)	biggestZ = z;
			if (x < smallestX)	smallestX = x;
			if (x > biggestX)	biggestX = x;
			if (y < smallestY)	smallestY = y;
			if (y > biggestY)	biggestY = y;
		}

		// map the biggest x to 255 and the smallest x to 1...
		frameHeader.scaleX = (biggestX - smallestX) / 255.0f;
		frameHeader.scaleY = (biggestY - smallestY) / 255.0f;
		frameHeader.scaleZ = (biggestZ - smallestZ) / 255.0f;
		frameHeader.translationX = smallestX;
		frameHeader.translationY = smallestY;
		frameHeader.translationZ = smallestZ;

		model->frames[f].name.copy(frameHeader.name, 16);

		for (u32 v = 0; v < header.numVertices; v++) {
			frameVertices[v].x = floor( (model->frames[f].vertices[v].position.X - frameHeader.translationX) / frameHeader.scaleX );
			frameVertices[v].y = floor( (model->frames[f].vertices[v].position.Y - frameHeader.translationY) / frameHeader.scaleY );
			frameVertices[v].z = floor( (model->frames[f].vertices[v].position.Z - frameHeader.translationZ) / frameHeader.scaleZ );
			// TODO: figure out the light normal correctly
			frameVertices[v].lightNormalIndex = 0;
		}

		file->write((char*) &frameHeader, sizeof(MD2_FrameHeader));
		file->write((char*) frameVertices, sizeof(MD2_Vertex) * header.numVertices);
		delete frameVertices;*/
	}

    assert(file->tellp() == (streampos) header.offsetGLCommands);

	// Write the GL commands.

    /*
	for (u32 i = 0; i < model->polygons.size(); i++) {

		u32 numCommandVertices = (model->polygons[i].vertices.size());
		if (model->polygons[i].type == 0) {
			numCommandVertices = (-numCommandVertices);
		}

		file->write((char*) &numCommandVertices, sizeof(u32));

		if (model->polygons[i].type == 0)
			numCommandVertices = (-numCommandVertices);

		for (u32 g = 0; g < numCommandVertices; g++) {
			MD2_GLCommandVertex glCommand;

			glCommand.vertexIndex = model->polygons[i].vertices[g];
			glCommand.x = model->polygons[i].textureCoordinates[g].x;
			glCommand.y = model->polygons[i].textureCoordinates[g].y;

			file->write((char*) &glCommand, sizeof(MD2_GLCommandVertex));
		}
	}
	*/

	// Indicate the end of the GLCommand list.
	s32 zero = 0;
	file->write((char*) &zero, sizeof(s32));

	// Now we should be at the end of the file.

    assert(file->tellp() == (streampos) header.offsetEnd);

	return true;
}

vector<shared_ptr<Phobos::Model>> MD2::readModels(shared_ptr<File> source) const {
    LOG_ENTER();
    assert(source);

    vector<shared_ptr<Phobos::Model>> models;

	// Read the header.
    Log().trace() << "Reading the header.";

	MD2_Header header = source->read<MD2_Header>();

	if (header.magicNumber != MAGIC_NUMBER) {
        Log().error() << "Could not read file " << source->filename << ": Not a valid MD2 file.";
        LOG_EXIT();
		return models;
	}

	if (header.version != VERSION) {
        Log().error() << "Could not read file " << source->filename << ": Invalid MD2 version number.";
        LOG_EXIT();
		return models;
	}

    shared_ptr<Phobos::Model> model(new Phobos::Model);
    models.push_back(model);

    Log().trace() << "Reading skins.";

	// TODO: actually use skin dimensions
	//skinHeight = header.skinHeight;
	//skinWidth = header.skinWidth;

    assert(source->readPos() == (streampos) header.offsetSkins);

	for (u32 i = 0; i < header.numSkins; i++) {
		MD2_Skin skin = source->read<MD2_Skin>();

        shared_ptr<Texture> t(new Texture());
        t->name = string(skin.name);
        t->texture_id = SOIL_load_OGL_texture(skin.name, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_DDS_LOAD_DIRECT);
        model->addTexture(t, t->name);
        model->setGroupTexture(0, t->name);
	}

    Log().trace() << "Reading texture coordinates.";

    assert(source->readPos() == (streampos) header.offsetTexCoords);

    model->textureCoordinates.resize(header.numTexCoords);
	for (u32 i = 0; i < header.numTexCoords; i++) {
		MD2_TextureCoordinate texCoord = source->read<MD2_TextureCoordinate>();

		model->textureCoordinates[i].x = texCoord.x;
		model->textureCoordinates[i].y = texCoord.y;
	}

    Log().trace() << "Reading triangles.";

    assert(source->readPos() == (streampos) header.offsetTriangles);

	for (u32 i = 0; i < header.numTriangles; i++) {
		MD2_Triangle triangle = source->read<MD2_Triangle>();

        Triangle t(triangle.vertexIndexes[2], triangle.vertexIndexes[1], triangle.vertexIndexes[0]);
        t.texCoords[0] = triangle.textureIndexes[0];
        t.texCoords[1] = triangle.textureIndexes[1];
        t.texCoords[2] = triangle.textureIndexes[2];

        model->addTriangle(t);
	}

    Log().trace() << "Reading frames.";

    assert(source->readPos() == (streampos) header.offsetFrames);

    model->frames.resize(header.numFrames);

    for (u32 f = 0; f < header.numFrames; f++) {
        MD2_FrameHeader frameHeader = source->read<MD2_FrameHeader>();

        // TODO: implement this
        //model->frames[f].name.assign(frameHeader.name, 16);

		for (u32 i = 0; i < header.numVertices; i++) {

            MD2_Vertex md2Vert = source->read<MD2_Vertex>();

            Vertex v;
            v.x = (md2Vert.x * frameHeader.scaleX) + frameHeader.translationX;
		    v.y = (md2Vert.y * frameHeader.scaleY) + frameHeader.translationY;
		    v.z = (md2Vert.z * frameHeader.scaleZ) + frameHeader.translationZ;

            Normal n;
            n.x = MD2_VERTEX_NORMALS[md2Vert.lightNormalIndex][0];
            n.y = MD2_VERTEX_NORMALS[md2Vert.lightNormalIndex][1];
            n.z = MD2_VERTEX_NORMALS[md2Vert.lightNormalIndex][2];
            n = glm::normalize(n);

            model->frames[f].vertices.push_back(v);
            model->frames[f].vertex_normals.push_back(n);
		}
	}

    Log().trace() << "Reading GLCommands.";

    // We handle GL commands by converting them into triangles.

    assert(source->readPos() == (streampos) header.offsetGLCommands);

	while (true) {
		s32 glCommandCount = source->read<s32>();

		if (glCommandCount == 0) {
            // end of list reached
			break;
		}

        if (glCommandCount < 0) {
            // Triangle fan

            //! \bug This code won't work for fans that "wrap" (I think.) Needs some testing.
            vector<Triangle> triangles;
            triangles.resize((-glCommandCount) - 2);
            for (s32 v = 0; v < (-glCommandCount); v++) {
                MD2_GLCommandVertex glCommandVertex = source->read<MD2_GLCommandVertex>();

				//TextureCoordinate texCoord;
				//texCoord.x = glCommandVertex.x;
				//texCoord.y = glCommandVertex.y;
				//mesh->textureCoordinates.push_back(texCoord);

				// Start by making the center of the fan a vertex of every triangle.
				if (v == 0) {
                    for (Triangle &t : triangles) {
                        t.vertices[0] = glCommandVertex.vertexIndex;
                        //t.textureIndexes[0] = mesh->textureCoordinates.size() - 1;
                    }
				} else if (v > 1 && v < (-glCommandCount) - 1) {
					triangles[v-1].vertices[1] = glCommandVertex.vertexIndex;
					//triangles[v-1].vertices[1] = mesh->textureCoordinates.size() - 1;
				} else if (v > 2 && v < (-glCommandCount) - 2) {
					triangles[v].vertices[2] = glCommandVertex.vertexIndex;
					//triangles[v].vertices[2] = mesh->textureCoordinates.size() - 1;
				}
            }

            for (Triangle &t : triangles) {
                //model->addTriangle(t);
            }
        } else {
            // Triangle strip

            vector<Triangle> triangles;
            triangles.resize(glCommandCount - 2);
            for (s32 v = 0; v < glCommandCount; v++) {
                MD2_GLCommandVertex glCommandVertex = source->read<MD2_GLCommandVertex>();

				//TextureCoordinate texCoord;
				//texCoord.x = glCommandVertex.x;
				//texCoord.y = glCommandVertex.y;
				//mesh->textureCoordinates.push_back(texCoord);

				if (v < glCommandCount - 2) {
					//triangles[v].vertices[0] = glCommandVertex.vertexIndex;
                }

				if (v > 1 && v < glCommandCount - 1) {
					//triangles[v-1].vertices[1] = glCommandVertex.vertexIndex;
                }

				if (v > 2) {
					//triangles[v-1].vertices[1] = glCommandVertex.vertexIndex;
					//triangles[v-2].vertices[2] = glCommandVertex.vertexIndex;
				}
            }

            for (Triangle &t : triangles) {
                //model->addTriangle(t);
            }
        }
    }

    Log().trace() << "Reached end of file.";

    assert(source->readPos() == (streampos) header.offsetEnd);

    Log().trace() << "Rotating model.";

    model->rotate(180,-90);

    Log().trace() << "Model rotated, calculating face normals.";

    model->calculateFaceNormals();

    assert(model);

    LOG_EXIT();
	return models;
}

bool MD2::print(shared_ptr<File> source) const {
    /*
    assert(source);
	assert(source->readSource());

    shared_ptr<istream> file = source->readSource();

	// Read the header.
	cout << "= Reading MD2 file =" << endl;

	cout << "== File Header ==" << endl;

	MD2_Header header;
	file->read((char*) &header, sizeof(MD2_Header));

	cout << "    Magic Number: 0x" << hex << header.magicNumber << dec << endl;
	cout << "    Version: " << header.version << endl;
	cout << "    Skin Width: " << header.skinWidth << endl;
	cout << "    Skin Height: " << header.skinHeight << endl;
	cout << "    Frame Size: " << header.frameSize << " bytes" << endl;
	cout << "    Number of skins: " << header.numSkins << endl;
	cout << "    Number of vertices: " << header.numVertices << endl;
	cout << "    Number of texture coordinates: " << header.numTexCoords << endl;
	cout << "    Number of triangles: " << header.numTriangles << endl;
	cout << "    Size of GLCommands block: " << header.glCommandsSize << " dwords" << endl;
	cout << "    Number of frames: " << header.numFrames << endl;
	cout << "    Skin Offset: 0x" << hex << header.offsetSkins << dec << endl;
	cout << "    Texture Coordinates Offset: 0x" << hex << header.offsetTexCoords << dec << endl;
	cout << "    Triangles Offset: 0x" << hex << header.offsetTriangles << dec << endl;
	cout << "    Frames Offset: 0x" << hex << header.offsetFrames << dec << endl;
	cout << "    GLCommands Offset: 0x" << hex << header.offsetGLCommands << dec << endl;
	cout << "    End Offset: 0x" << hex << header.offsetEnd << dec << endl;

	cout << "== Skins ==" << endl;
	if (file->tellg() != (streampos) header.offsetSkins) {
		cerr << "Wrong skin offset. Off by " << header.offsetSkins - file->tellg() << " bytes." << endl;
		file->seekg(header.offsetSkins);
	}
	for (u32 i = 0; i < header.numSkins; i++) {
		MD2_Skin skin;
		file->read((char*) &skin, sizeof(MD2_Skin));
		cout << "    Skin " << i << ": " << skin.name << endl;
	}

	// Read the texture coordinates.
	cout << "== Texture Coordinates ==" << endl;
	if (file->tellg() != (streampos) header.offsetTexCoords) {
		cerr << "Warning: Didn't read correct texCoords offset. Missed by " << header.offsetSkins - file->tellg() << " bytes." << endl;
		file->seekg(header.offsetTexCoords);
	}
	for (u32 i = 0; i < header.numTexCoords; i++) {
		MD2_TextureCoordinate texCoord;
		file->read((char*) &texCoord, sizeof(MD2_TextureCoordinate));

		cout << "    Texture Coordinate " << i << ": x=" << texCoord.x << " y=" << texCoord.y << endl;
	}

	// Read the triangles.
	cout << "== Triangles ==" << endl;
	if (file->tellg() != (streampos) header.offsetTriangles) {
		cerr << "Warning: Didn't read correct triangles offset. Missed by " << header.offsetTriangles - file->tellg() << " bytes." << endl;
		file->seekg(header.offsetTriangles);
	}
	for (u32 i = 0; i < header.numTriangles; i++) {
		MD2_Triangle triangle;

		file->read((char*) &triangle, sizeof(MD2_Triangle));

		cout << "    Triangle " << i << ": vx=" << triangle.vertexIndexes[0] << " vy=" << triangle.vertexIndexes[1] << " vz=" << triangle.vertexIndexes[2];
		cout << " tx=" << triangle.textureIndexes[0] << " ty=" << triangle.textureIndexes[1] << " tz=" << triangle.textureIndexes[2] << endl;
	}

	// Read the frames.
	cout << "== Frames ==" << endl;
	if (file->tellg() != (streampos) header.offsetFrames) {
		cerr << "Warning: Didn't read correct frame offset. Missed by " << header.offsetFrames - file->tellg() << " bytes." << endl;
		file->seekg(header.offsetFrames);
	}
	for (u32 f = 0; f < header.numFrames; f++) {
		MD2_FrameHeader frameHeader;
		MD2_Vertex* frameVertices = new MD2_Vertex[header.numVertices];

		file->read((char*) &frameHeader, sizeof(MD2_FrameHeader));
		file->read((char*) frameVertices, sizeof(MD2_Vertex) * header.numVertices);

		cout << "    === Frame " << f << " ===" << endl;
		cout << "        Name: " << frameHeader.name << endl;
		cout << "        Scale: x=" << frameHeader.scaleX << " y=" << frameHeader.scaleY << " z=" << frameHeader.scaleZ << endl;
		cout << "        Translation: x=" << frameHeader.translationX << " y=" << frameHeader.translationY << " z=" << frameHeader.translationZ << endl;
		cout << "        ==== Vertices ====" << endl;

		for (u32 v = 0; v < header.numVertices; v++) {
			cout << "            Vertex " << v << ": x=" << (s32) frameVertices[v].x << " y=" << (s32) frameVertices[v].y << " z=" << (s32) frameVertices[v].z;
			cout << " Normal Index: " << (s32) frameVertices[v].lightNormalIndex << endl;
		}

		delete [] frameVertices;
	}

	// Read the GL commands.
	cout << "== GLCommands ==" << endl;
	if (file->tellg() != (streampos) header.offsetGLCommands) {
		cerr << "Warning: Didn't read correct GLCommand offset. Missed by " << header.offsetGLCommands - file->tellg() << " bytes." << endl;
		file->seekg(header.offsetGLCommands);
	}

	bool endOfListReached = false;
	int i = 0;
	while (!endOfListReached) {
		s32 numCommandVertices;
		file->read((char*) &numCommandVertices, sizeof(s32));

		if (numCommandVertices == 0) {
			endOfListReached = true;
		} else {
			cout << "    GLCommand " << i << ": ";

			if (numCommandVertices < 0) {
				numCommandVertices = (-numCommandVertices);
				cout << numCommandVertices << " vertices. (triangle fan)" << endl;
			} else {
				cout << numCommandVertices << " vertices. (triangle strip)" << endl;
			}

			for (s32 v = 0; v < numCommandVertices; v++) {
				MD2_GLCommandVertex glCommandVertex;
				file->read((char*) &glCommandVertex, sizeof(MD2_GLCommandVertex));

				cout << "        Vertex " << v << ": v=" << glCommandVertex.vertexIndex << " tx=" << glCommandVertex.x << " ty=" << glCommandVertex.y << endl;
			}
		}
		i++;
	}

	// Now we should be at the end of the file.
	cout << "== End of file ==" << endl;

	if (file->tellg() != (streampos) header.offsetEnd) {
		cerr << "Warning: Didn't read correct EOF offset. Missed by " << header.offsetEnd - file->tellg() << " bytes." << endl;
		file->seekg(header.offsetEnd);
	}
    */
	return true;
}

}
}